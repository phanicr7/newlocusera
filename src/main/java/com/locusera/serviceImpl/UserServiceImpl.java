package com.locusera.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.locusera.command.EnquiryCommand;
import com.locusera.command.JobListCommand;
import com.locusera.command.LoginCommand;
import com.locusera.command.RegistrationCommand;
import com.locusera.dao.UserDao;
import com.locusera.modal.Job_Post;
import com.locusera.modal.UserRegistration;
import com.locusera.service.UserService;
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDaoImpl;
	
	@Override
	public boolean saveUser(RegistrationCommand registrationCommand) {
		
		return userDaoImpl.saveUser(registrationCommand);
	}

	@Override
	public UserRegistration checkUser(LoginCommand loginCommand) {
		
		return userDaoImpl.checkUser(loginCommand);
	}

	@Override
	public boolean saveEnquiry(EnquiryCommand command) {
		
		return userDaoImpl.saveEnquiry(command);
	}

	@Override
	public boolean checkUserEMail(String mail) {
		
		return userDaoImpl.checkUserEMail(mail);
	}

	@Override
	public boolean checkUserName(String username) {

		return userDaoImpl.checkUserName(username);
	}
	
	@Override
	public List<Job_Post> getJobs() {
		
		return userDaoImpl.getJobs();
	}

	@Override
	public boolean saveJob(JobListCommand joblistCommand) {
		
		return userDaoImpl.saveJob(joblistCommand);
	}

	@Override
	public UserRegistration updatepassword(int id, String Pwd) {
		
		return userDaoImpl.updatepassword(id, Pwd);
	}

	@Override
	public boolean checkUserPhone(long phone) {
		return userDaoImpl.checkUserPhone(phone);
	}

	@Override
	public boolean updateNewPass(long mobile, String pwd) {
	
		return userDaoImpl.updateNewPass(mobile, pwd);
	}

	@Override
	public boolean CheckEnMail(String mail) {
		return userDaoImpl.CheckEnMail(mail);
	}

	@Override
	public boolean CheckEnPhone(long phone) {
		return userDaoImpl.CheckEnPhone(phone);
	}

	@Override
	public int checkUserCredentials(String username, String password) {
		
		return userDaoImpl.checkUserCredentials(username, password);
	}


	@Override
	public List<Job_Post> jobindustry(int id) {
		return userDaoImpl.jobindustry(id);
	}


	



}
