package com.locusera.serviceImpl;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.locusera.service.EmailService;
@Service
public class EmailServiceImpl implements EmailService   {
	
	
	 @Autowired
	    JavaMailSender mailSender;
	 
	    @Override
	    public void sendEmail(String mailto,String body) {
	    	
	 
	       
	 
	        MimeMessagePreparator preparator = getMessagePreparator(mailto,body);
	 
	        try {
	            mailSender.send(preparator);
	            System.out.println("Message Send...Hurrey");
	        } catch (MailException ex) {
	            System.err.println(ex.getMessage());
	        }
	    }
	 
	    private MimeMessagePreparator getMessagePreparator(final String mailto,final String body) {
	 
	        MimeMessagePreparator preparator = new MimeMessagePreparator() {
	 
	            public void prepare(MimeMessage mimeMessage) throws Exception {
	                mimeMessage.setFrom("web@jagahunt.com");
	                mimeMessage.setRecipient(Message.RecipientType.TO,
	                        new InternetAddress(mailto));
	                mimeMessage.setText(body);
	                mimeMessage.setSubject("Job Alert");
	            }
	        };
	        return preparator;
	    }

}
