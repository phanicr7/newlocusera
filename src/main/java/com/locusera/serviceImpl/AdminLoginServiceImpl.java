package com.locusera.serviceImpl;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.locusera.command.ArticalCommand;
import com.locusera.command.Citycommand;
import com.locusera.command.ClientLogoCommand;
import com.locusera.command.CompanyAddressCommand;
import com.locusera.command.CompanyCommand;
import com.locusera.command.EventCommand;
import com.locusera.command.IndustryCommand;
import com.locusera.command.JobCommand;
import com.locusera.command.LocationCommand;
import com.locusera.command.NewsCommand;
import com.locusera.command.ProfileCommand;
import com.locusera.command.Statecommand;
import com.locusera.command.SubIndustryCommand;
import com.locusera.dao.AdminLoginDao;
import com.locusera.dao.EnquiryDao;
import com.locusera.dao.JobsDao;
import com.locusera.dao.ResumeDataDao;
import com.locusera.modal.AdminRegistration;
import com.locusera.modal.BlogArticals;
import com.locusera.modal.City;
import com.locusera.modal.Client_Logo;
import com.locusera.modal.Company;
import com.locusera.modal.CompanyAddress;
import com.locusera.modal.Contact_Us;
import com.locusera.modal.Events;
import com.locusera.modal.Industry;
import com.locusera.modal.Job_Post;
import com.locusera.modal.Location;
import com.locusera.modal.News;
import com.locusera.modal.State;
import com.locusera.modal.SubIndustry;
import com.locusera.modal.TestiMonial;
import com.locusera.modal.UserRegistration;
import com.locusera.service.AdminLoginService;

@Service
public class AdminLoginServiceImpl implements AdminLoginService {
	
	@Autowired
	private AdminLoginDao adminLoginDao;
	

	@Override
	public AdminRegistration login(String username, String password) {

		return adminLoginDao.login(username, password);
	}

	@Override
	public int saveartical(BlogArticals artical) {
		return adminLoginDao.saveartical(artical);
	}

	@Override
	public int savenews(News news) {
		return adminLoginDao.savenews(news);
	}

	@Override
	public int saveevent(Events event) {
		return adminLoginDao.saveevent(event);
	}

	@Override
	public int savejob(Job_Post job) {
		return adminLoginDao.savejob(job);
	}

	@Override
	public int savesate(State state) {
		return adminLoginDao.savesate(state);
	}

	@Override
	public List<State> getStates() {
		return adminLoginDao.getStates();
	}

	@Override
	public int savecity(Citycommand command) {
		return adminLoginDao.savecity(command);
	}

	@Override
	public Map<Integer,String> getcitys(int stateid) {
		return adminLoginDao.getcitys(stateid);
	}

	@Override
	public int savelocation(LocationCommand command) {
		return adminLoginDao.savelocation(command);
	}

	@Override
	public Map<Integer, String> getlocations(int cityid) {
		return adminLoginDao.getlocations(cityid);
	}

	@Override
	public AdminRegistration updateprofileinfo(int id,ProfileCommand command) {
		return adminLoginDao.updateprofileinfo(id,command);
	}

	@Override
	public AdminRegistration updatepassword(int id,String pwd) {
		return adminLoginDao.updatepassword(id,pwd);
	}

	@Override
	public List<UserRegistration> getusers() {
		return adminLoginDao.getusers();
	}

	@Override
	public int saveindustry(Industry commnand) {
		return adminLoginDao.saveindustry(commnand);
	}

	@Override
	public List<Industry> getIndustrys() {
		return adminLoginDao.getIndustrys();
	}

	@Override
	public int savesubindustry(SubIndustryCommand command) {
		return adminLoginDao.savesubindustry(command);
	}

	@Override
	public Map<Integer, String> getjobs(int id) {
		return adminLoginDao.getjobs(id);
	}

	@Override
	public List<ResumeDataDao> getResumes() {
		return adminLoginDao.getResumes();
	}

	@Override
	public Location getLocation(int id) {
		return adminLoginDao.getLocation(id);
	}

	@Override
	public SubIndustry getsubindustry(int id) {
		return adminLoginDao.getsubindustry(id);
	}

	@Override
	public int savecompany(Company company) {
		return adminLoginDao.savecompany(company);
	}

	@Override
	public List<Company> getCompanys() {
		return adminLoginDao.getCompanys();
	}

	@Override
	public int address(CompanyAddressCommand command) {
		return adminLoginDao.address(command);
	}

	@Override
	public Map<Integer, String> getcompanylocations(int id) {
		return adminLoginDao.getcompanylocations(id);
	}

	@Override
	public CompanyAddress getCompanyAddress(int id) {
		return adminLoginDao.getCompanyAddress(id);
	}

	@Override
	public List<Events> getEventList() {
		
		return adminLoginDao.getEventList();
	}

	@Override
	public List<News> getNewsList() {
		
		return adminLoginDao.getNewsList();
	}
	@Override
	public List<JobsDao> getjobs() {
		
	     
		return adminLoginDao.getjobs();
	}

	@Override
	public List<EnquiryDao> getEnquirydetails() {
		return adminLoginDao.getEnquirydetails();
	}

	@Override
	public List<BlogArticals> getArticals() {
		return adminLoginDao.getArticals();
	}


	@Override
	public List<Object[]> getdetails(int id) {
		return adminLoginDao.getdetails(id);
	}

	@Override
	public News getNewsById(int news_id) {
		
		return adminLoginDao.getNewsById(news_id);
	}

	@Override
	public Events getEventById(int event_id) {
		
		return adminLoginDao.getEventById(event_id);
	}
	public int saveTestimonial(TestiMonial monial) {
		return adminLoginDao.saveTestimonial(monial);
	}

	@Override
	public List<TestiMonial> getTestimonialsList() {
		return adminLoginDao.getTestimonialsList();
	}

	@Override
	public int savecontactus(Contact_Us contact) {
		return adminLoginDao.savecontactus(contact);
	}

	@Override
	public BlogArticals getBlogById(int blog_id) {
		
		return adminLoginDao.getBlogById(blog_id);
	}

	@Override
	public List<Contact_Us> getConatactUsDetails() {
		return adminLoginDao.getConatactUsDetails();
	}

	@Override
	public List<Client_Logo> getClienttLogos() {
		return adminLoginDao.getClienttLogos();
	}

	@Override
	public int saveClientLogo(Client_Logo logo) {
		return adminLoginDao.saveClientLogo(logo);
	}

	@Override
	public boolean deleteBlog(int id) {
		return adminLoginDao.deleteBlog(id);
	}

	@Override
	public boolean deleteNews(int id) {
		return adminLoginDao.deleteNews(id);
	}

	
	@Override
	public boolean deleteClient(int id) {
		return adminLoginDao.deleteClient(id);
	}
	@Override
	public boolean deleteEvent(int id) {
		return adminLoginDao.deleteEvent(id);
	}


	public boolean deletEnquiry(int id) {
		return adminLoginDao.deletEnquiry(id);
	}

	

	@Override
	public BlogArticals editArticle(int articalId) {
		return adminLoginDao.editArticle(articalId);
	}

	@Override
	public boolean deleteJob(int id) {
		return adminLoginDao.deleteJob(id);
	}

	@Override
	public boolean updateArticle(ArticalCommand articalCommand) {
		
		return adminLoginDao.updateArticle(articalCommand);
	}

	@Override
	public News editNews(int id) {
		return  adminLoginDao.editNews(id);
	}

	@Override
	public boolean updateNews(NewsCommand newsCommand) {
		return adminLoginDao.updateNews(newsCommand);
	}

	@Override
	public Events editEvents(int id) {
		
		return adminLoginDao.editEvents(id);
	}

	@Override
	public boolean updateEvent(EventCommand eventCommand) {
		return adminLoginDao.updateEvent(eventCommand);
	}

	@Override
	public Job_Post editJob(int id) {
		return adminLoginDao.editJob(id);
	}

	@Override
	public Client_Logo editClient(int id) {
		
		return adminLoginDao.editClient(id);
	}

	@Override
	public boolean updateClient(ClientLogoCommand clientCommand) {
		
		return adminLoginDao.updateClient(clientCommand);
	}

	@Override
	public List<Industry> getIndustryList() {
		return adminLoginDao.getIndustryList();
	}

	@Override
	public List<ResumeDataDao> getResumesByIndustry(int id) {
		return adminLoginDao.getResumesByIndustry(id);
	}

	@Override
	public List<ResumeDataDao> getResumesBySubInd(List<Integer> allIds) {
		
		return adminLoginDao.getResumesBySubInd(allIds);
	}

	@Override
	public List<ResumeDataDao> getResumesByDate(Date fromDate,Date toDate) {
		return adminLoginDao.getResumesByDate(fromDate, toDate);
	}

	@Override
	public boolean updateJob(JobCommand jobCommand) {
		return adminLoginDao.updateJob(jobCommand);
	}

	@Override
	public List<LocationCommand> getLocations() {
		
		return adminLoginDao.getLocations();
	}

	@Override
	public List<LocationCommand> getCity() {
		
		return adminLoginDao.getCity();
	}

	@Override
	public List<SubIndustryCommand> getSubIndustryList() {
		
		return adminLoginDao.getSubIndustryList();
	}

	@Override
	public boolean deleteState(int id) {
		return  adminLoginDao.deleteState(id);
	}

	@Override
	public State editState(int id) {
		return adminLoginDao.editState(id);
	}

	@Override
	public boolean deleteCity(int id) {
	
		return adminLoginDao.deleteCity(id);
	}

	@Override
	public boolean deleteLocation(int id) {
		return adminLoginDao.deleteLocation(id);
	}

	@Override
	public boolean deleteIndustry(int id) {
		return adminLoginDao.deleteIndustry(id);
	}

	
	@Override
	public boolean deleteCompany(int id) {
		
		return adminLoginDao.deleteCompany(id);
	}

	@Override
	public boolean deleteSubIndustry(int id) {
		return adminLoginDao.deleteSubIndustry(id);
	}

	
	@Override
	public Industry editIndustry(int id) {
		return adminLoginDao.editIndustry(id);
	}

	@Override
	public boolean updateState(Statecommand statecommand) {
		return adminLoginDao.updateState(statecommand);
	}

	@Override
	public boolean updateIndustry(IndustryCommand industryCommand) {
		return adminLoginDao.updateIndustry(industryCommand);
	}

	@Override
	public Company editCompany(int id) {
		return adminLoginDao.editCompany(id);
	}

	@Override
	public City editCity(int id) {
		return adminLoginDao.editCity(id);
	}

	@Override
	public boolean updateCompany(CompanyCommand companyCommand) {
		return adminLoginDao.updateCompany(companyCommand);
	}

	@Override
	public Location editLocation(int id) {
		return adminLoginDao.editLocation(id);
	}

	@Override
	public boolean updatecity(LocationCommand command) {
		return adminLoginDao.updatecity(command);
	}

	@Override
	public SubIndustry editSub(int id) {
		return adminLoginDao.editSub(id);
	}

	@Override
	public boolean updateSub(SubIndustryCommand subindustrycommand) {
		return adminLoginDao.updateSub(subindustrycommand);
	}

	@Override
	public boolean updatelocation(LocationCommand locationCommand) {
		return adminLoginDao.updatelocation(locationCommand);
	}

	@Override
	public boolean testmonialdelete(int id) {
		return adminLoginDao.testmonialdelete(id);
	}

	@Override
	public int getTotalBlog() {
		return adminLoginDao.getTotalBlog();
	}

	@Override
	public List<BlogArticals> totalblog(int resultsFrom, int recordsPerPage) {
		return adminLoginDao.totalblog(resultsFrom, recordsPerPage);
	}

	@Override
	public int getTotaljobs() {
		return adminLoginDao.getTotaljobs();
	}

	@Override
	public List<Job_Post> totaljobs(int resultsFrom, int recordsPerPage) {
		return adminLoginDao.totaljobs(resultsFrom, recordsPerPage);
	}

	
	
	}
