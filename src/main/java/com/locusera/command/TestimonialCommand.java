package com.locusera.command;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class TestimonialCommand {
	
	private String name;
	private String contentbody;
	private MultipartFile logo;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getContentbody() {
		return contentbody;
	}
	public void setContentbody(String contentbody) {
		this.contentbody = contentbody;
	}
	public MultipartFile getLogo() {
		return logo;
	}
	public void setLogo(MultipartFile logo) {
		this.logo = logo;
	}
	

}
