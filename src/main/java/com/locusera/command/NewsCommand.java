package com.locusera.command;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
@Component
public class NewsCommand implements Serializable {

	
	private static final long serialVersionUID = 413863475012410742L;

	private int newsid;
	public int getNewsid() {
		return newsid;
	}
	public void setNewsid(int newsid) {
		this.newsid = newsid;
	}
	private String title;
	private String heading;
    private MultipartFile logo;
	private String news_content;
	private String oldpath;
	private String newpath;
	
	public String getNewpath() {
		return newpath;
	}
	public void setNewpath(String newpath) {
		this.newpath = newpath;
	}
	public String getOldpath() {
		return oldpath;
	}
	public void setOldpath(String oldpath) {
		this.oldpath = oldpath;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public MultipartFile getLogo() {
		return logo;
	}
	public void setLogo(MultipartFile logo) {
		this.logo = logo;
	}
	public String getNews_content() {
		return news_content;
	}
	public void setNews_content(String news_content) {
		this.news_content = news_content;
	}

	
	

}
