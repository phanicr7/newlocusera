package com.locusera.command;

import org.springframework.web.multipart.MultipartFile;

public class ClientLogoCommand {
	private int clientid;
	public int getClientid() {
		return clientid;
	}
	public void setClientid(int clientid) {
		this.clientid = clientid;
	}
	private String name;
	private MultipartFile path;
	private String oldpath;
	private String newpath;
	public String getNewpath() {
		return newpath;
	}
	public void setNewpath(String newpath) {
		this.newpath = newpath;
	}
	public String getOldpath() {
		return oldpath;
	}
	public void setOldpath(String oldpath) {
		this.oldpath = oldpath;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MultipartFile getPath() {
		return path;
	}
	public void setPath(MultipartFile path) {
		this.path = path;
	}
	
   	  
	

}
