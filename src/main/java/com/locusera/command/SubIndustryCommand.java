package com.locusera.command;

import org.springframework.stereotype.Component;

@Component
public class SubIndustryCommand {
	
	
	private int id;
	private String name;
	
	private String industry_name;
	public String getIndustry_name() {
		return industry_name;
	}
	public void setIndustry_name(String industry_name) {
		this.industry_name = industry_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
