package com.locusera.command;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
@Component
public class JobCommand implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1461439471479907477L;
	private int jobid;
	 public int getJobid() {
		return jobid;
	}
	public void setJobid(int jobid) {
		this.jobid = jobid;
	}
	 private  int  companyname;
     private int locationname;
	 private int openings;
	 private int catogory;
	 private int subcatogory;
	 private String experience;
	 private String job_status;
	
	public String getJob_status() {
		return job_status;
	}
	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public int getCatogory() {
		return catogory;
	}
	public void setCatogory(int catogory) {
		this.catogory = catogory;
	}
	public int getSubcatogory() {
		return subcatogory;
	}
	public void setSubcatogory(int subcatogory) {
		this.subcatogory = subcatogory;
	}
    private String jobtitle;
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	private String  qualification;
	private String  skills;
	private MultipartFile  image;
	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}
	
	public int getLocationname() {
		return locationname;
	}
	public void setLocationname(int locationname) {
		this.locationname = locationname;
	}
	public int getOpenings() {
		return openings;
	}
	public void setOpenings(int openings) {
		this.openings = openings;
	}
	
	

	public String getQualification() {
		return qualification;
	}
	public int getCompanyname() {
		return companyname;
	}
	public void setCompanyname(int companyname) {
		this.companyname = companyname;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}

}
