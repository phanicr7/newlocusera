package com.locusera.command;

import org.springframework.stereotype.Component;

@Component
public class ProfileCommand {
	

	private String name;
	private String email;
	private Long number;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getNumber() {
		return number;
	}
	public void setNumber(Long number) {
		this.number = number;
	}
	public static void main(String[] args) {
	
	}
	

}
