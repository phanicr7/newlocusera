package com.locusera.command;

public class EnquiryCommand {

  private String username;
  private String email;
  private long phone;
  public int getState_id() {
	return state_id;
}
public void setState_id(int state_id) {
	this.state_id = state_id;
}
public int getCity_id() {
	return city_id;
}
public void setCity_id(int city_id) {
	this.city_id = city_id;
}
public int getLocation_id() {
	return location_id;
}
public void setLocation_id(int location_id) {
	this.location_id = location_id;
}
private int state_id;
  private int city_id;
  private int location_id;
  private String description;
  public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public long getPhone() {
	return phone;
}
public void setPhone(long phone) {
	this.phone = phone;
}

public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
  
}
