package com.locusera.command;

import java.sql.Date;

import org.springframework.web.multipart.MultipartFile;

public class JobListCommand {
	private int job_id;
	private long candidate_mobilenumber;
	private String candidate_email;
	private String candidate_name;
	private MultipartFile candidate_resume;
	private String resume_path;
	private String applied_as;
	
	private Date from_date;
	private Date to_date;
	
	public Date getFrom_date() {
		return from_date;
	}
	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}
	public Date getTo_date() {
		return to_date;
	}
	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}
	public String getApplied_as() {
		return applied_as;
	}
	public void setApplied_as(String applied_as) {
		this.applied_as = applied_as;
	}
	public String getResume_path() {
		return resume_path;
	}
	public void setResume_path(String resume_path) {
		this.resume_path = resume_path;
	}
	public long getCandidate_mobilenumber() {
		return candidate_mobilenumber;
	}
	public void setCandidate_mobilenumber(long candidate_mobilenumber) {
		this.candidate_mobilenumber = candidate_mobilenumber;
	}
	public String getCandidate_email() {
		return candidate_email;
	}
	public void setCandidate_email(String candidate_email) {
		this.candidate_email = candidate_email;
	}
	public String getCandidate_name() {
		return candidate_name;
	}
	public void setCandidate_name(String candidate_name) {
		this.candidate_name = candidate_name;
	}
	public MultipartFile getCandidate_resume() {
		return candidate_resume;
	}
	public void setCandidate_resume(MultipartFile candidate_resume) {
		this.candidate_resume = candidate_resume;
	}
	private String job_title;
	public String getJob_title() {
		return job_title;
	}
	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}
	private int no_openings;
	private String experience;
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	private String qualification_details;
	private String skills_required;
	private String city_name;
	private String location_name;
	private String company_name;
	private String industry;
	private String job_status;
	public String getJob_status() {
		return job_status;
	}
	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	private String sub_industry;
	public String getSub_industry() {
		return sub_industry;
	}
	public void setSub_industry(String sub_industry) {
		this.sub_industry = sub_industry;
	}
	public int getJob_id() {
		return job_id;
	}
	public void setJob_id(int job_id) {
		this.job_id = job_id;
	}
	public int getNo_openings() {
		return no_openings;
	}
	public void setNo_openings(int no_openings) {
		this.no_openings = no_openings;
	}
	public String getQualification_details() {
		return qualification_details;
	}
	public void setQualification_details(String qualification_details) {
		this.qualification_details = qualification_details;
	}
	public String getSkills_required() {
		return skills_required;
	}
	public void setSkills_required(String skills_required) {
		this.skills_required = skills_required;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

}
