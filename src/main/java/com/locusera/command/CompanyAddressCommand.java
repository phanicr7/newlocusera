package com.locusera.command;

import org.springframework.stereotype.Component;

@Component
public class CompanyAddressCommand {
	
	public int getCompanyname() {
		return companyname;
	}
	public void setCompanyname(int companyname) {
		this.companyname = companyname;
	}
	public int getLocationname() {
		return locationname;
	}
	public void setLocationname(int locationname) {
		this.locationname = locationname;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
	private int companyname;
	private int locationname;
	private String information;
	private String statename;
	private String cityname;
	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	public String getCityname() {
		return cityname;
	}
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}

}
