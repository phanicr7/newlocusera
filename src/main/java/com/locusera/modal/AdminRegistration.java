package com.locusera.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity  
@Table(name="le_admin_registration")  
public class AdminRegistration implements Serializable {
	
	  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int getLe_id() {
		return le_id;
	}
	public void setLe_id(int le_id) {
		this.le_id = le_id;
	}
	public String getLe_name() {
		return le_name;
	}
	public void setLe_name(String le_name) {
		this.le_name = le_name;
	}
	public String getLe_pws() {
		return le_pws;
	}
	public void setLe_pws(String le_pws) {
		this.le_pws = le_pws;
	}
	public String getLe_email() {
		return le_email;
	}
	public void setLe_email(String le_email) {
		this.le_email = le_email;
	}
	public Long getLe_phone_number() {
		return le_phone_number;
	}
	public void setLe_phone_number(Long le_phone_number) {
		this.le_phone_number = le_phone_number;
	}
	@Id
	@Column(name="le_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	private int le_id;
	
	private String le_name;
	private String le_pws;
	private String le_email;
	private Long le_phone_number;
	
	

}
