package com.locusera.modal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="le_company_address")
public class CompanyAddress {
	@Id
	@Column(name="address_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private int address_id;
    private String address;
	public int getAddress_id() {
		return address_id;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public void setAddress_id(int address_id) {
		this.address_id = address_id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name = "company_id", nullable = false)
	private Company company=new Company();
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name ="location_id", nullable = false)

	private Location location=new Location();
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=Job_Post.class,orphanRemoval = true,cascade=CascadeType.ALL)
	@JoinColumn(name="address_id", referencedColumnName="address_id")
	private List<Job_Post> jobposted=new ArrayList<Job_Post>();
	
	
	
	
	
	
	
	
	
}
