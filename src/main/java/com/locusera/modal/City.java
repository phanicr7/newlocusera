package com.locusera.modal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="le_city_master")
public class City 
{
	@Id
	@Column(name="city_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private Integer cityid;
	
	@Column(name="cityname")
	private String cityname;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=Location.class,orphanRemoval = true,cascade=CascadeType.ALL)
	@JoinColumn(name="city_id", referencedColumnName="city_id")
	private List<Location> location=new ArrayList<Location>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name = "state_id", nullable = false)

	private State state=new State();
	


	public Integer getCityid() {
		return cityid;
	}




	public State getState() {
		return state;
	}




	public void setState(State state) {
		this.state = state;
	}




	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}




	public String getCityname() {
		return cityname;
	}




	public void setCityname(String cityname) {
		this.cityname = cityname;
	}




	public List<Location> getLocation() {
		return location;
	}




	public void setLocation(List<Location> location) {
		this.location = location;
	}

	

 }
