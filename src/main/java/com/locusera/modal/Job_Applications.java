package com.locusera.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
@Entity  
@Table(name="le_job_applications")  
public class Job_Applications {
	@Id
	@Column(name="le_application_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
    private int le_application_id;
	private String candidate_name;
	private String candidate_email;
	private long candidate_mobilenumber;
	private String applied_as;
	
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime applied_date;

	
	public LocalDateTime getApplied_date() {
		return applied_date;
	}

	public void setApplied_date(LocalDateTime applied_date) {
		this.applied_date = applied_date;
	}

	public String getApplied_as() {
		return applied_as;
	}

	public void setApplied_as(String applied_as) {
		this.applied_as = applied_as;
	}

	public int getLe_application_id() {
		return le_application_id;
	}

	public void setLe_application_id(int le_application_id) {
		this.le_application_id = le_application_id;
	}

	public String getCandidate_name() {
		return candidate_name;
	}

	public void setCandidate_name(String candidate_name) {
		this.candidate_name = candidate_name;
	}

	public String getCandidate_email() {
		return candidate_email;
	}

	public void setCandidate_email(String candidate_email) {
		this.candidate_email = candidate_email;
	}

	public long getCandidate_mobilenumber() {
		return candidate_mobilenumber;
	}

	public void setCandidate_mobilenumber(long candidate_mobilenumber) {
		this.candidate_mobilenumber = candidate_mobilenumber;
	}

	public String getCandidate_resume() {
		return candidate_resume;
	}

	public void setCandidate_resume(String candidate_resume) {
		this.candidate_resume = candidate_resume;
	}

	public Job_Post getJob_Post() {
		return job_Post;
	}

	public void setJob_Post(Job_Post job_Post) {
		this.job_Post = job_Post;
	}

	private String candidate_resume;

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name = "job_id", nullable = false)

	private Job_Post job_Post=new Job_Post();
	
	
}
