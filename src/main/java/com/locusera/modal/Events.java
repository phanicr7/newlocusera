package com.locusera.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;
@Entity  
@Table(name="le_events")
@Proxy(lazy = false)
public class Events implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="event_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	private int event_id;
	private String event_title;
	private String event_logo;
	private String event_heading;
	private String event_content;
	public int getEvent_id() {
		return event_id;
	}
	public void setEvent_id(int event_id) {
		this.event_id = event_id;
	}
	public String getEvent_title() {
		return event_title;
	}
	public void setEvent_title(String event_title) {
		this.event_title = event_title;
	}
	public String getEvent_logo() {
		return event_logo;
	}
	public void setEvent_logo(String event_logo) {
		this.event_logo = event_logo;
	}
	public String getEvent_heading() {
		return event_heading;
	}
	public void setEvent_heading(String event_heading) {
		this.event_heading = event_heading;
	}
	public String getEvent_content() {
		return event_content;
	}
	public void setEvent_content(String event_content) {
		this.event_content = event_content;
	}
}
