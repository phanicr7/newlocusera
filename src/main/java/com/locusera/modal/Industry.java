package com.locusera.modal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="le_industry")
public class Industry {
	
	@Id
	@Column(name="industry_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private int industry_id;
	
	
	
	public int getIndustry_id() {
		return industry_id;
	}


	public void setIndustry_id(int industry_id) {
		this.industry_id = industry_id;
	}


	public String getIndustryname() {
		return industryname;
	}


	public void setIndustryname(String industryname) {
		this.industryname = industryname;
	}


	public List<SubIndustry> getSubindustry() {
		return subindustry;
	}


	public void setSubindustry(List<SubIndustry> subindustry) {
		this.subindustry = subindustry;
	}


	@Column(name="industryname")
	private String industryname;
	
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=SubIndustry.class,orphanRemoval = true,cascade=CascadeType.ALL)
	@JoinColumn(name="industry_id", referencedColumnName="industry_id")
	private List<SubIndustry> subindustry=new ArrayList<SubIndustry>();

}
