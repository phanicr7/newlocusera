package com.locusera.modal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="le_company_list")
public class Company {
	
	@Id
	@Column(name="company_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private int company_id;
	private String company_name;
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	private long company_number;
	private String company_email;
	private String company_logo;
	public String getCompany_logo() {
		return company_logo;
	}
	public void setCompany_logo(String company_logo) {
		this.company_logo = company_logo;
	}
	public int getCompany_id() {
		return company_id;
	}
	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	public List<CompanyAddress> getLocation() {
		return location;
	}
	public void setLocation(List<CompanyAddress> location) {
		this.location = location;
	}
	public long getCompany_number() {
		return company_number;
	}
	public void setCompany_number(long company_number) {
		this.company_number = company_number;
	}
	public String getCompany_email() {
		return company_email;
	}
	public void setCompany_email(String company_email) {
		this.company_email = company_email;
	}
	public String getAbout_us() {
		return about_us;
	}
	public void setAbout_us(String about_us) {
		this.about_us = about_us;
	}
	private String about_us;
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=CompanyAddress.class,orphanRemoval = true,cascade=CascadeType.ALL)
	@JoinColumn(name="company_id", referencedColumnName="company_id")
	private List<CompanyAddress> location=new ArrayList<CompanyAddress>();

}
