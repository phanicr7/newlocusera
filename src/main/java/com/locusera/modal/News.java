package com.locusera.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;
@SuppressWarnings("serial")
@Entity  
@Table(name="le_news") 
@Proxy(lazy = false)
public class News implements Serializable {
	@Id
	@Column(name="news_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	private int news_id;
	private String news_logo;
	private String news_title;
	private String news_heading;
	private String news_content;
	public int getNews_id() {
		return news_id;
	}
	public void setNews_id(int news_id) {
		this.news_id = news_id;
	}


	public String getNews_logo() {
		return news_logo;
	}
	public void setNews_logo(String news_logo) {
		this.news_logo = news_logo;
	}
	public String getNews_title() {
		return news_title;
	}
	public void setNews_title(String news_title) {
		this.news_title = news_title;
	}
	public String getNews_heading() {
		return news_heading;
	}
	public void setNews_heading(String news_heading) {
		this.news_heading = news_heading;
	}
	public String getNews_content() {
		return news_content;
	}
	public void setNews_content(String news_content) {
		this.news_content = news_content;
	}


}
