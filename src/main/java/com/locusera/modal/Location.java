package com.locusera.modal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="le_location_master")
public class Location 
{
	
	public List<CompanyAddress> getAddress() {
		return address;
	}


	public void setAddress(List<CompanyAddress> address) {
		this.address = address;
	}


	@Id
	@Column(name="location_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private int locationid ;
	

	public int getLocationid() {
		return locationid;
	}


	public void setLocationid(int locationid) {
		this.locationid = locationid;
	}


	public String getLocationname() {
		return locationname;
	}


	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}


	public City getCity() {
		return city;
	}


	

	public void setCity(City city) {
		this.city = city;
	}


	@Column(name="locationname")
	private String locationname;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name ="city_id", nullable = false)

	private City city=new City();
	
	
	


	
	
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=CompanyAddress.class,orphanRemoval = true,cascade=CascadeType.ALL)
	@JoinColumn(name="location_id", referencedColumnName="location_id")
	private List<CompanyAddress> address=new ArrayList<CompanyAddress>();
	
	
	
	
}
