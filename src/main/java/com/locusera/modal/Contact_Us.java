package com.locusera.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity  
@Table(name="le_contacus")  
public class Contact_Us {
	@Id
	@Column(name="contact_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	private int contact_id;
	public int getContact_id() {
		return contact_id;
	}
	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}
	public String getContact_email() {
		return contact_email;
	}
	public void setContact_email(String contact_email) {
		this.contact_email = contact_email;
	}
	public String getContact_subject() {
		return contact_subject;
	}
	public void setContact_subject(String contact_subject) {
		this.contact_subject = contact_subject;
	}
	public String getContact_message() {
		return contact_message;
	}
	public void setContact_message(String contact_message) {
		this.contact_message = contact_message;
	}
	private String  contact_email;
	private String  contact_subject;
	private String  contact_message;
	private String contact_name;
	public String getContact_name() {
		return contact_name;
	}
	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

}
