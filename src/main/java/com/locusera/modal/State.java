package com.locusera.modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="le_state_master")
public class State implements Serializable
{
 
	



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="state_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private int state_id;
	
	
	
	public int getState_id() {
		return state_id;
	}







	public void setState_id(int state_id) {
		this.state_id = state_id;
	}




	@Column(name="statename")
	private String statename;
	








	public String getStatename() {
		return statename;
	}







	public void setStatename(String statename) {
		this.statename = statename;
	}







	public List<City> getCity() {
		return city;
	}







	public void setCity(List<City> city) {
		this.city = city;
	}




	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=City.class,orphanRemoval = true,cascade=CascadeType.ALL)
	@JoinColumn(name="state_id", referencedColumnName="state_id")
	private List<City> city=new ArrayList<City>();


	
	
	
	
}
