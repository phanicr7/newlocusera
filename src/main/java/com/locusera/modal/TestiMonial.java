package com.locusera.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="le_testimonial")
public class TestiMonial {
	@Id
	@Column(name="testimonial_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	   private int testimonial_id;
	   public int getTestimonial_id() {
		return testimonial_id;
	}
	public void setTestimonial_id(int testimonial_id) {
		this.testimonial_id = testimonial_id;
	}
	public String getTestimonial_image() {
		return testimonial_image;
	}
	public void setTestimonial_image(String testimonial_image) {
		this.testimonial_image = testimonial_image;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getTestimonial_content() {
		return testimonial_content;
	}
	public void setTestimonial_content(String testimonial_content) {
		this.testimonial_content = testimonial_content;
	}
	private String testimonial_image;
	   private String customer_name;
	   private String testimonial_content;
	   

}
