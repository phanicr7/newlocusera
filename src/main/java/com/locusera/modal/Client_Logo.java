package com.locusera.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="le_clients_logo")
public class Client_Logo {
	@Id
	@Column(name="client_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private int client_id;
	public int getClient_id() {
		return client_id;
	}
	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}
	public String getClient_logo() {
		return client_logo;
	}
	public void setClient_logo(String client_logo) {
		this.client_logo = client_logo;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	private String client_logo;
	private String company_name;
	
	
	
	

}
