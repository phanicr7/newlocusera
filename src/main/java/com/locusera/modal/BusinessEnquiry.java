package com.locusera.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity  
@Table(name="le_business_enquiry")
public class BusinessEnquiry {
	
	@Id
	@Column(name="enquiry_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	private int enquiry_id;
	
	private String enquiry_name;
	private String enquiry_email;
	private String enquiry_description;
	private long enquiry_number;
	private int enquiry_locationid;
	private int enquiry_cityid;
	private int enquiry_stateid;
	public int getEnquiry_id() {
		return enquiry_id;
	}
	public void setEnquiry_id(int enquiry_id) {
		this.enquiry_id = enquiry_id;
	}
	public String getEnquiry_name() {
		return enquiry_name;
	}
	public void setEnquiry_name(String enquiry_name) {
		this.enquiry_name = enquiry_name;
	}
	public String getEnquiry_email() {
		return enquiry_email;
	}
	public void setEnquiry_email(String enquiry_email) {
		this.enquiry_email = enquiry_email;
	}
	public String getEnquiry_description() {
		return enquiry_description;
	}
	public void setEnquiry_description(String enquiry_description) {
		this.enquiry_description = enquiry_description;
	}
	public long getEnquiry_number() {
		return enquiry_number;
	}
	public void setEnquiry_number(long enquiry_number) {
		this.enquiry_number = enquiry_number;
	}
	public int getEnquiry_locationid() {
		return enquiry_locationid;
	}
	public void setEnquiry_locationid(int enquiry_locationid) {
		this.enquiry_locationid = enquiry_locationid;
	}
	public int getEnquiry_cityid() {
		return enquiry_cityid;
	}
	public void setEnquiry_cityid(int enquiry_cityid) {
		this.enquiry_cityid = enquiry_cityid;
	}
	public int getEnquiry_stateid() {
		return enquiry_stateid;
	}
	public void setEnquiry_stateid(int enquiry_stateid) {
		this.enquiry_stateid = enquiry_stateid;
	}
	
	
	
	
	

}
