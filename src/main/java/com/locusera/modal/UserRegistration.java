package com.locusera.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;



@Entity
@Table(name="le_registration")
public class UserRegistration {
	
	
	@Id
	@Column(name="le_user_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	private int le_user_id;
	
	private String le_user_firstname;
	public int getLe_user_id() {
		return le_user_id;
	}
	public void setLe_user_id(int le_user_id) {
		this.le_user_id = le_user_id;
	}
	private String le_user_lastname;
	private String le_username;
	private String le_user_email;
	private String le_user_pwd;
	private long  le_user_phone;
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime le_user_created_date;
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime le_user_modified_date;
	private String le_created_by;
	private String le_modified_by;
	private String ip_address;
	
	public String getLe_user_firstname() {
		return le_user_firstname;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ip_address == null) ? 0 : ip_address.hashCode());
		result = prime * result
				+ ((le_created_by == null) ? 0 : le_created_by.hashCode());
		result = prime * result + le_user_id;
		result = prime * result
				+ ((le_modified_by == null) ? 0 : le_modified_by.hashCode());
		result = prime
				* result
				+ ((le_user_created_date == null) ? 0 : le_user_created_date
						.hashCode());
		result = prime * result
				+ ((le_user_email == null) ? 0 : le_user_email.hashCode());
		result = prime
				* result
				+ ((le_user_firstname == null) ? 0 : le_user_firstname
						.hashCode());
		result = prime
				* result
				+ ((le_user_lastname == null) ? 0 : le_user_lastname.hashCode());
		result = prime
				* result
				+ ((le_user_modified_date == null) ? 0 : le_user_modified_date
						.hashCode());
		result = prime * result
				+ (int) (le_user_phone ^ (le_user_phone >>> 32));
		result = prime * result
				+ ((le_user_pwd == null) ? 0 : le_user_pwd.hashCode());
		result = prime * result
				+ ((le_username == null) ? 0 : le_username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRegistration other = (UserRegistration) obj;
		if (ip_address == null) {
			if (other.ip_address != null)
				return false;
		} else if (!ip_address.equals(other.ip_address))
			return false;
		if (le_created_by == null) {
			if (other.le_created_by != null)
				return false;
		} else if (!le_created_by.equals(other.le_created_by))
			return false;
		if (le_user_id != other.le_user_id)
			return false;
		if (le_modified_by == null) {
			if (other.le_modified_by != null)
				return false;
		} else if (!le_modified_by.equals(other.le_modified_by))
			return false;
		if (le_user_created_date == null) {
			if (other.le_user_created_date != null)
				return false;
		} else if (!le_user_created_date.equals(other.le_user_created_date))
			return false;
		if (le_user_email == null) {
			if (other.le_user_email != null)
				return false;
		} else if (!le_user_email.equals(other.le_user_email))
			return false;
		if (le_user_firstname == null) {
			if (other.le_user_firstname != null)
				return false;
		} else if (!le_user_firstname.equals(other.le_user_firstname))
			return false;
		if (le_user_lastname == null) {
			if (other.le_user_lastname != null)
				return false;
		} else if (!le_user_lastname.equals(other.le_user_lastname))
			return false;
		if (le_user_modified_date == null) {
			if (other.le_user_modified_date != null)
				return false;
		} else if (!le_user_modified_date.equals(other.le_user_modified_date))
			return false;
		if (le_user_phone != other.le_user_phone)
			return false;
		if (le_user_pwd == null) {
			if (other.le_user_pwd != null)
				return false;
		} else if (!le_user_pwd.equals(other.le_user_pwd))
			return false;
		if (le_username == null) {
			if (other.le_username != null)
				return false;
		} else if (!le_username.equals(other.le_username))
			return false;
		return true;
	}
	public void setLe_user_firstname(String le_user_firstname) {
		this.le_user_firstname = le_user_firstname;
	}
	public String getLe_user_lastname() {
		return le_user_lastname;
	}
	public void setLe_user_lastname(String le_user_lastname) {
		this.le_user_lastname = le_user_lastname;
	}
	public String getLe_username() {
		return le_username;
	}
	public void setLe_username(String le_username) {
		this.le_username = le_username;
	}
	public String getLe_user_email() {
		return le_user_email;
	}
	public void setLe_user_email(String le_user_email) {
		this.le_user_email = le_user_email;
	}
	public String getLe_user_pwd() {
		return le_user_pwd;
	}
	public void setLe_user_pwd(String le_user_pwd) {
		this.le_user_pwd = le_user_pwd;
	}
	public long getLe_user_phone() {
		return le_user_phone;
	}
	public void setLe_user_phone(long le_user_phone) {
		this.le_user_phone = le_user_phone;
	}

	public LocalDateTime getLe_user_created_date() {
		return le_user_created_date;
	}
	public void setLe_user_created_date(LocalDateTime le_user_created_date) {
		this.le_user_created_date = le_user_created_date;
	}
	public LocalDateTime getLe_user_modified_date() {
		return le_user_modified_date;
	}
	public void setLe_user_modified_date(LocalDateTime le_user_modified_date) {
		this.le_user_modified_date = le_user_modified_date;
	}
	public String getLe_created_by() {
		return le_created_by;
	}
	public void setLe_created_by(String le_created_by) {
		this.le_created_by = le_created_by;
	}
	public String getLe_modified_by() {
		return le_modified_by;
	}
	public void setLe_modified_by(String le_modified_by) {
		this.le_modified_by = le_modified_by;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	
	

}

