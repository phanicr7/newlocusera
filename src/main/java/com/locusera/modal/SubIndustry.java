package com.locusera.modal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="le_subindustry")
public class SubIndustry {
	
	@Id
	@Column(name="subindustry_id")
	@GenericGenerator(name="generator" , strategy="increment")
	@GeneratedValue(generator="generator")
	private int subindustry_id;
	
	
	
	@Column(name="subindustryname")
	private String subindustryname;
	

	public int getSubindustry_id() {
		return subindustry_id;
	}





	public List<Job_Post> getJobs() {
		return jobs;
	}





	public void setJobs(List<Job_Post> jobs) {
		this.jobs = jobs;
	}





	public void setSubindustry_id(int subindustry_id) {
		this.subindustry_id = subindustry_id;
	}


	public String getSubindustryname() {
		return subindustryname;
	}


	public void setSubindustryname(String subindustryname) {
		this.subindustryname = subindustryname;
	}


	public Industry getIndustry() {
		return industry;
	}


	public void setIndustry(Industry industry) {
		this.industry = industry;
	}


	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name = "industry_id", nullable = false)

	private Industry industry=new Industry();
	
	
	
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=Job_Post.class,orphanRemoval = true,cascade=CascadeType.ALL)
	@JoinColumn(name="subindustry_id", referencedColumnName="subindustry_id")
	private List<Job_Post> jobs=new ArrayList<Job_Post>();
	

}
