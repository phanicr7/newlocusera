package com.locusera.modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
@Entity  
@Table(name="le_jobs_list")  
public class Job_Post implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
    private int job_id; 
	
	private int no_openings;
	private String qualification_details;
	private String Skills_required;
    private String jobtitle;
    private String experience;
    private String job_status;
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime applied_as;
    
    private String applied;
  



	public LocalDateTime getApplied_as() {
		return applied_as;
	}

	public void setApplied_as(LocalDateTime applied_as) {
		this.applied_as = applied_as;
	}

	public String getApplied() {
		return applied;
	}

	public void setApplied(String applied) {
		this.applied = applied;
	}

	public String getJob_status() {
		return job_status;
	}

	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public int getJob_id() {
		return job_id;
	}

	public void setJob_id(int job_id) {
		this.job_id = job_id;
	}

public String getJobtitle() {
		return jobtitle;
	}

	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}

public String getSkills_required() {
		return Skills_required;
	}

	public void setSkills_required(String skills_required) {
		Skills_required = skills_required;
	}

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name = "subindustry_id", nullable = false)

	private SubIndustry subindustry_id=new SubIndustry();


	public int getNo_openings() {
		return no_openings;
	}

	public void setNo_openings(int no_openings) {
		this.no_openings = no_openings;
	}

	public String getQualification_details() {
		return qualification_details;
	}

	public void setQualification_details(String qualification_details) {
		this.qualification_details = qualification_details;
	}

	public SubIndustry getSubindustry_id() {
		return subindustry_id;
	}

	public void setSubindustry_id(SubIndustry subindustry_id) {
		this.subindustry_id = subindustry_id;
	}

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=Job_Applications.class,cascade=CascadeType.ALL)
	@JoinColumn(name="job_id", referencedColumnName="job_id")
	private List<Job_Applications> jobapplication=new ArrayList<Job_Applications>();
	public List<Job_Applications> getJobapplication() {
		return jobapplication;
	}
	public void setJobapplication(List<Job_Applications> jobapplication) {
		this.jobapplication = jobapplication;
	}
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name = "address_id", nullable = false)

	private CompanyAddress address=new CompanyAddress();

	public CompanyAddress getAddress() {
		return address;
	}

	public void setAddress(CompanyAddress address) {
		this.address = address;
	}

}
