package com.locusera.modal;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity  
@Table(name="le_blog_articals")  
public class BlogArticals implements Serializable {
	@Override
	public String toString() {
		return "BlogArticals [blog_id=" + blog_id + ", posteddate="
				+ posteddate + ", writer_name=" + writer_name
				+ ", artical_title=" + artical_title + ", artical_heading="
				+ artical_heading + ", articalwriter_info="
				+ articalwriter_info + ", artical_logo_path="
				+ artical_logo_path + ", artical_content=" + artical_content
				+ "]";
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="blog_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	private int  blog_id;
	private Timestamp posteddate;
	public Timestamp getPosteddate() {
		return posteddate;
	}
	public void setPosteddate(Timestamp posteddate) {
		this.posteddate = posteddate;
	}
	private String writer_name;
	private String  artical_title;
	private String artical_heading;
	private String articalwriter_info;
	private String artical_logo_path;
	public int getBlog_id() {
		return blog_id;
	}
	public void setBlog_id(int blog_id) {
		this.blog_id = blog_id;
	}
	public String getWriter_name() {
		return writer_name;
	}
	public void setWriter_name(String writer_name) {
		this.writer_name = writer_name;
	}
	public String getArtical_title() {
		return artical_title;
	}
	public void setArtical_title(String artical_title) {
		this.artical_title = artical_title;
	}
	public String getArtical_heading() {
		return artical_heading;
	}
	public void setArtical_heading(String artical_heading) {
		this.artical_heading = artical_heading;
	}
	public String getArticalwriter_info() {
		return articalwriter_info;
	}
	public void setArticalwriter_info(String articalwriter_info) {
		this.articalwriter_info = articalwriter_info;
	}
	public String getArtical_logo_path() {
		return artical_logo_path;
	}
	public void setArtical_logo_path(String artical_logo_path) {
		this.artical_logo_path = artical_logo_path;
	}
	public String getArtical_content() {
		return artical_content;
	}
	public void setArtical_content(String artical_content) {
		this.artical_content = artical_content;
	}
	private String artical_content;
}
