package com.locusera.daoImpl;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.config.SetFactoryBean;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.locusera.command.ArticalCommand;
import com.locusera.command.Citycommand;
import com.locusera.command.ClientLogoCommand;
import com.locusera.command.CompanyAddressCommand;
import com.locusera.command.CompanyCommand;
import com.locusera.command.EventCommand;
import com.locusera.command.IndustryCommand;
import com.locusera.command.JobCommand;
import com.locusera.command.LocationCommand;
import com.locusera.command.NewsCommand;
import com.locusera.command.ProfileCommand;
import com.locusera.command.Statecommand;
import com.locusera.command.SubIndustryCommand;
import com.locusera.dao.AdminLoginDao;
import com.locusera.dao.EnquiryDao;
import com.locusera.dao.JobsDao;
import com.locusera.dao.ResumeDataDao;
import com.locusera.modal.AdminRegistration;
import com.locusera.modal.BlogArticals;
import com.locusera.modal.City;
import com.locusera.modal.Client_Logo;
import com.locusera.modal.Company;
import com.locusera.modal.CompanyAddress;
import com.locusera.modal.Contact_Us;
import com.locusera.modal.Events;
import com.locusera.modal.Industry;
import com.locusera.modal.Job_Applications;
import com.locusera.modal.Job_Post;
import com.locusera.modal.Location;
import com.locusera.modal.News;
import com.locusera.modal.State;
import com.locusera.modal.SubIndustry;
import com.locusera.modal.TestiMonial;
import com.locusera.modal.UserRegistration;
@Transactional
@SuppressWarnings("unchecked")
public class AdminLoginDaoImpl  extends HibernateDaoSupport implements AdminLoginDao {

	@Override
	public AdminRegistration login(String username, String password) {
		AdminRegistration results=null;
	
		try {
			Query qry=getSessionFactory().getCurrentSession().createQuery("from AdminRegistration eb where eb.le_name=:username and eb.le_pws =:password");
			qry.setParameter("username",username);
			qry.setParameter("password",password);
			List<AdminRegistration> data=qry.list();
			
			int size=data.size();
			
			if(size>0)
			{
				results=data.get(0);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		
		return results;
	}

	@Override
	public int saveartical(BlogArticals artical) {
		
		
		return (Integer)getSessionFactory().getCurrentSession().save(artical);
	}

	@Override
	public int savenews(News news) {
	
		return (Integer)getSessionFactory().getCurrentSession().save(news);
	}

	@Override
	public int saveevent(Events event) {
		
		return (Integer)getSessionFactory().getCurrentSession().save(event);
	}

	@Override
	public int savejob(Job_Post job) {
		return (Integer)getSessionFactory().getCurrentSession().save(job);
	}

	@Override
	public int savesate(State state) {
		return (Integer)getSessionFactory().getCurrentSession().save(state);
	}

	@Override
	public List<State> getStates() {
		Query qry=getSessionFactory().getCurrentSession().createQuery("from State");
		
		List<State> states=qry.list();
		return states;
	}

	@Override
	public int savecity(Citycommand command) {
		Session session=getSessionFactory().getCurrentSession();
		City city=new City();
		city.setState((State)session.get(State.class,command.getState_id()));
		city.setCityname(command.getCity());
		
		
		return (Integer)session.save(city);
	}

	@Override
	public Map<Integer,String> getcitys(int stateid) {
		Map<Integer,String> citys=new HashMap<Integer,String>();
		
		try {
			String sql = "SELECT * FROM locusera.le_city_master where state_id=:id";
			SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
			
			query.setParameter("id", stateid);
			List<Object[]> results = query.list();
			for(Object[] object:results)
			{
				System.out.println((Integer)object[0]);
				System.out.println((String)object[1]);
				citys.put((Integer)object[0],(String)object[1]);
				
				
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		
		return citys;
	}

	@Override
	public int savelocation(LocationCommand command) {
		
		Session session=getSessionFactory().getCurrentSession();
		Location location=new Location();
		location.setCity((City)session.get(City.class,command.getCity()));
		location.setLocationname(command.getLocation());
		return (Integer)session.save(location);
	}

	@Override
	public Map<Integer, String> getlocations(int cityid) {
Map<Integer,String> locations=new HashMap<Integer,String>();
		
		try {
			String sql = "SELECT * FROM locusera.le_location_master where city_id=:id";
			SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
			
			query.setParameter("id", cityid);
			List<Object[]> results = query.list();
			for(Object[] object:results)
			{
				
				locations.put((Integer)object[0],(String)object[1]);
				
				
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		
		return locations;
	}

	@Override
	public AdminRegistration updateprofileinfo(int id,ProfileCommand command) {
		Session session=getSessionFactory().getCurrentSession();
		AdminRegistration data=(AdminRegistration)session.get(AdminRegistration.class,id);
		data.setLe_name(command.getName());
		data.setLe_phone_number(command.getNumber());
		data.setLe_email(command.getEmail());
		session.update(data);
		
		return data;
	}

	@Override
	public AdminRegistration updatepassword(int id,String pwd) {
		
		Session session=getSessionFactory().getCurrentSession();
		AdminRegistration data=(AdminRegistration)session.get(AdminRegistration.class,id);
		data.setLe_pws(pwd);
		
		session.update(data);
		return data;
	}

	@Override
	public List<UserRegistration> getusers() {
        Query qry=getSessionFactory().getCurrentSession().createQuery("from UserRegistration");
		
		List<UserRegistration> users=qry.list();
		  
		return users;
	}

	@Override
	public int saveindustry(Industry commnand) {
		return (Integer)getSessionFactory().getCurrentSession().save(commnand);
	}

	@Override
	public List<Industry> getIndustrys() {
		
       Query qry=getSessionFactory().getCurrentSession().createQuery("from Industry");
		
		List<Industry> industrys=qry.list();
		return industrys;
	}

	@Override
	public int savesubindustry(SubIndustryCommand command) {
		
		Session session=getSessionFactory().getCurrentSession();
		SubIndustry industry=new SubIndustry();
		industry.setIndustry((Industry)session.get(Industry.class,command.getId()));
		industry.setSubindustryname(command.getName());
		return (Integer)session.save(industry);
		
	}

	@Override
	public Map<Integer, String> getjobs(int id) {
		
		
Map<Integer,String> jobs=new HashMap<Integer,String>();
		
		try {
			String sql = "SELECT * FROM locusera.le_subindustry where industry_id=:id";
			SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
			
			query.setParameter("id", id);
			List<Object[]> results = query.list();
			for(Object[] object:results)
			{
			
				 jobs.put((Integer)object[0],(String)object[1]);
				
				
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return jobs;
		
		
		
		
		
	}

	@Override
	public List<ResumeDataDao> getResumes() {
		
		
          Query qry=getSessionFactory().getCurrentSession().createQuery("from Job_Applications");
		
		  List<Job_Applications> applications=qry.list();
		  
		  List<ResumeDataDao> userdao=new ArrayList<ResumeDataDao>();
		  
		  
		  for(Job_Applications application:applications)
		  {
			  Job_Post post=application.getJob_Post();
			  
			  ResumeDataDao dao=new ResumeDataDao();
			  dao.setName(application.getCandidate_name());
			  dao.setNumber(application.getCandidate_mobilenumber());
			  dao.setJobtype(post.getSubindustry_id().getSubindustryname());
			  dao.setCatogory(post.getSubindustry_id().getIndustry().getIndustryname());
			  dao.setCompanyname(post.getAddress().getCompany().getCompany_name());
			  dao.setResumeurl(application.getCandidate_resume());
			  
			  userdao.add(dao);
		  }
		
		
		
		
		
		
		
	/*	String sql = "select  a.candidate_name,a.candidate_mobilenumber,i.subindustryname,j.company_name,a.candidate_resume  from le_job_applications  a,"
				+ "le_jobs_list j ,le_subindustry i where  a.subindustry_type=i.subindustry_id and a.apply_company_id=j.job_id";
		SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
		
		
		List<Object[]> results = query.list();*/
		return userdao;
	}

	@Override
	public Location getLocation(int id) {
		Session session=getSessionFactory().getCurrentSession();
		
		return (Location)session.get(Location.class,id);
	}

	@Override
	public SubIndustry getsubindustry(int id) {
		
		Session session=getSessionFactory().getCurrentSession();
		
		return (SubIndustry)session.get(SubIndustry.class,id);
	}

	@Override
	public int savecompany(Company company) {
		return (Integer)getSessionFactory().getCurrentSession().save(company);
	}

	@Override
	public List<Company> getCompanys() {
           Query qry=getSessionFactory().getCurrentSession().createQuery("from Company");
		
		List<Company> users=qry.list();
		return users;
	}

	@Override
	public int address(CompanyAddressCommand command) {
		
		Session session=getSessionFactory().getCurrentSession();
		
		
		CompanyAddress address=new CompanyAddress();
		address.setAddress(command.getInformation());
		address.setCompany((Company)session.get(Company.class,command.getCompanyname()));
		address.setLocation((Location)session.get(Location.class,command.getLocationname()));
		
		return (Integer)session.save(address);
		
		
	}

	@Override
	public Map<Integer, String> getcompanylocations(int id) {
		Session session=getSessionFactory().getCurrentSession();
		Company company=(Company)session.get(Company.class,id);
		Map<Integer,String> locations=new HashMap<Integer,String>();
		
		List<CompanyAddress> address=company.getLocation();
		try {
			for(CompanyAddress companyaddress:address)
			{
				String locationname=companyaddress.getLocation().getLocationname();
				String cityname=companyaddress.getLocation().getCity().getCityname();
				String statename=companyaddress.getLocation().getCity().getState().getStatename();
				
				locations.put(companyaddress.getAddress_id(), statename+","+cityname+","+locationname);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		
		return locations;
	}

	@Override
	public CompanyAddress getCompanyAddress(int id) {
		
         Session session=getSessionFactory().getCurrentSession();
		
		return (CompanyAddress)session.get(CompanyAddress.class,id);
		
	}

	@Override
	public List<Events> getEventList() {
		
		Query query = getSessionFactory().getCurrentSession().createQuery("from Events");
		List<Events> list =query.list();
		return list;
	}

	@Override
	public List<News> getNewsList() {
		
		Query query = getSessionFactory().getCurrentSession().createQuery("from News");
		List<News> list =query.list();
		return list;
	}
	public List<JobsDao> getjobs() {
		
		 Query qry=getSessionFactory().getCurrentSession().createQuery("from Job_Post");
			
			List<Job_Post> users=qry.list();
			
			List<JobsDao> jobs=new ArrayList<JobsDao>();
			try {
				for(Job_Post job:users)
				{
					JobsDao dao=new JobsDao();
					dao.setJob_id(job.getJob_id());
					dao.setJobtitle(job.getJobtitle());
					dao.setCompanyname(job.getAddress().getCompany().getCompany_name());
					dao.setJobrole(job.getSubindustry_id().getSubindustryname());
					dao.setLocationname(job.getAddress().getLocation().getLocationname());
					dao.setOpenings(job.getNo_openings());
					dao.setQualification(job.getQualification_details());
					dao.setSkills(job.getSkills_required());
					dao.setJob_status(job.getJob_status());
					
					jobs.add(dao);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		return jobs;
	}

	@Override
	public List<EnquiryDao> getEnquirydetails() {
		
		  String sql="SELECT e.enquiry_id,e.enquiry_name,e.enquiry_email,e.enquiry_number,l. locationname,s.statename,e.enquiry_description from locusera.le_business_enquiry e,locusera.le_location_master  l,"
				+ "locusera.le_state_master s  where  e.enquiry_locationid=l.location_id and e.enquiry_stateid=s.state_id";
		
          SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
		
		
		  List<Object[]> results = query.list();
		  List<EnquiryDao> daolist=new ArrayList<EnquiryDao>();
		  try {
			for(Object[] obj:results)
			  {
				  EnquiryDao dao=new EnquiryDao();
				  dao.setEnquiry_id((Integer)obj[0]);
				  dao.setName((String)obj[1]);
				  dao.setEmail((String)obj[2]);
				  dao.setNumber((String)obj[3].toString());
				  dao.setLocation((String)obj[4]);
				  dao.setState((String)obj[5]);
				  dao.setDes((String)obj[6]);
				  daolist.add(dao);
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return daolist;
	}

	@Override
	public List<BlogArticals> getArticals() {
		

		 Query qry=getSessionFactory().getCurrentSession().createQuery("from BlogArticals");
			
			List<BlogArticals> users=qry.list();
		return users;
	}


	@Override
	public  List<Object[]> getdetails(int id) {
		
		 String sql="SELECT candidate_email,candidate_mobilenumber FROM locusera.le_job_applications where job_id in(SELECT job_id FROM locusera.le_jobs_list where subindustry_id=:id)";
			
	          SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
			  query.setParameter("id", id);
			
			  List<Object[]> results = query.list();
			  
		     
		
		
		return results;
	}

	@Override
	public News getNewsById(int news_id) {
		News news = (News) getSessionFactory().getCurrentSession().load(News.class,news_id);
		return news;
	}

	@Override
	public Events getEventById(int event_id) {
		
		Events event = (Events) getSessionFactory().getCurrentSession().load(Events.class,event_id);
		return event;
	}
	public int saveTestimonial(TestiMonial monial) {
		return (Integer)getSessionFactory().getCurrentSession().save(monial);
	}

	@Override
	public List<TestiMonial> getTestimonialsList() {
		Query qry=getSessionFactory().getCurrentSession().createQuery("from TestiMonial");
		
		List<TestiMonial> list=qry.list();
		return list;
	}

	@Override
	public int savecontactus(Contact_Us contact) {
		return (Integer)getSessionFactory().getCurrentSession().save(contact);
	}

	@Override
	public BlogArticals getBlogById(int blog_id) {
	 BlogArticals articals = (BlogArticals) getSessionFactory().getCurrentSession().get(BlogArticals.class, blog_id);
		return articals;
	}

	@Override
	public List<Contact_Us> getConatactUsDetails() {
		Query query = getSessionFactory().getCurrentSession().createQuery("from Contact_Us");
		List<Contact_Us> list =query.list();
		return list;
	}

	@Override
	public List<Client_Logo> getClienttLogos() {
		Query query = getSessionFactory().getCurrentSession().createQuery("from Client_Logo");
		List<Client_Logo> list =query.list();
		return list;
	}

	@Override
	public int saveClientLogo(Client_Logo logo) {
		return (Integer)getSessionFactory().getCurrentSession().save(logo);
	}

	@Override
	public boolean deleteBlog(int id) {
	
	   boolean flag=false;
		try {
			BlogArticals articals=(BlogArticals) getSessionFactory().getCurrentSession().load(BlogArticals.class,id);
			getSessionFactory().getCurrentSession().delete(articals);
			flag=true;
		} 
		catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return flag;
	}

	@Override
	public boolean deleteNews(int id) {
		
		
		boolean flag=false;
		
		try {
			News news =(News) getSessionFactory().getCurrentSession().load(News.class, id);
			getSessionFactory().getCurrentSession().delete(news);
			
			flag=true;
		} catch (HibernateException e) {
			
			e.printStackTrace();
		}

		
		
		return flag;
	}

	
	@Override
	public boolean deleteClient(int id) {
		boolean flag=false;
		
		
		try {
			Client_Logo client=(Client_Logo) getSessionFactory().getCurrentSession().load(Client_Logo.class, id);
			
			getSessionFactory().getCurrentSession().delete(client);
			flag=true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return flag;
	}

	@Override
	public boolean deleteEvent(int id) {
		boolean flag=false;
		
		
		try {
			Events event=(Events) getSessionFactory().getCurrentSession().load(Events.class, id);
			
			getSessionFactory().getCurrentSession().delete(event);
			flag=true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return flag;
	}
	@Override
	public boolean deletEnquiry(int id) {
	boolean flag=false;
		
		
	try {
		Query query = getSessionFactory().getCurrentSession().createQuery("delete from BusinessEnquiry where enquiry_id=:eid");
		
		query.setParameter("eid", id);
		query.executeUpdate();
		flag=true;
	} catch (HibernateException e) {
		e.printStackTrace();
	}
	
	return flag;
}

	@Override
	public boolean deleteJob(int id) {
boolean flag=false;
		
		
		try {
			Query query = getSessionFactory().getCurrentSession().createQuery("delete from Job_Post where job_id=:m");
			
			query.setParameter("m", id);
			query.executeUpdate();
			flag=true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return flag;
	}



	@Override
	public BlogArticals editArticle(int articalId) {
		BlogArticals blog=null;
		try {
			 blog=(BlogArticals) getSessionFactory().getCurrentSession().load(BlogArticals.class, articalId);
			 
			 System.out.println(blog.getArtical_heading());
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		 
		return blog;
	}

	@Override
	public boolean updateArticle(ArticalCommand articalCommand) {
		
		
		BlogArticals articals = (BlogArticals) getSessionFactory().getCurrentSession().load(BlogArticals.class, articalCommand.getBlogid());
		
		try {
			articals.setArtical_content(articalCommand.getArt_content());
			articals.setArtical_heading(articalCommand.getHeading());
			articals.setArtical_title(articalCommand.getTitle());
			articals.setArticalwriter_info(articalCommand.getInfo());
			articals.setWriter_name(articalCommand.getWritername());
			if(articalCommand.getNewpath()!=null){
				articals.setArtical_logo_path(articalCommand.getNewpath());
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		getSessionFactory().getCurrentSession().update(articals);
		
		
		
		return false;
	}

	@Override
	public News editNews(int id) {
		News news=null;
		try {
			news=(News) getSessionFactory().getCurrentSession().load(News.class, id);
			 
			 System.out.println(news.getNews_heading());
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		 
		return news;
	}

	@Override
	public boolean updateNews(NewsCommand newsCommand) {
		
		boolean flag=false;
		try{
      News news =(News) getSessionFactory().getCurrentSession().load(News.class,newsCommand.getNewsid());
      
      news.setNews_content(newsCommand.getNews_content());
      news.setNews_heading(newsCommand.getHeading());
      news.setNews_title(newsCommand.getTitle());
     
  	   if(newsCommand.getNewpath()!=null){
		news.setNews_logo(newsCommand.getNewpath());
		
	}
	
	getSessionFactory().getCurrentSession().update(news);
	
	flag=true;
		}
		catch(Exception exception){
			
			exception.printStackTrace();
		}
		return flag;
	}

	@Override
	public Events editEvents(int id) {
		Events events=null;
		try {
			events=(Events) getSessionFactory().getCurrentSession().load(Events.class, id);
			 
			 System.out.println(events.getEvent_title());
					 
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		 
		return events;
	}

	@Override
	public boolean updateEvent(EventCommand eventCommand) {
		
		
		boolean flag=false;
		try{
      Events events =(Events) getSessionFactory().getCurrentSession().load(Events.class,eventCommand.getEventid());
      
      events.setEvent_content(eventCommand.getEventcontent());
      events.setEvent_heading(eventCommand.getHeading());
      events.setEvent_title(eventCommand.getTitle());
     
  	   if(eventCommand.getNewpath()!=null){
  		 events.setEvent_logo(eventCommand.getNewpath());
		
	}
	
	getSessionFactory().getCurrentSession().update(events);
	
	flag=true;
		}
		catch(Exception exception){
			
			exception.printStackTrace();
		}
		return flag;
	}

	@Override
	public Job_Post editJob(int id) {
		
		Job_Post job = null;
		try {
			job = null;
			
				job=(Job_Post) getSessionFactory().getCurrentSession().load(Job_Post.class, id);
				 
				 System.out.println(job.getJobtitle());
				
		} catch (HibernateException e) {
			e.printStackTrace();
			
			
		}
		return job;
	}
	
	

	@Override
	public Client_Logo editClient(int id) {
		Client_Logo logo=null;
		try {
			logo=(Client_Logo) getSessionFactory().getCurrentSession().load(Client_Logo.class, id);
			 
			 System.out.println(logo.getCompany_name());
			
		} catch (Exception e) {
			
			System.out.println(e);
		}
		 
		return logo;
	}

	@Override
	public boolean updateClient(ClientLogoCommand clientCommand) {
		
		boolean flag=false;
		try{
      Client_Logo logo =(Client_Logo) getSessionFactory().getCurrentSession().load(Client_Logo.class,clientCommand.getClientid());
        logo.setClient_id(clientCommand.getClientid());
        logo.setCompany_name(clientCommand.getName());
     
  	   if(clientCommand.getNewpath()!=null){
  		 logo.setClient_logo(clientCommand.getNewpath());
		
	}
	
	getSessionFactory().getCurrentSession().update(logo);
	
	flag=true;
		}
		catch(Exception exception){
			
			exception.printStackTrace();
		}
		return flag;
	}

	@Override
	public List<Industry> getIndustryList() {
		
			
	       Query qry=getSessionFactory().getCurrentSession().createQuery("from Industry");
			
			List<Industry> industries =qry.list();
			
			return industries;
		}
							
			@Override
		public List<ResumeDataDao> getResumesByIndustry(int id) {
		
				List<ResumeDataDao> resumeDataDaos=new ArrayList<ResumeDataDao>();
				String sql = "from Industry where industry_id=:id";
				Query query = getSessionFactory().getCurrentSession().createQuery(sql);
				query.setParameter("id", id);
				List<Industry> industries= query.list();
				
				for(Industry industry : industries){
					
					List<SubIndustry> subIndustry = industry.getSubindustry();
					
					for(SubIndustry subInd :subIndustry){
						
						
						List<Job_Post> job_Posts = subInd.getJobs();
						
						for(Job_Post job_Post : job_Posts){
														
							List<Job_Applications> applications = job_Post.getJobapplication();							  
							  
							  for(Job_Applications application:applications)
							  {
								  Job_Post post=application.getJob_Post();
								  
								  ResumeDataDao dao=new ResumeDataDao();
								  dao.setName(application.getCandidate_name());
								  dao.setNumber(application.getCandidate_mobilenumber());
								  dao.setJobtype(post.getSubindustry_id().getSubindustryname());
								  dao.setCatogory(post.getSubindustry_id().getIndustry().getIndustryname());
								  dao.setCompanyname(post.getAddress().getCompany().getCompany_name());
								  dao.setResumeurl(application.getCandidate_resume());
								  
								  resumeDataDaos.add(dao);
							  }
									
						}						
						
					}			
					
				}
				
				
				return resumeDataDaos;
	}

	@Override
	public List<ResumeDataDao> getResumesBySubInd(List<Integer> allIds) {

		List<ResumeDataDao> resumeDataDaos=new ArrayList<ResumeDataDao>();
		Query query = getSessionFactory().getCurrentSession().createQuery("from SubIndustry where subindustry_id in (:ids)");
		query.setParameterList("ids", allIds);
		List<SubIndustry> subIndustries= query.list();
				
		for(SubIndustry subIndustry :subIndustries){
			
			
			List<Job_Post> job_Posts = subIndustry.getJobs();
			
			
			for(Job_Post job_Post : job_Posts){
				
				List<Job_Applications> applications = job_Post.getJobapplication();
				
				  for(Job_Applications application:applications)
				  {
					  Job_Post post=application.getJob_Post();
					  
					  ResumeDataDao dao=new ResumeDataDao();
					  dao.setName(application.getCandidate_name());
					  dao.setNumber(application.getCandidate_mobilenumber());
					  dao.setJobtype(post.getSubindustry_id().getSubindustryname());
					  dao.setCatogory(post.getSubindustry_id().getIndustry().getIndustryname());
					  dao.setCompanyname(post.getAddress().getCompany().getCompany_name());
					  dao.setResumeurl(application.getCandidate_resume());
					  
					  resumeDataDaos.add(dao);
				  }
			}
		}
		return resumeDataDaos;
		
	}

	@Override
	public List<ResumeDataDao> getResumesByDate(Date fromDate,Date toDate) {
		List<ResumeDataDao> resumeDataDaos=new ArrayList<ResumeDataDao>();
		try{
		String sql = "FROM Job_Applications where CAST(applied_date as date) BETWEEN :fromdate and :toDate";
		Query query = getSessionFactory().getCurrentSession().createQuery(sql);
		query.setParameter("fromdate", fromDate);
		query.setParameter("toDate", toDate);
		List<Job_Applications> applications= query.list();
				  for(Job_Applications application:applications)
				  {
					  Job_Post post=application.getJob_Post();
					  ResumeDataDao dao=new ResumeDataDao();
					  dao.setName(application.getCandidate_name());
					  dao.setNumber(application.getCandidate_mobilenumber());
					  dao.setJobtype(post.getSubindustry_id().getSubindustryname());
					  dao.setCatogory(post.getSubindustry_id().getIndustry().getIndustryname());
					  dao.setCompanyname(post.getAddress().getCompany().getCompany_name());
					  dao.setResumeurl(application.getCandidate_resume());
					 
					  resumeDataDaos.add(dao);
		}
		}
		catch(Exception exception){
			
			exception.printStackTrace();
		}
		return resumeDataDaos;
		
	}

	@Override
	public boolean updateJob(JobCommand jobCommand) {
		
		boolean flag=false;
		
      try {
		Job_Post job = (Job_Post) getSessionFactory().getCurrentSession().load(Job_Post.class,jobCommand.getJobid());
		    job.setJob_id(jobCommand.getJobid());
			job.setJobtitle(jobCommand.getJobtitle());
			job.setNo_openings(jobCommand.getOpenings());
			job.setExperience(jobCommand.getExperience());
			job.setQualification_details(jobCommand.getQualification());
			job.setSkills_required(jobCommand.getSkills());
			job.setJob_status(jobCommand.getJob_status());
		    job.setSubindustry_id(getsubindustry(jobCommand.getSubcatogory()));
		    job.setAddress(getCompanyAddress(jobCommand.getCompanyname()));
		  getSessionFactory().getCurrentSession().update(job);
	} catch (HibernateException e) {
		e.printStackTrace();
	}
	   flag=true;
		return flag;
	}

	@Override
	public List<LocationCommand> getLocations() {
		
		List<LocationCommand> commands = new ArrayList<LocationCommand>();
		
 		Query qry=getSessionFactory().getCurrentSession().createQuery("from Location");
		
		List<Location> locations=qry.list();
		
		for(Location lc: locations){
			LocationCommand locationCommand = new LocationCommand();
			locationCommand.setLocation_id(lc.getLocationid());
			locationCommand.setLocation(lc.getLocationname());
			City city = lc.getCity();
			locationCommand.setCityname(city.getCityname());
			State state = city.getState();
			locationCommand.setStatename(state.getStatename());
			commands.add(locationCommand);
		}
		
		
		return commands;
	}

	@Override
	public List<LocationCommand> getCity() {
List<LocationCommand> commands = new ArrayList<LocationCommand>();
		
 		Query qry=getSessionFactory().getCurrentSession().createQuery("from City");
		
		List<City> cities=qry.list();
		
		for(City lc: cities){
			LocationCommand locationCommand = new LocationCommand();
			locationCommand.setCity_id(lc.getCityid());
			locationCommand.setCityname(lc.getCityname());	
			State state = lc.getState();
			locationCommand.setStatename(state.getStatename());
			
			
			commands.add(locationCommand);
		}
		
		
		return commands;
	}

	@Override
	public List<SubIndustryCommand> getSubIndustryList() {
Query qry=getSessionFactory().getCurrentSession().createQuery("from SubIndustry");
		

List<SubIndustryCommand> commands = new ArrayList<SubIndustryCommand>();
		List<SubIndustry> subindustries=qry.list();
		
		for(SubIndustry subIndustry : subindustries){
			
			SubIndustryCommand industryCommand = new SubIndustryCommand();
			
			industryCommand.setId(subIndustry.getSubindustry_id());
			industryCommand.setName(subIndustry.getSubindustryname());
			Industry industry = subIndustry.getIndustry();
			industryCommand.setIndustry_name(industry.getIndustryname());
			commands.add(industryCommand);
		}
		
		return commands;
	}

	@Override
	public boolean deleteState(int id) {
		
		boolean flag=false;
		
		try {
			String sql = "delete from State where state_id=:id";
			
			Query query = getSessionFactory().getCurrentSession().createQuery(sql);
			query.setParameter("id", id);
			
			int status = query.executeUpdate();
			System.out.println(status);
			flag=true;
		} catch (HibernateException e) {
			
			e.printStackTrace();
		}

		
		
		return flag;
	}



	@Override
	public boolean deleteCity(int id) {
		boolean flag=false;
		
		try {
			String sql = "delete from City where city_id=:id";
			
			Query query = getSessionFactory().getCurrentSession().createQuery(sql);
			query.setParameter("id", id);
			
			int status = query.executeUpdate();
			System.out.println(status);
			flag=true;
		} catch (HibernateException e) {
			
			e.printStackTrace();
		}
			
			return flag;
		}

	@Override
	public boolean deleteLocation(int id) {

		boolean flag=false;
		
		try {
			String sql = "delete from Location where location_id=:id";
			
			Query query = getSessionFactory().getCurrentSession().createQuery(sql);
			query.setParameter("id", id);
			
			int status = query.executeUpdate();
			System.out.println(status);
			flag=true;
		} catch (HibernateException e) {
			
			e.printStackTrace();
		}
			
			return flag;
		}

	@Override
	public boolean deleteIndustry(int id) {
    boolean flag=false;
		try {
			String sql = "delete from Industry where industry_id=:id";
			
			Query query = getSessionFactory().getCurrentSession().createQuery(sql);
			query.setParameter("id", id);
			
			int status = query.executeUpdate();
			System.out.println(status);
			flag=true;
		} catch (HibernateException e) {
			
			e.printStackTrace();
		}
			
			return flag;
		}

	@Override
	public boolean deleteCompany(int id) {
		 boolean flag=false;
			try {
				String sql = "delete from Company where company_id=:id";
				
				Query query = getSessionFactory().getCurrentSession().createQuery(sql);
				query.setParameter("id", id);
				
				int status = query.executeUpdate();
				System.out.println(status);
				flag=true;
			} catch (HibernateException e) {
				
				e.printStackTrace();
			}
				
				return flag;
			}

	@Override
	public boolean deleteSubIndustry(int id) {
		 boolean flag=false;
			try {
				String sql = "delete from SubIndustry where subindustry_id=:id";
				
				Query query = getSessionFactory().getCurrentSession().createQuery(sql);
				query.setParameter("id", id);
				
				int status = query.executeUpdate();
				System.out.println(status);
				flag=true;
			} catch (HibernateException e) {
				
				e.printStackTrace();
			}
				
				return flag;
			}
	@Override
	public State editState(int id) {
		State state=null;
		try {
			state=(State) getSessionFactory().getCurrentSession().load(State.class,id);
			
			System.out.println(state.getStatename());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		 
		return state;
	}
	
	@Override
	public Industry editIndustry(int id) {
		Industry industry=null;
		try {
			industry=(Industry) getSessionFactory().getCurrentSession().load(Industry.class,id);
			System.out.println(industry.getIndustryname());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		 
		return industry;
	}

	@Override
	public boolean updateState(Statecommand statecommand) {
		boolean flag=false;
		
	      try {
			State state = (State) getSessionFactory().getCurrentSession().load(State.class,statecommand.getState_id());
			 state.setStatename(statecommand.getName());
				
			  getSessionFactory().getCurrentSession().update(state);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		   flag=true;
			return flag;
		}

	@Override
	public boolean updateIndustry(IndustryCommand industryCommand) {
		boolean flag=false;
		
	      try {
			Industry industry = (Industry) getSessionFactory().getCurrentSession().load(Industry.class,industryCommand.getId());
			industry.setIndustryname(industryCommand.getName());
				
			  getSessionFactory().getCurrentSession().update(industry);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		   flag=true;
			return flag;
		}
	@Override
	public Company editCompany(int id) {
		Company company=null;
		try {
			company=(Company) getSessionFactory().getCurrentSession().load(Company.class, id);
			 System.out.println(company.getAbout_us());
		
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return company;
	}
	@Override
	public City editCity(int id) {
		City city=null;
		try {
			city=(City) getSessionFactory().getCurrentSession().load(City.class,id);
			System.out.println(city.getCityname());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return city;
	}
	@Override
	public boolean updateCompany(CompanyCommand companyCommand) {
		boolean flag=false;
		try{
      Company company =(Company) getSessionFactory().getCurrentSession().load(Company.class,companyCommand.getCompany_id());
      
      company.setAbout_us(companyCommand.getInfo());
      company.setCompany_email(companyCommand.getMail());
      company.setCompany_name(companyCommand.getName());
      company.setCompany_number(companyCommand.getNumber());
  	   if(companyCommand.getNewpath()!=null){
  		 company.setCompany_logo(companyCommand.getNewpath());
	}
	getSessionFactory().getCurrentSession().update(company);
	flag=true;
		}
		catch(Exception exception){
			exception.printStackTrace();
		}
		return flag;
	}

	@Override
	public Location editLocation(int id) {
		Location  location=null;
		try {
			location=(Location) getSessionFactory().getCurrentSession().load(Location.class,id);
			System.out.println(location.getLocationname());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return location;
	}

	@Override
	public boolean updatecity(LocationCommand command) {
		boolean flag=false;
		
	      try {
			   City city = (City) getSessionFactory().getCurrentSession().load(City.class,command.getCity_id());
			    city.setCityid(command.getCity_id());
			    city.setCityname(command.getCityname());
			    int stateId= command.getState();
			    State state = new State();
			    state.setState_id(stateId);
			    city.setState(state);
			    
			   
			  getSessionFactory().getCurrentSession().update(city);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		   flag=true;
			return flag;
		}

	@Override
	public SubIndustry editSub(int id) {
		SubIndustry subindustry=null;
		try {
			subindustry=(SubIndustry) getSessionFactory().getCurrentSession().load(SubIndustry.class,id);
			System.out.println(subindustry.getSubindustryname());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return subindustry;
	}

	@Override
	public boolean updateSub(SubIndustryCommand subindustrycommand) {
		boolean flag=false;
		
	      try {
			   SubIndustry subindustry = (SubIndustry) getSessionFactory().getCurrentSession().load(SubIndustry.class,subindustrycommand.getId());
			    subindustry.setSubindustry_id(subindustrycommand.getId());
			    subindustry.setSubindustryname(subindustrycommand.getName());

			  getSessionFactory().getCurrentSession().update(subindustry);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		   flag=true;
			return flag;
		}

	@Override
	public boolean updatelocation(LocationCommand locationCommand) {
		boolean flag=false;
		
	      try {
			   Location location = (Location) getSessionFactory().getCurrentSession().load(Location.class,locationCommand.getLocation_id());
			    location.setLocationid(locationCommand.getLocation_id());
			    location.setLocationname(locationCommand.getLocation());
			    State state = new State();
			    state.setState_id(locationCommand.getState());
			    
			    
			   City city = new City();
			   city.setCityid(locationCommand.getCity_id());
			   city.setState(state);
			   location.setCity(city);
			   
			   
			   
			    getSessionFactory().getCurrentSession().update(location);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		   flag=true;
			return flag;
		}

	@Override
	public boolean testmonialdelete(int id) {
	boolean flag=false;
		
		
		try {
			TestiMonial testimonial=(TestiMonial) getSessionFactory().getCurrentSession().load(TestiMonial.class, id);
			
			getSessionFactory().getCurrentSession().delete(testimonial);
			flag=true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return flag;
	}

	@Override
	public int getTotalBlog() {
		int status=0;
		String sql = "select count(*) from le_blog_articals";
		SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
		List<BigInteger> list = query.list();
	    for(BigInteger o :list){
	      
	      status=o.intValue();
	    }
		return status;
	}

	@Override
	public List<BlogArticals> totalblog(int resultsFrom, int recordsPerPage) {
		 Query qry=getSessionFactory().getCurrentSession().createQuery("from BlogArticals");
		 qry.setFirstResult(resultsFrom);
		 qry.setMaxResults(recordsPerPage);
			
			List<BlogArticals> users=qry.list();
		
		   
		return users; 
			   
	}

	@Override
	public int getTotaljobs() {
		int status=0;
		String sql = "select count(*) from le_jobs_list";
		SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery(sql);
		List<BigInteger> list = query.list();
	    for(BigInteger o :list){
	      status=o.intValue();    
	    }
	return status;
	}

	
	@Override
	public List <Job_Post> totaljobs(int resultsFrom, int recordsPerPage) {
		 Query qry=getSessionFactory().getCurrentSession().createQuery("from Job_Post");
		 qry.setFirstResult(resultsFrom);
		 qry.setMaxResults(recordsPerPage);
			List<Job_Post> users=qry.list();		   
		return users; 
	}

	}


			 
	
		
		 
	



