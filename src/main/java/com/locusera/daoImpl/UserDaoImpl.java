package com.locusera.daoImpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.joda.time.LocalDateTime;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.locusera.command.EnquiryCommand;
import com.locusera.command.JobListCommand;
import com.locusera.command.LoginCommand;
import com.locusera.command.RegistrationCommand;
import com.locusera.dao.ResumeDataDao;
import com.locusera.dao.UserDao;
import com.locusera.modal.BusinessEnquiry;
import com.locusera.modal.City;
import com.locusera.modal.Company;
import com.locusera.modal.CompanyAddress;
import com.locusera.modal.Industry;
import com.locusera.modal.Job_Applications;
import com.locusera.modal.Job_Post;
import com.locusera.modal.Location;
import com.locusera.modal.SubIndustry;
import com.locusera.modal.UserRegistration;
@Transactional
public class UserDaoImpl   extends HibernateDaoSupport  implements UserDao{

	@Override
	public boolean saveUser(RegistrationCommand registrationCommand) {
	
		boolean flag=false;
		try{
		UserRegistration userReg = new UserRegistration();
		
		userReg.setLe_user_firstname(registrationCommand.getFirstname());
		userReg.setLe_user_lastname(registrationCommand.getLastname());
		userReg.setLe_username(registrationCommand.getUsername());
		userReg.setLe_user_email(registrationCommand.getEmail());
		userReg.setLe_user_pwd(registrationCommand.getPassword());
		userReg.setLe_user_phone(registrationCommand.getMobile());
		userReg.setLe_created_by(registrationCommand.getEmail());
		userReg.setLe_modified_by(registrationCommand.getEmail());
		LocalDateTime date=new LocalDateTime();
		userReg.setLe_user_created_date(date);
		userReg.setLe_user_modified_date(date);
		userReg.setIp_address(registrationCommand.getClient_ip());
		getSessionFactory().getCurrentSession().save(userReg);
		flag=true;
		
		return flag;
		}
		catch(Exception exception){
			exception.printStackTrace();
			return flag;
			
		}
		
		
	}

	@Override
	public UserRegistration checkUser(LoginCommand loginCommand) {
		UserRegistration data=null;
		try {
			String sql = "from UserRegistration where le_username=:uname and  CAST(le_user_pwd as binary)=CAST(:pass as binary)";
			Query query =getSessionFactory().getCurrentSession().createQuery(sql);
			query.setParameter("uname", loginCommand.getUsername());
			query.setParameter("pass", loginCommand.getPassword());
			
			List<UserRegistration> list=  query.list();
			if(list.size()>0){
				data = list.get(0);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return data;
	}

	@Override
	public boolean saveEnquiry(EnquiryCommand command) {
		
		boolean  flag= false;
		BusinessEnquiry enquiry=new BusinessEnquiry();
		try {
			enquiry.setEnquiry_name(command.getUsername());
			enquiry.setEnquiry_email(command.getEmail());
			enquiry.setEnquiry_number(command.getPhone());
			enquiry.setEnquiry_stateid(command.getState_id());
			enquiry.setEnquiry_cityid(command.getCity_id());
			enquiry.setEnquiry_locationid(command.getLocation_id());
			enquiry.setEnquiry_description(command.getDescription());
			
			getSessionFactory().getCurrentSession().save(enquiry);
			flag= true;
			
		} catch (HibernateException e) {
			
			e.printStackTrace();
			
		}
		
		
		return flag;
	}

	@Override
	public boolean checkUserEMail(String mail) {
		boolean flag=true;
		
	SQLQuery query=getSessionFactory().getCurrentSession().createSQLQuery("select le_user_email from le_registration where le_user_email=:mail");

	
	query.setParameter("mail", mail);
	@SuppressWarnings("unchecked")
	List<Object> list =query.list();
	if(list.size()>0){
		
		flag = false;
	}
		return flag;
	}

	@Override
	public boolean checkUserPhone(long phone) {

		  boolean flag=true;
		    
			SQLQuery query=getSessionFactory().getCurrentSession().createSQLQuery("select le_user_phone from le_registration where le_user_phone=:uphone");
			
			query.setParameter("uphone", phone);
			
			@SuppressWarnings("unchecked")
			List<Object> list=query.list();
			
			if(list.size()>0){
				
				 flag = false;
			}
			
				return flag;
			}

	@Override
	public boolean checkUserName(String username) {
    boolean flag=true;
    
	SQLQuery query=getSessionFactory().getCurrentSession().createSQLQuery("select le_username from le_registration where le_username=:uname");
	
	query.setParameter("uname", username);
	
	@SuppressWarnings("unchecked")
	List<Object> list=query.list();
	
	if(list.size()>0){
		
		 flag = false;
	}
	
		return flag;
	}

	@Override
	public List<Job_Post> getJobs() {
		Query query = getSessionFactory().getCurrentSession().createQuery("from Job_Post");
		
		@SuppressWarnings("unchecked")
		List<Job_Post> list = query.list();
		return list;
	}

	@Override
	public boolean saveJob(JobListCommand joblistCommand) {
		
		boolean  flag= false;
		try {
			Job_Applications job=new Job_Applications();
			job.setCandidate_name(joblistCommand.getCandidate_name());
			job.setCandidate_email(joblistCommand.getCandidate_email());
			job.setCandidate_mobilenumber(joblistCommand.getCandidate_mobilenumber());
			job.setCandidate_resume(joblistCommand.getResume_path());
			job.setApplied_as(joblistCommand.getApplied_as());
			LocalDateTime date=new LocalDateTime();
		    job.setApplied_date(date);
			Job_Post job_Post = (Job_Post) getSessionFactory().getCurrentSession().get(Job_Post.class, joblistCommand.getJob_id());
			job.setJob_Post(job_Post);
			getSessionFactory().getCurrentSession().save(job);
			
			flag= true;
		} catch (Exception e) {
			e.printStackTrace();
		}
       return flag;
	}

	@Override
	public UserRegistration updatepassword(int id, String pwd) {
		UserRegistration data = null;
			try{
			 Session session=getSessionFactory().getCurrentSession();
			 data=(UserRegistration)session.get(UserRegistration.class,id);
			 data.setLe_user_pwd(pwd);
			 session.update(data);
			}
			catch(Exception exception){
				exception.printStackTrace();
			}
			return data;
	}

	@Override
	public boolean updateNewPass(long mobile, String pwd) {
		SQLQuery query = getSessionFactory().getCurrentSession().createSQLQuery("update le_registration set le_user_pwd=:pass where le_user_phone=:mobile");
		query.setParameter("pass", pwd);
		query.setParameter("mobile", mobile);
		int i  = query.executeUpdate();
		System.out.println(i);
		return false;
	}

	@Override
	public boolean CheckEnMail(String mail) {
		boolean flag=true;
		SQLQuery query=getSessionFactory().getCurrentSession().createSQLQuery("select enquiry_email from le_business_enquiry where enquiry_email=:mail");
		query.setParameter("mail", mail);
		@SuppressWarnings("unchecked")
		List<Object> list =query.list();
		if(list.size()>0){
			flag = false;
		}
			return flag;
		}
	@Override
	public boolean CheckEnPhone(long phone) {
	boolean flag=true;
		SQLQuery query=getSessionFactory().getCurrentSession().createSQLQuery("select enquiry_number from le_business_enquiry where enquiry_number=:ephone");
		query.setParameter("ephone", phone);
		@SuppressWarnings("unchecked")
		List<Object> list =query.list();
		if(list.size()>0){
			flag = false;
		}
			return flag;
		}
	@Override
	public int checkUserCredentials(String username, String password) {
    SQLQuery query=getSessionFactory().getCurrentSession().createSQLQuery("select count(*) from le_registration where le_username=:username and CAST(le_user_pwd as binary )=CAST(:pass as binary)");

		query.setParameter("username", username);
		query.setParameter("pass", password);
		@SuppressWarnings("unchecked")
	
		List<Object> list =query.list();
		BigInteger count =(BigInteger) list.get(0);
		
		
		return count.intValue();
		
	}


	@Override
	public List<Job_Post> jobindustry(int id) {
		List<Job_Post> job_Postslist = new ArrayList<Job_Post>();
		String sql = "from Industry where industry_id=:id";
		Query query = getSessionFactory().getCurrentSession().createQuery(sql);
		query.setParameter("id", id);
		List<Industry> industries= query.list();
		
		for(Industry industry : industries){
			
			List<SubIndustry> subIndustry = industry.getSubindustry();
		
			
			for(SubIndustry subInd :subIndustry){
				
				List<Job_Post> job_Posts = subInd.getJobs();
				
				job_Postslist.addAll(job_Posts);
				
			}
		
		}
		
		
		
		return job_Postslist;
		
		

	}
		
		
}



	





	

