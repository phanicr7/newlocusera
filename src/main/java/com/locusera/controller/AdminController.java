package com.locusera.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.locusera.command.ArticalCommand;
import com.locusera.command.ChangePwdCommand;
import com.locusera.command.Citycommand;
import com.locusera.command.ClientLogoCommand;
import com.locusera.command.CompanyAddressCommand;
import com.locusera.command.CompanyCommand;
import com.locusera.command.EventCommand;
import com.locusera.command.IndustryCommand;
import com.locusera.command.JobCommand;
import com.locusera.command.JobListCommand;
import com.locusera.command.LocationCommand;
import com.locusera.command.NewsCommand;
import com.locusera.command.ProfileCommand;
import com.locusera.command.Statecommand;
import com.locusera.command.SubIndustryCommand;
import com.locusera.command.TestimonialCommand;
import com.locusera.dao.ResumeDataDao;
import com.locusera.modal.AdminRegistration;
import com.locusera.modal.BlogArticals;
import com.locusera.modal.City;
import com.locusera.modal.Client_Logo;
import com.locusera.modal.Company;
import com.locusera.modal.CompanyAddress;
import com.locusera.modal.Events;
import com.locusera.modal.Industry;
import com.locusera.modal.Job_Post;
import com.locusera.modal.Location;
import com.locusera.modal.News;
import com.locusera.modal.State;
import com.locusera.modal.SubIndustry;
import com.locusera.modal.TestiMonial;
import com.locusera.service.AdminLoginService;
import com.locusera.service.EmailService;
@Controller
@EnableWebMvc
public class AdminController {
	@Autowired
	private AdminLoginService loginservice;

	public AdminLoginService getLoginservice() {
		return loginservice;
	}
	public void setLoginservice(AdminLoginService loginservice) {
		this.loginservice = loginservice;
	}

	@Autowired
	private EmailService emailService;
	Random rand = new Random();

	Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name",
			"locusera", "api_key", "425233796544152", "api_secret",
			"_Y8-k2R0gmrPp-YGc9CtD0tJR-Q"));

	private String username = "satyasai";
	private String password = "9441487624";
	private String url = "http://login.bulksmsgateway.in/sendmessage.php?";

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String loginPage() {

		return "adminlogin";
	}

	@RequestMapping(value = "/adminLoging", method = RequestMethod.POST)
	public String loginChecking(@RequestParam("username") String username,
			@RequestParam("password") String password,
			HttpServletRequest request) {

		String returntype = null;
		HttpSession session = request.getSession();

		AdminRegistration logindata = loginservice.login(username, password);

		if (logindata != null) {

			session.setAttribute("user", logindata);
			returntype = "redirect:/admindashboard.do";

		} else {

			returntype = "adminlogin";
		}
		return returntype;
	}
	@RequestMapping(value = "/admindashboard", method = RequestMethod.GET)
	public String admindashboard(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
				model.addAttribute("list", loginservice.getusers().size());

				return "admindashboard";
			
		}

		return "adminlogin";

	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String users(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			model.addAttribute("list", loginservice.getusers());
			return "users";
    }

		return "adminlogin";

	}
	@RequestMapping(value = "/postjob", method = RequestMethod.GET)
	public String postjob(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("company", loginservice.getCompanys());
			model.addAttribute("industry", loginservice.getIndustrys());
			model.addAttribute("jobCommand", new JobCommand());
			return "jobpost";
	    }

		return "adminlogin";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request,
			HttpServletResponse response) {

		HttpSession session = request.getSession();
		session.removeAttribute("user");

		return "adminlogin";
	}

	@RequestMapping(value = "/listofjobs", method = RequestMethod.GET)
	public String listofjobs(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("jobs", loginservice.getjobs());
			return "jobslist";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/savejob", method = RequestMethod.POST)
	public String saveevent(HttpServletRequest request,
			@ModelAttribute("jobCommand") JobCommand command, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				Job_Post job = new Job_Post();
				job.setJob_id(job.getJob_id());
				job.setJobtitle(command.getJobtitle());
				job.setQualification_details(command.getQualification());
				job.setNo_openings(command.getOpenings());
				job.setSkills_required(command.getSkills());
				job.setExperience(command.getExperience());
				job.setJob_status(command.getJob_status());
				job.setSubindustry_id(loginservice.getsubindustry(command.getSubcatogory()));
				job.setAddress(loginservice.getCompanyAddress(command.getLocationname()));
				int i = loginservice.savejob(job);
				model.addAttribute("company", loginservice.getCompanys());
				model.addAttribute("industry", loginservice.getIndustrys());
				model.addAttribute("jobCommand", new JobCommand());
				attributes.addFlashAttribute("add", "add");
			} catch (Exception ex) {
				System.out.println(ex);

			}

			return "redirect:listofjobs.do";
		}
		return "adminlogin";

	}

	@RequestMapping(value = "/editjob", method = RequestMethod.GET)
	public String jobedit(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
			     	JobCommand jobCommand = new JobCommand();
					int id= Integer.parseInt(request.getParameter("jobId"));
					Job_Post job = loginservice.editJob(id);
					jobCommand.setExperience(job.getExperience());
					jobCommand.setJobid(job.getJob_id());
					jobCommand.setJobtitle(job.getJobtitle());
					jobCommand.setOpenings(job.getNo_openings());
					jobCommand.setQualification(job.getQualification_details());
					jobCommand.setSkills(job.getSkills_required());
					SubIndustry subIndustry = job.getSubindustry_id();
					jobCommand.setSubcatogory(subIndustry.getSubindustry_id());					
					Industry industry= subIndustry.getIndustry();
					jobCommand.setCatogory(industry.getIndustry_id());	
					CompanyAddress address = job.getAddress();	
					Company company = address.getCompany();
					jobCommand.setCompanyname(company.getCompany_id());		
					Location location =  address.getLocation();		  
				    jobCommand.setLocationname(location.getLocationid());
				    jobCommand.setJob_status(job.getJob_status());
				    model.addAttribute("company", loginservice.getCompanys());
					model.addAttribute("industry", loginservice.getIndustrys());
					model.addAttribute("locations",loginservice.getcompanylocations(company.getCompany_id()));
					model.addAttribute("jobtype",loginservice.getjobs(industry.getIndustry_id()));
					model.addAttribute("jobCommand", jobCommand);
					}
			    catch (NumberFormatException e) {
				e.printStackTrace();
			}
			return "editjob";
     }

		return "adminlogin";
	}
	@RequestMapping(value = "/updateJob", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String updateJob(JobCommand jobCommand,HttpServletRequest request, Model model,RedirectAttributes attributes) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			boolean flag = loginservice.updateJob(jobCommand);
			attributes.addFlashAttribute("updated", "updated");

			return "redirect:listofjobs.do";
		}
		
		return "adminlogin";
	}
	@RequestMapping(value = "/jobdelete")
	public String jobdelete(HttpServletRequest request,
			RedirectAttributes attributes) {

		int id = Integer.parseInt(request.getParameter("delete_job_id"));

		boolean flag = loginservice.deleteJob(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}

	@RequestMapping(value = "/enquiryforms", method = RequestMethod.GET)
	public String enquiryforms(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("enquiry", loginservice.getEnquirydetails());
			return "enquiryforms";
		}

		return "adminlogin";
	}

	@RequestMapping(value = "/enquirydelete")
	public String enquirydelete(HttpServletRequest request,
			RedirectAttributes attributes) {

		int id = Integer.parseInt(request.getParameter("delete_enquiry_id"));

		boolean flag = loginservice.deletEnquiry(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}

	@RequestMapping(value = "/addartical", method = RequestMethod.GET)
	public String addartical(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("blogCommand", new ArticalCommand());
			return "addartical";
		}
		return "adminlogin";
	}

	@RequestMapping(value = "/articallist", method = RequestMethod.GET)
	public String articallist(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		try {
			if (session.getAttribute("user") != null) {
				
					List<BlogArticals> blog = loginservice.getArticals();
					List<BlogArticals> articals = new ArrayList<BlogArticals>();
					for (BlogArticals artical : blog) {
						artical.setArtical_logo_path(cloudinary
								.url()
								.secure(true)
								.transformation(
										new Transformation().width(60).height(60))
								.generate((artical.getArtical_logo_path())));
						articals.add(artical);

					}

					model.addAttribute("blog", articals);
					return "articallist";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "adminlogin";

	}
	
	@RequestMapping(value = "/saveblog", method = RequestMethod.POST)
	public String saveartical(HttpServletRequest request,
			@ModelAttribute("blogCommand") ArticalCommand command, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				int imagerandom = rand.nextInt(9000000) + 1000000;
				byte[] l = null;

				l = command.getLogo().getBytes();

				Map params = ObjectUtils.asMap("public_id","locusera/articals/"	+ command.getLogo().getOriginalFilename().split("\\.[^\\.]*$")[0] + "-locusera-"+ imagerandom);
				Map uploadResult = null;
				uploadResult = cloudinary.uploader().upload(l, params);
				BlogArticals artical = new BlogArticals();
				artical.setArtical_content(command.getArt_content());
				artical.setArtical_heading(command.getHeading());
				artical.setArtical_logo_path((String) uploadResult.get("public_id")+ "."+ (String) uploadResult.get("format"));
				artical.setArtical_title(command.getTitle());
				artical.setWriter_name(command.getWritername());
				artical.setArticalwriter_info(command.getInfo());

				artical.setPosteddate(new java.sql.Timestamp(DateUtils.truncate(new java.util.Date(), Calendar.MILLISECOND).getTime()));
				int i = loginservice.saveartical(artical);
				attributes.addFlashAttribute("add", "add");
				// model.addAttribute("blogCommand", new ArticalCommand());
				/*
				 * String urle = cloudinary .url().secure(true) .transformation(
				 * new Transformation().width(600).height(
				 * 500)).generate((pid));
				 */

			} catch (Exception ex) {
				System.out.println(ex);

			}

			return "redirect:articallist.do";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/editartical", method = RequestMethod.GET)
	public String editartical(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			try {
				int id = Integer.parseInt(request.getParameter("blogId"));

				BlogArticals blog = loginservice.editArticle(id);
				ArticalCommand articalCommand = new ArticalCommand();
				articalCommand.setBlogid(blog.getBlog_id());
				articalCommand.setArt_content(blog.getArtical_content());
				articalCommand.setHeading(blog.getArtical_heading());
				articalCommand.setInfo(blog.getArticalwriter_info());
				articalCommand.setTitle(blog.getArtical_title());
				articalCommand.setWritername(blog.getWriter_name());
				articalCommand.setOldpath(cloudinary.url().secure(true)
						.transformation(new Transformation().width(45).height(45))
						.generate((blog.getArtical_logo_path())));

				model.addAttribute("articalCommand", articalCommand);

				return "editartical";
			} catch (NumberFormatException e) {
				
				e.printStackTrace();
			}

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/updateArtical", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String updateArtical(ArticalCommand articalCommand,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			try {

				int imagerandom = rand.nextInt(9000000) + 1000000;
				byte[] l = null;

				l = articalCommand.getLogo().getBytes();
				if (l != null) {
					if (l.length > 0) {

						Map params = ObjectUtils.asMap("public_id",
								"locusera/articals/"
										+ articalCommand.getLogo()
												.getOriginalFilename()
												.split("\\.[^\\.]*$")[0]
										+ "-locusera-" + imagerandom);
						Map uploadResult = null;

						uploadResult = cloudinary.uploader().upload(l, params);
						articalCommand.setNewpath((String) uploadResult
								.get("public_id")
								+ "."
								+ (String) uploadResult.get("format"));

					}
				}

			} catch (Exception exception) {
				exception.printStackTrace();

			}

			boolean flag = loginservice.updateArticle(articalCommand);

			return "redirect:articallist.do";

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/blogdelete")
	public String blogdelete(HttpServletRequest request,
			RedirectAttributes attributes) {

		int id = Integer.parseInt(request.getParameter("delete_blog_id"));

		boolean flag = loginservice.deleteBlog(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");
	}

	@RequestMapping(value = "/addnews", method = RequestMethod.GET)
	public String addnews(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("newsCommand", new NewsCommand());
			return "addnews";

		}
		return "adminlogin";
	}
	@RequestMapping(value = "/savenews", method = RequestMethod.POST)
	public String savenews(HttpServletRequest request,
			@ModelAttribute("newsCommand") NewsCommand command, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				int imagerandom = rand.nextInt(9000000) + 1000000;
				byte[] l = null;
				l = command.getLogo().getBytes();
				Map params = ObjectUtils.asMap("public_id",
						"locusera/news/"
								+ command.getLogo().getOriginalFilename()
										.split("\\.[^\\.]*$")[0] + "-locusera-"
								+ imagerandom);
				Map uploadResult = null;

				uploadResult = cloudinary.uploader().upload(l, params);

				News news = new News();
				news.setNews_title(command.getTitle());
				
				news.setNews_content(command.getNews_content());
				news.setNews_heading(command.getHeading());
				news.setNews_logo((String) uploadResult.get("public_id") + "."
						+ (String) uploadResult.get("format"));
				attributes.addFlashAttribute("add", "add");

				int i = loginservice.savenews(news);

				/*
				 * String urle = cloudinary .url().secure(true) .transformation(
				 * new Transformation().width(600).height(
				 * 500)).generate((pid));
				 */
				// model.addAttribute("newsCommand", new NewsCommand());

			} catch (Exception ex) {
				System.out.println(ex);

			}

			return "redirect:newslist.do";

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/newslist", method = RequestMethod.GET)
	public String newslist(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			try {
				List<News> news = loginservice.getNewsList();
				List<News> articals = new ArrayList<News>();
				System.out.println(news.size());
				for (News artical : news) {

					artical.setNews_logo(cloudinary
							.url()
							.secure(true)
							.transformation(
									new Transformation().width(45).height(45))
							.generate((artical.getNews_logo())));
					articals.add(artical);

				}
				model.addAttribute("news", articals);

				return "newslist";
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/editnews", method = RequestMethod.GET)
	public String editnews(HttpServletRequest request,Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null){
       try {
				NewsCommand newsCommand = new NewsCommand();
				int id = Integer.parseInt(request.getParameter("NewsId"));
				News news = loginservice.editNews(id);
				newsCommand.setNewsid(news.getNews_id());
				newsCommand.setTitle(news.getNews_title());
				newsCommand.setHeading(news.getNews_heading());
				newsCommand.setNews_content(news.getNews_content());
				newsCommand.setOldpath(cloudinary.url().secure(true).transformation(new Transformation().width(45).height(45)).generate((news.getNews_logo())));
				model.addAttribute("newsCommand", newsCommand);
		} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			return "editnews";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/updateNews", method = { RequestMethod.GET,RequestMethod.POST })
	public String updateNews(NewsCommand newsCommand,HttpServletRequest request, Model model,RedirectAttributes attributes) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
				try {
					int imagerandom = rand.nextInt(9000000) + 1000000;
					byte[] l = null;

					l = newsCommand.getLogo().getBytes();
					if (l != null) {
						if (l.length > 0) {

							Map params = ObjectUtils.asMap(
									"public_id",
									"locusera/news/"
											+ newsCommand.getLogo()
													.getOriginalFilename()
													.split("\\.[^\\.]*$")[0]
											+ "-locusera-" + imagerandom);
							Map uploadResult = null;

							uploadResult = cloudinary.uploader().upload(l, params);
							newsCommand.setNewpath((String) uploadResult
									.get("public_id")
									+ "."
									+ (String) uploadResult.get("format"));

						}
					}
					boolean flag = loginservice.updateNews(newsCommand);
					attributes.addFlashAttribute("updated", "updated");


				} catch (Exception exception) {
					exception.printStackTrace();

				}
				
		return "redirect:newslist.do";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/newsdelete")
	public String newsdelete(HttpServletRequest request,RedirectAttributes attributes) {
		int id = Integer.parseInt(request.getParameter("delete_news_id"));

		boolean flag = loginservice.deleteNews(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}

	
	@RequestMapping(value = "/viewState", method = RequestMethod.GET)
	public String viewState(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("stateList", loginservice.getStates());
			return "stateList";
		}
		return "adminlogin";
	}

	@RequestMapping(value = "/addstate", method = RequestMethod.GET)
	public String addstate(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("stateCommand", new Statecommand());
			return "addstate";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/editstate", method = RequestMethod.GET)
	public String editstate(HttpServletRequest request, Model model,Statecommand stateCommand) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
 try {
									int id= Integer.parseInt(request.getParameter("stateId"));
									State state = loginservice.editState(id);
									stateCommand.setState_id(state.getState_id());
									stateCommand.setName(state.getStatename());
								} catch (NumberFormatException e) {
									e.printStackTrace();
								}
			return "editstate";
		}

		return "adminlogin";

	}
	@RequestMapping(value = "/updatestate", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String updatestate(Statecommand statecommand,HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			boolean flag = loginservice.updateState(statecommand);
			attributes.addFlashAttribute("updated", "updated");
			return "redirect:viewState.do";
		}
		
		return "adminlogin";
	}
	@RequestMapping(value = "/deletestate")
	public String deleteState(HttpServletRequest request,
			RedirectAttributes attributes) {
		int id = Integer.parseInt(request.getParameter("delete_state_id"));
		boolean flag = loginservice.deleteState(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}

	@RequestMapping(value = "/addevents", method = RequestMethod.GET)
	public String addevents(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("eventCommand", new EventCommand());
			return "addevents";
		}
		return "adminlogin";
	}

	@RequestMapping(value = "/eventslist", method = RequestMethod.GET)
	public String eventlist(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			try {
				List<Events> events = loginservice.getEventList();
				List<Events> articals = new ArrayList<Events>();
				for (Events artical : events) {

					artical.setEvent_logo(cloudinary
							.url()
							.secure(true)
							.transformation(
									new Transformation().width(45).height(45))
							.generate((artical.getEvent_logo())));
					articals.add(artical);

				}

				model.addAttribute("events", articals);

				return "eventlist";
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/saveevent", method = RequestMethod.POST)
	public String saveevent(HttpServletRequest request,
			@ModelAttribute("eventCommand") EventCommand command, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				int imagerandom = rand.nextInt(9000000) + 1000000;
				byte[] l = null;

				l = command.getLogo().getBytes();

				Map params = ObjectUtils.asMap("public_id",
						"locusera/events/"
								+ command.getLogo().getOriginalFilename()
										.split("\\.[^\\.]*$")[0] + "-locusera-"
								+ imagerandom);
				Map uploadResult = null;
				uploadResult = cloudinary.uploader().upload(l, params);
				Events artical = new Events();
				artical.setEvent_content(command.getEventcontent());
				artical.setEvent_heading(command.getHeading());
				artical.setEvent_logo((String) uploadResult.get("public_id")
						+ "." + (String) uploadResult.get("format"));
				artical.setEvent_title(command.getTitle());
				int i = loginservice.saveevent(artical);
				attributes.addFlashAttribute("add", "add");
				// model.addAttribute("eventCommand", new EventCommand());

				/*
				 * String urle = cloudinary .url().secure(true) .transformation(
				 * new Transformation().width(600).height(
				 * 500)).generate((pid));
				 */

			} catch (Exception ex) {
				System.out.println(ex);

			}

			return "redirect:eventslist.do";

		}

		return "adminlogin";
	}

	@RequestMapping(value = "/editevent", method = RequestMethod.GET)
	public String editevent(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null)

		{
              try {
							EventCommand eventCommand = new EventCommand();
							int id= Integer.parseInt(request.getParameter("eventId"));
   							Events events = loginservice.editEvents(id);
							eventCommand.setEventid(events.getEvent_id());	
							eventCommand.setTitle(events.getEvent_title());
							eventCommand.setEventcontent(events.getEvent_content());
							eventCommand.setHeading(events.getEvent_heading());
							eventCommand.setOldpath(cloudinary
									.url()
									.secure(true)
									.transformation(
											new Transformation().width(45).height(45))
									.generate((events.getEvent_logo())));
							model.addAttribute("eventCommand", eventCommand);

						} catch (NumberFormatException e) {
							e.printStackTrace();
						}

			return "editevent";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/updateEvent", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String updateEvent(EventCommand eventCommand,HttpServletRequest request, Model model,RedirectAttributes attributes ) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			try {
					int imagerandom = rand.nextInt(9000000) + 1000000;
					byte[] l = null;

					l = eventCommand.getLogo().getBytes();
					if (l != null) {
						if (l.length > 0) {

							Map params = ObjectUtils.asMap(
									"public_id",
									"locusera/events/"
											+ eventCommand.getLogo()
													.getOriginalFilename()
													.split("\\.[^\\.]*$")[0]
											+ "-locusera-" + imagerandom);
							Map uploadResult = null;

							uploadResult = cloudinary.uploader().upload(l, params);
							eventCommand.setNewpath((String) uploadResult
									.get("public_id")
									+ "."
									+ (String) uploadResult.get("format"));

						}
					}
					boolean flag = loginservice.updateEvent(eventCommand);
					attributes.addFlashAttribute("updated", "updated");


				} catch (Exception exception) {
					exception.printStackTrace();

				}

			return "redirect:eventslist.do";

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/eventsdelete")
	public String eventsdelete(HttpServletRequest request,
			RedirectAttributes attributes) {

		int id = Integer.parseInt(request.getParameter("delete_event_id"));
		boolean flag = loginservice.deleteEvent(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}

	@RequestMapping(value = "/savestate", method = RequestMethod.POST)
	public String savestate(
			@ModelAttribute("stateCommand") Statecommand command,
			HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			State state = new State();
			state.setStatename(command.getName());
			loginservice.savesate(state);
			attributes.addFlashAttribute("add", "add");

			return "redirect:viewState.do";
         
		}

		return "adminlogin";

	}

	@RequestMapping(value = "/addcity", method = RequestMethod.GET)
	public String addcity(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			model.addAttribute("cityCommand", new Citycommand());
			model.addAttribute("states", loginservice.getStates());
			return "addcity";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/editcity", method = RequestMethod.GET)
	public String editcity(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			            int id= Integer.parseInt(request.getParameter("cityId"));
                        City city=loginservice.editCity(id);
                        LocationCommand command = new LocationCommand();
                        State state = city.getState();
                        command.setState(state.getState_id());
                        command.setCityname(city.getCityname());
                        command.setCity_id(city.getCityid());
                        model.addAttribute("command",command);
						model.addAttribute("states", loginservice.getStates());	
			return "editcity";
		}

		return "adminlogin";

	}
	@RequestMapping(value = "/updateCity", method = { RequestMethod.GET,RequestMethod.POST })
	public String updateCity(LocationCommand command,HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			boolean flag = loginservice.updatecity(command);
			attributes.addFlashAttribute("updated", "updated");
			return "redirect:viewCity.do";
		}
		
		return "adminlogin";
	}
	@RequestMapping(value = "/deletecity")
	public String deleteCity(HttpServletRequest request,
			RedirectAttributes attributes) {
		try {
			int id = Integer.parseInt(request.getParameter("delete_city_id"));

			boolean flag = loginservice.deleteCity(id);

			if (flag) {

				attributes.addFlashAttribute("deleted", "deleted");

			} else {
				attributes.addFlashAttribute("error", "error");

			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return "redirect:" + request.getHeader("referer");

	}

	@RequestMapping(value = "/addindustry", method = RequestMethod.GET)
	public String addindustry(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("industryCommand", new IndustryCommand());
			return "addindustry";
		}
		return "adminlogin";
	}
	@RequestMapping(value = "/editindustry", method = RequestMethod.GET)
	public String editindustry(HttpServletRequest request, Model model,IndustryCommand industryCommand) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
	            	            try {
									int id= Integer.parseInt(request.getParameter("industryId"));
									Industry industry = loginservice.editIndustry(id);
									industryCommand.setId(industry.getIndustry_id());
									industryCommand.setName(industry.getIndustryname());

								} catch (NumberFormatException e) {
									e.printStackTrace();
								}
			return "editindustry";
		}

		return "adminlogin";

	}
	@RequestMapping(value = "/updateindustry", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String updateindustry(HttpServletRequest request, Model model,RedirectAttributes attributes,IndustryCommand industryCommand) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			boolean flag = loginservice.updateIndustry(industryCommand);
			attributes.addFlashAttribute("updated", "updated");

			return "redirect:viewIndustry.do";
		}
		
		return "adminlogin";
	}
	@RequestMapping(value = "/viewIndustry", method = RequestMethod.GET)
	public String viewIndustry(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			model.addAttribute("industryList", loginservice.getIndustryList());

			return "industryList";
		}

		return "adminlogin";
	}

	@RequestMapping(value = "/saveindustry", method = RequestMethod.POST)
	public String saveindustry(
			@ModelAttribute("industryCommand") IndustryCommand command,
			HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			Industry industry = new Industry();
			industry.setIndustryname(command.getName());
			loginservice.saveindustry(industry);

			attributes.addFlashAttribute("save", "save");

			return "redirect:viewIndustry.do";

		}

		return "adminlogin";
	}
	@RequestMapping(value = "/deleteindustry")
	public String deleteIndustry(HttpServletRequest request,
			RedirectAttributes attributes) {
		int id = Integer.parseInt(request.getParameter("delete_industry_id"));
		boolean flag = loginservice.deleteIndustry(id);
		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}
	@RequestMapping(value = "/addsubindustry", method = RequestMethod.GET)
	public String addsubindustry(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			model.addAttribute("industry", loginservice.getIndustrys());
			model.addAttribute("subindustryCommand", new SubIndustryCommand());

			return "addsubindustry";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/editsubindustry", method = RequestMethod.GET)
	public String editsubindustry(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			 try {
				int id= Integer.parseInt(request.getParameter("subId"));
				 SubIndustry subindustry=loginservice.editSub(id);
				 SubIndustryCommand subcommand = new SubIndustryCommand();
				 Industry  industry = subindustry.getIndustry();
				 subcommand.setId(subindustry.getSubindustry_id());
				 subcommand.setName(subindustry.getSubindustryname());
                   model.addAttribute("subcommand",subcommand);
				model.addAttribute("industry", loginservice.getIndustrys());
				model.addAttribute("subindustryCommand", new SubIndustryCommand());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			return "editsubindustry";
		}
		return "adminlogin";
	}
	@RequestMapping(value = "/updateSubindustry", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String updateSubindustry(SubIndustryCommand subindustrycommand,HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			boolean flag = loginservice.updateSub(subindustrycommand);
			attributes.addFlashAttribute("updated", "updated");
			return "redirect:viewSubIndustry.do";
		}
		return "adminlogin";
	}
	
	@RequestMapping(value = "/deletesubindustry")
	public String deletesubindustry(HttpServletRequest request,
			RedirectAttributes attributes) {
		int id = Integer.parseInt(request.getParameter("delete_subindustry_id"));

		boolean flag = loginservice.deleteSubIndustry(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}
	@RequestMapping(value = "/viewSubIndustry", method = RequestMethod.GET)
	public String viewSubIndustry(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			model.addAttribute("subIndustryList", loginservice.getSubIndustryList());
			return "subIndustryList";
		}
		return "adminlogin";

	}
	@RequestMapping(value = "/savesubindustry", method = RequestMethod.POST)
	public String savesubindustry(
			@ModelAttribute("subindustryCommand") SubIndustryCommand command,
			HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			loginservice.savesubindustry(command);
			model.addAttribute("industry", loginservice.getIndustrys());
			attributes.addFlashAttribute("save", "save");

			return "redirect:viewSubIndustry.do";

		}

		return "adminlogin";

	}
	
	@RequestMapping(value = "/viewCity", method = RequestMethod.GET)
	public String viewCity(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("cityList", loginservice.getCity());
			return "cityList";

		}

		return "adminlogin";

	}
	

	@RequestMapping(value = "/savecity", method = RequestMethod.POST)
	public String savecity(@ModelAttribute("cityCommand") Citycommand command,
			HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			loginservice.savecity(command);
			model.addAttribute("cityCommand", new Citycommand());
			model.addAttribute("states", loginservice.getStates());
			attributes.addFlashAttribute("add", "add");

			return "redirect:viewCity.do";

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/savelocation", method = RequestMethod.POST)
	public String savelocation(
			@ModelAttribute("locationCommand") LocationCommand command,
			HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			loginservice.savelocation(command);
			model.addAttribute("locationCommand", new LocationCommand());
			model.addAttribute("states", loginservice.getStates());
			attributes.addFlashAttribute("add", "add");
			return "redirect:viewLocation.do";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/editlocation", method = RequestMethod.GET)
	public String editlocation(HttpServletRequest request, Model model,LocationCommand locationCommand) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			int id = Integer.parseInt(request.getParameter("locationId"));
			Location location = loginservice.editLocation(id);
			locationCommand.setLocation_id(location.getLocationid());
			locationCommand.setLocation(location.getLocationname());
			City city=location.getCity();
			locationCommand.setCity_id(city.getCityid());
			State state = city.getState();
			locationCommand.setState(state.getState_id());
		
            model.addAttribute("locationCommand",locationCommand);			
			model.addAttribute("states", loginservice.getStates()); 	
			model.addAttribute("cities",loginservice.getcitys(state.getState_id()));
			return "editlocation";
		}
		return "adminlogin";

	}
	@RequestMapping(value = "/updateLocation", method = { RequestMethod.GET,RequestMethod.POST })
	public String updateLocation(LocationCommand locationCommand,HttpServletRequest request, Model model,RedirectAttributes attributes) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			boolean flag = loginservice.updatelocation(locationCommand);
			attributes.addFlashAttribute("updated", "updated");

			return "redirect:viewLocation.do";
		}
		return "adminlogin";
	}
	@RequestMapping(value = "/viewLocation", method = RequestMethod.GET)
	public String viewLocation(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("locationList", loginservice.getLocations());
			return "locationList";
		}
		return "adminlogin";
	}

	@RequestMapping(value = "/addlocation", method = RequestMethod.GET)
	public String addlocation(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("locationCommand", new LocationCommand());
			model.addAttribute("states", loginservice.getStates());
			return "addlocation";
		}

		return "adminlogin";

	}
	@RequestMapping(value = "/deletelocation")
	public String deleteLocation(HttpServletRequest request,
			RedirectAttributes attributes) {
		int id = Integer.parseInt(request.getParameter("delete_location_id"));
		boolean flag = loginservice.deleteLocation(id);
		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}
	@RequestMapping(value = "/getcity", method = RequestMethod.GET)
	public @ResponseBody Map<Integer, String> getcitys(
			@RequestParam("id") int id) {

		Map<Integer, String> city = loginservice.getcitys(id);

		return city;

	}

	@RequestMapping(value = "/getlocation", method = RequestMethod.GET)
	public @ResponseBody Map<Integer, String> getlocation(
			@RequestParam("id") int id) {

		Map<Integer, String> locations = loginservice.getlocations(id);

		return locations;

	}

	@RequestMapping(value = "/getjobtype", method = RequestMethod.GET)
	public @ResponseBody Map<Integer, String> getjobtype(
			@RequestParam("id") int id) {

		Map<Integer, String> jobs = loginservice.getjobs(id);

		return jobs;

	}

	@RequestMapping(value = "/change", method = RequestMethod.GET)
	public String changepwd(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("changeCommand", new ChangePwdCommand());
			return "changepwd";

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profile(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("profileCommand", new ProfileCommand());
			return "profile";

		}

		return "adminlogin";

	}
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String profile(HttpServletRequest request, Model model,
			@ModelAttribute("profileCommand") ProfileCommand command) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			try {
				AdminRegistration admindata = (AdminRegistration) session.getAttribute("user");
				AdminRegistration logindata = loginservice.updateprofileinfo(admindata.getLe_id(), command);
				session.setAttribute("user", logindata);
				model.addAttribute("profileCommand", new ProfileCommand());
				model.addAttribute("alert", "update  profile Successfully");
				return "profile";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "adminlogin";
	}

	@RequestMapping(value = "/savepwd", method = RequestMethod.POST)
	public String changePassword(HttpServletRequest request, Model model,
			@ModelAttribute("changeCommand") ChangePwdCommand command) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				AdminRegistration admindata = (AdminRegistration) session
						.getAttribute("user");
				AdminRegistration logindata = loginservice.updatepassword(
						admindata.getLe_id(), command.getPwd());

				session.setAttribute("user", logindata);

				model.addAttribute("profileCommand", new ProfileCommand());
				model.addAttribute("alert", "update  password  Successfully");
				return "changepwd";
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}

		return "adminlogin";

	}

	@RequestMapping(value = "/resume", method = RequestMethod.GET)
	public String resume(HttpServletRequest request, Model model,
			@ModelAttribute("changeCommand") ChangePwdCommand command) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			List<ResumeDataDao> userdao = loginservice.getResumes();

			model.addAttribute("resume", userdao);

			return "resume";
		}

		return "adminlogin";

	}

	@RequestMapping(value = "/searchresume", method = RequestMethod.GET)
	public String serchresume(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("industry", loginservice.getIndustrys());
			model.addAttribute("dateCommand",new JobListCommand());
			return "searching";
		}

		return "adminlogin";

	}

	@RequestMapping(value = "/searchinglist", method = {RequestMethod.GET,RequestMethod.POST})
	public String searchingresume(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
			List<ResumeDataDao> user = null;
				try {
					if (session.getAttribute("user") != null ) {
						
						int id= Integer.parseInt(request.getParameter("industryid"));
						String[] subInd_ids = request.getParameterValues("subcatogory");
						if(id!=0 && subInd_ids==null){
						user= loginservice.getResumesByIndustry(id);
						
						}
						if(id!=0 && subInd_ids!=null && subInd_ids.length>0){
						List<Integer> allIds = new ArrayList<Integer>(); 
							for(int i=0;subInd_ids.length>i;i++){
								int subIndId = Integer.parseInt(subInd_ids[i]);
								allIds.add(subIndId);
							}
							user = loginservice.getResumesBySubInd(allIds);
						}
						model.addAttribute("resume", user);
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			
			 return "searchinglist";
     }
	
	@RequestMapping(value="/dateSearch", method = {RequestMethod.GET,RequestMethod.POST})
	public String dateSearch(JobListCommand dateCommand,HttpServletRequest request,Model model ){
		List<ResumeDataDao> user = null;
		HttpSession session = request.getSession();
		try {
			if (session.getAttribute("user") != null ) {
				Date fromDate= dateCommand.getFrom_date();
				Date toDate= dateCommand.getTo_date();
				user =loginservice.getResumesByDate(fromDate,toDate);
				model.addAttribute("resume", user);
			}
			}
			catch(Exception exception){
			exception.printStackTrace();
			}
		
		return "searchinglist";
	} 
	@RequestMapping(value = "/company", method = RequestMethod.GET)
	public String company(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			model.addAttribute("companyCommand", new CompanyCommand());

			return "company";
		}

		return "adminlogin";

	}
	@RequestMapping(value = "/editcompany", method = RequestMethod.GET)
	public String editcompany(HttpServletRequest request, Model model,CompanyCommand companyCommand) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				int id = Integer.parseInt(request.getParameter("companyId"));
				Company company = loginservice.editCompany(id);
				companyCommand.setCompany_id(company.getCompany_id());
				companyCommand.setInfo(company.getAbout_us());
				companyCommand.setMail(company.getCompany_email());
				companyCommand.setName(company.getCompany_name());
				companyCommand.setNumber(company.getCompany_number());
				companyCommand.setOldpath(cloudinary
						.url()
						.secure(true)
						.transformation(
								new Transformation().width(45).height(45))
						.generate((company.getCompany_logo())));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			return "editcompany";
		}
		return "adminlogin";
	}
	@RequestMapping(value = "/updateCompany", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String updateCompany(CompanyCommand companyCommand,HttpServletRequest request, Model model,RedirectAttributes attributes ) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
					int imagerandom = rand.nextInt(9000000) + 1000000;
					byte[] l = null;
					l = companyCommand.getLogo().getBytes();
					if (l != null) {
						if (l.length > 0){
							Map params = ObjectUtils.asMap(
		                           	"public_id",
									"locusera/company/"
											+ companyCommand.getLogo()
													.getOriginalFilename()
													.split("\\.[^\\.]*$")[0]
											+ "-locusera-" + imagerandom);
							Map uploadResult = null;
							uploadResult = cloudinary.uploader().upload(l, params);
							companyCommand.setNewpath((String) uploadResult
									.get("public_id")
									+ "."
									+ (String) uploadResult.get("format"));
						}
					}
					boolean flag = loginservice.updateCompany(companyCommand);
					attributes.addFlashAttribute("updated", "updated");
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			return "redirect:viewCompany.do.do";
		}
		return "adminlogin";

	}

	@RequestMapping(value = "/savecompany", method = RequestMethod.POST)
	public String saveevent(HttpServletRequest request,
			@ModelAttribute("companyCommand") CompanyCommand command,
			Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				int imagerandom = rand.nextInt(9000000) + 1000000;
				byte[] l = null;

				l = command.getLogo().getBytes();

				Map params = ObjectUtils.asMap("public_id",
						"locusera/companylogos/"
								+ command.getLogo().getOriginalFilename()
										.split("\\.[^\\.]*$")[0] + "-locusera-"
								+ imagerandom);
				Map uploadResult = null;

				uploadResult = cloudinary.uploader().upload(l, params);

				Company company = new Company();
				company.setAbout_us(command.getInfo());
				company.setCompany_email(command.getMail());
				company.setCompany_logo((String) uploadResult.get("public_id")
						+ "." + (String) uploadResult.get("format"));
				company.setCompany_number(command.getNumber());
				company.setCompany_name(command.getName());

				int i = loginservice.savecompany(company);
				attributes.addFlashAttribute("save", "save");
				model.addAttribute("companyCommand", new CompanyCommand());

			} catch (Exception ex) {
				System.out.println(ex);

			}

			return "redirect:viewCompany.do";

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/viewCompany", method = RequestMethod.GET)
	public String viewCompany(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("companyList", loginservice.getCompanys());
			return "companyList";
		}

		return "adminlogin";
	}
	
	@RequestMapping(value = "/companylocation", method = RequestMethod.GET)
	public String companylocation(HttpServletRequest request, Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			model.addAttribute("company", loginservice.getCompanys());
			model.addAttribute("states", loginservice.getStates());
			model.addAttribute("companylocationCommand",new CompanyAddressCommand());
			attributes.addFlashAttribute("save", "save");

			return "companylocation";
		}
	
		return "adminlogin";
	}
	@RequestMapping(value = "/deleteCompany")
	public String deleteCompany(HttpServletRequest request,
			RedirectAttributes attributes) {

		int id = Integer.parseInt(request.getParameter("delete_company_id"));

		boolean flag = loginservice.deleteCompany(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");
	}
	    @RequestMapping(value = "/address", method = RequestMethod.POST)
	public String address(HttpServletRequest request,@ModelAttribute("companylocationCommand") CompanyAddressCommand command,Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {

			loginservice.address(command);
			model.addAttribute("company", loginservice.getCompanys());
			model.addAttribute("states", loginservice.getStates());
			model.addAttribute("companylocationCommand",new CompanyAddressCommand());
			attributes.addFlashAttribute("save", "save");

			return "companylocation";
		}
		return "adminlogin";

	}

	@RequestMapping(value = "/getcompanylocations", method = RequestMethod.GET)
	public @ResponseBody Map<Integer, String> getcompanylocations(
			HttpServletRequest request, @RequestParam("id") int id) {
		HttpSession session = request.getSession();
		Map<Integer, String> locations = null;
		if (session.getAttribute("user") != null) {
			locations = loginservice.getcompanylocations(id);
		}
		return locations;
	}
	@RequestMapping(value = "/sms", method = RequestMethod.GET)
	public String sms(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {

			model.addAttribute("industry", loginservice.getIndustrys());
			return "sms";
		}
		return "adminlogin";
	}

	@RequestMapping(value = "/smssending", method = RequestMethod.POST)
	public String smssending(HttpServletRequest request, Model model,
			@RequestParam("subcatogory") int subcatogory,
			@RequestParam("data") String data,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			List<Object[]> sms = loginservice.getdetails(subcatogory);

			if (sms.size() > 0) {

				for (Object[] obj : sms) {
					System.out.println((String) obj[1].toString());
					String sResult1 = sendsms(username, password, url,
							(String) obj[1].toString(), data);
					System.out.println(sResult1);

				}

				model.addAttribute("industry", loginservice.getIndustrys());
				attributes.addFlashAttribute("sent", "sent");

				return "redirect:resume.do";

			} else {
				model.addAttribute("industry", loginservice.getIndustrys());
				attributes.addFlashAttribute("notsent", "notsent");

				return "redirect:resume.do";

			}

		}

		return "adminlogin";

	}
	
	@RequestMapping(value = "/emailsending", method = RequestMethod.POST)
	public String emailsending(HttpServletRequest request, Model model,@RequestParam("subcatogory") int subcatogory,@RequestParam("data") String data,RedirectAttributes attributes) {
		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {

			List<Object[]> sms = loginservice.getdetails(subcatogory);

			if (sms.size() > 0) {

				for (Object[] obj : sms) {
					System.out.println((String) obj[0].toString());
					emailService.sendEmail((String) obj[0].toString(), data);

				}

				model.addAttribute("industry", loginservice.getIndustrys());
				attributes.addFlashAttribute("sent", "sent");
				return "redirect:resume.do";

			} else {
				model.addAttribute("industry", loginservice.getIndustrys());
				attributes.addFlashAttribute("notsent", "notsent");

				return "redirect:resume.do";

			}

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/email", method = RequestMethod.GET)
	public String email(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {

			model.addAttribute("industry", loginservice.getIndustrys());

			return "email";
		}

		return "adminlogin";

	}

	public static String sendsms(String uasername, String smspassword,
			String urls, String contact, String datamsg) {
		try {
			// Construct data
			String User = uasername;
			String passwd = smspassword;
			String mobilenumber = contact;
			String message = datamsg;
			String data = "user=" + URLEncoder.encode(User, "UTF-8");
			data += "&password=" + URLEncoder.encode(passwd, "UTF-8");
			data += "&message=" + URLEncoder.encode(message, "UTF-8");
			data += "&sender=" + URLEncoder.encode("JAHUNT", "UTF-8");
			data += "&mobile=" + URLEncoder.encode(mobilenumber, "UTF-8");
			data += "&type=" + URLEncoder.encode("3", "UTF-8");
			// Send data
			URL url = new URL(urls + data);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			wr.write(data);
			wr.flush();
			// Get the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			String sResult1 = "";
			while ((line = rd.readLine()) != null) {
				// Process line...
				sResult1 = sResult1 + line + " ";
			}
			wr.close();
			rd.close();
			return sResult1;
		} catch (Exception e) {
			System.out.println("Error SMS " + e);
			return "Error " + e;
		}
	}

	@RequestMapping(value = "/testimonial", method = RequestMethod.GET)
	public String testimonial(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {

			model.addAttribute("testCommand", new TestimonialCommand());

			return "testimonial";
		}

		return "adminlogin";

	}

	@RequestMapping(value = "/addtestimonial", method = RequestMethod.POST)
	public String savetestimonial(HttpServletRequest request,@ModelAttribute("testCommand") TestimonialCommand command,Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				int imagerandom = rand.nextInt(9000000) + 1000000;
				byte[] l = null;

				l = command.getLogo().getBytes();

				Map params = ObjectUtils.asMap("public_id",
						"locusera/testimonials/"
								+ command.getLogo().getOriginalFilename()
										.split("\\.[^\\.]*$")[0] + "-locusera-"
								+ imagerandom);
				Map uploadResult = null;

				uploadResult = cloudinary.uploader().upload(l, params);

				TestiMonial monial = new TestiMonial();

				monial.setCustomer_name(command.getName());
				monial.setTestimonial_content(command.getContentbody());
				monial.setTestimonial_image((String) uploadResult
						.get("public_id")
						+ "."
						+ (String) uploadResult.get("format"));

				int i = loginservice.saveTestimonial(monial);
				attributes.addFlashAttribute("add", "add");
				model.addAttribute("testCommand", new TestimonialCommand());

				// model.addAttribute("eventCommand", new EventCommand());

				/*
				 * String urle = cloudinary .url().secure(true) .transformation(
				 * new Transformation().width(600).height(
				 * 500)).generate((pid));
				 */

			} catch (Exception ex) {
				System.out.println(ex);

			}

			return "redirect:testimoniallist.do";

		}

		return "adminlogin";

	}

	@RequestMapping(value = "/testimoniallist", method = RequestMethod.GET)
	public String testimoniallist(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			try {
				List<TestiMonial> list = loginservice.getTestimonialsList();
				List<TestiMonial> monials = new ArrayList<TestiMonial>();
				for (TestiMonial monial : list) {
					monial.setTestimonial_image(cloudinary
							.url()
							.secure(true)
							.transformation(
									new Transformation().width(45).height(45))
							.generate((monial.getTestimonial_image())));

					monials.add(monial);
				}
				model.addAttribute("monials", monials);

				return "testimoniallist";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return "adminlogin";
	}
	@RequestMapping(value = "/testmonialdelete")
	public String testmonialdelete(HttpServletRequest request,
			RedirectAttributes attributes) {

		int id = Integer.parseInt(request.getParameter("delete_testimonial_id"));
		boolean flag = loginservice.testmonialdelete(id);

		if (flag) {

			attributes.addFlashAttribute("deleted", "deleted");

		} else {
			attributes.addFlashAttribute("error", "error");

		}
		return "redirect:" + request.getHeader("referer");

	}

/*	@RequestMapping(value = "/us", method = RequestMethod.GET)
	public String contactusdetails(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {

			model.addAttribute("contactus", loginservice.getConatactUsDetails());

			return "contactusdetails";
		}

		return "adminlogin";

	}*/

	@RequestMapping(value = "/addClient", method = RequestMethod.GET)
	public String clientlogo(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {		
    model.addAttribute("clientCommand", new ClientLogoCommand());
			return "addClientPage";
		}
		return "adminlogin";
	}
	@RequestMapping(value = "/editclient", method = RequestMethod.GET)
	public String editclient(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		
		if (session.getAttribute("user") != null) {
			try {
				ClientLogoCommand clientCommand=new ClientLogoCommand();
				
				int id= Integer.parseInt(request.getParameter("clientId"));
				Client_Logo logo =loginservice.editClient(id);
				clientCommand.setClientid(logo.getClient_id());
					
					clientCommand.setName(logo.getCompany_name());
					clientCommand.setOldpath(cloudinary
							.url()
							.secure(true)
							.transformation(
									new Transformation().width(45).height(45))
							.generate((logo.getClient_logo())));

				model.addAttribute("clientCommand", clientCommand);
			} catch (NumberFormatException e) {
				
				e.printStackTrace();
			}

			
			return "editclient";
		}

		return "adminlogin";

	}

	@RequestMapping(value = "/updateClient", method = { RequestMethod.GET,RequestMethod.POST }) 
	public String updateClient(ClientLogoCommand clientCommand,HttpServletRequest request, Model model,RedirectAttributes attributes) {

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {
			try {
					int imagerandom = rand.nextInt(9000000) + 1000000;
					byte[] l = null;

					l = clientCommand.getPath().getBytes();
					if (l != null) {
						if (l.length > 0) {

							Map params = ObjectUtils.asMap(
									"public_id",
									"locusera/logo/"
											+ clientCommand.getPath()
													.getOriginalFilename()
													.split("\\.[^\\.]*$")[0]
											+ "-locusera-" + imagerandom);
							Map uploadResult = null;

							uploadResult = cloudinary.uploader().upload(l, params);
							clientCommand.setNewpath((String) uploadResult
									.get("public_id")
									+ "."
									+ (String) uploadResult.get("format"));

						}
					}
					boolean flag = loginservice.updateClient(clientCommand);
					attributes.addFlashAttribute("updated", "updated");

				} catch (Exception exception) {
					exception.printStackTrace();

				}

			return "redirect:viewlogos.do";

		}

		return "adminlogin";

	}
	
	@RequestMapping(value = "/deleteclient")
	public String deleteclient(HttpServletRequest request,
			RedirectAttributes attributes) {
		try {
			int id = Integer.parseInt(request.getParameter("delete_client_id"));

			boolean flag = loginservice.deleteClient(id);

			if (flag) {

				attributes.addFlashAttribute("deleted", "deleted");

			} else {
				attributes.addFlashAttribute("error", "error");

			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:" + request.getHeader("referer");

	}

	@RequestMapping(value = "/savelogo", method = RequestMethod.POST)
	public String savelogo(HttpServletRequest request,
			@ModelAttribute("clientCommand") ClientLogoCommand command,
			Model model,RedirectAttributes attributes) {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null) {
			try {
				int imagerandom = rand.nextInt(9000000) + 1000000;
				byte[] l = null;

				l = command.getPath().getBytes();

				Map params = ObjectUtils.asMap("public_id",
						"locusera/clientlogos/"
								+ command.getPath().getOriginalFilename()
										.split("\\.[^\\.]*$")[0] + "-locusera-"
								+ imagerandom);
				Map uploadResult = null;

				uploadResult = cloudinary.uploader().upload(l, params);

				Client_Logo logo = new Client_Logo();
				logo.setClient_logo((String) uploadResult.get("public_id")
						+ "." + (String) uploadResult.get("format"));
				logo.setCompany_name(command.getName());

				int i = loginservice.saveClientLogo(logo);
				attributes.addFlashAttribute("add", "add");
				model.addAttribute("clientCommand", new ClientLogoCommand());

				// model.addAttribute("eventCommand", new EventCommand());

				/*
				 * String urle = cloudinary .url().secure(true) .transformation(
				 * new Transformation().width(600).height(
				 * 500)).generate((pid));
				 */

			} catch (Exception ex) {
				System.out.println(ex);

			}

			return "redirect:viewlogos.do";

		}

		return "adminlogin";

	}


	@RequestMapping(value = "/viewlogos", method = RequestMethod.GET)
	public String viewlogos(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();

		try {
			if (session.getAttribute("user") != null) {
				List<Client_Logo> logos = loginservice.getClienttLogos();

				List<Client_Logo> list = new ArrayList<Client_Logo>();
				for (Client_Logo logo : logos) {
					logo.setClient_logo(cloudinary
							.url()
							.secure(true)
							.transformation(
									new Transformation().width(45).height(45))
							.generate((logo.getClient_logo())));

					list.add(logo);
				}

				model.addAttribute("logo", list);

				return "viewlogos";
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		return "adminlogin";

	}

	


}
