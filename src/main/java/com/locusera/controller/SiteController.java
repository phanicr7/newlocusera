package com.locusera.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.locusera.command.ChangePwdCommand;
import com.locusera.command.CommonView;
import com.locusera.command.EnquiryCommand;
import com.locusera.command.JobListCommand;
import com.locusera.command.LoginCommand;
import com.locusera.command.RegistrationCommand;
import com.locusera.modal.BlogArticals;
import com.locusera.modal.City;
import com.locusera.modal.Client_Logo;
import com.locusera.modal.Contact_Us;
import com.locusera.modal.Industry;
import com.locusera.modal.SubIndustry;
import com.locusera.modal.TestiMonial;
import com.locusera.modal.Events;
import com.locusera.modal.News;
import com.locusera.modal.Company;
import com.locusera.modal.CompanyAddress;
import com.locusera.modal.Job_Post;
import com.locusera.modal.Location;
import com.locusera.modal.UserRegistration;
import com.locusera.service.AdminLoginService;
import com.locusera.service.UserService;
import com.locusera.utility.IpCheck;


@Controller  
public class SiteController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private AdminLoginService loginservice;
	
	Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
			  "cloud_name", "locusera",
			  "api_key", "425233796544152",
			  "api_secret", "_Y8-k2R0gmrPp-YGc9CtD0tJR-Q"));
	@RequestMapping("/LEhome")
	public String LEhome(HttpServletRequest request,Model model){
		HttpSession  session = request.getSession();
		session.setAttribute("logout", "logout");
		List<TestiMonial> list=loginservice.getTestimonialsList();
		List<TestiMonial> monials=new ArrayList<TestiMonial>();
		for(TestiMonial monial:list)
		{
			monial.setTestimonial_image(cloudinary
	                .url().secure(true)
	                .transformation(
	                    new Transformation().width(120).height(
	                        120)).generate((monial.getTestimonial_image())));
			
			monials.add(monial);
		}
	    model.addAttribute("monials", monials);
		List<News> newsList = loginservice.getNewsList();
		List<Client_Logo> logos=loginservice.getClienttLogos();
		List<Client_Logo> logolist=new ArrayList<Client_Logo>();
		for(Client_Logo logo:logos)
		{
			logo.setClient_logo(cloudinary.url().secure(true) .transformation(new Transformation().width(250).height( 250)).generate((logo.getClient_logo())));
			
			logolist.add(logo);
		}
		List<Job_Post> jobList=  userService.getJobs();
		List<JobListCommand>  jobLists =  new ArrayList<JobListCommand>();
		for(Job_Post jP: jobList){
			
			JobListCommand jL = new JobListCommand();
		
			jL.setJob_id(jP.getJob_id());
			SubIndustry sI = jP.getSubindustry_id();
			Industry industry = sI.getIndustry();
		    jL.setIndustry(industry.getIndustryname());
			jL.setSub_industry(sI.getSubindustryname());
			jobLists.add(jL);
	
		}
		model.addAttribute("registerUser", new RegistrationCommand());
		model.addAttribute("loginCommand", new LoginCommand());
		model.addAttribute("jobLists",jobLists);
	    model.addAttribute("logo",logolist);
		model.addAttribute("newsList",newsList);
		return "LEhome";
	}
	
	@RequestMapping(value = "/LEabout", method = RequestMethod.GET)  
	public String LEabout(Model model) {  
		model.addAttribute("registerUser", new RegistrationCommand());
		model.addAttribute("loginCommand", new LoginCommand());
	  return "LEabout";  
	 }  
	@RequestMapping(value = "/LEservices", method = RequestMethod.GET)  
	public String LEservices(Model model) {  
		model.addAttribute("registerUser", new RegistrationCommand());
		model.addAttribute("loginCommand", new LoginCommand());
	  return "LEservices";  
	 }  
	
	@RequestMapping(value = "/LEcontact", method = RequestMethod.GET)  
	public String LEcontact(Model model) {  
		model.addAttribute("command",new EnquiryCommand());
		model.addAttribute("states",loginservice.getStates());
		model.addAttribute("registerUser", new RegistrationCommand());
		model.addAttribute("loginCommand", new LoginCommand());
	  return "LEcontact";  
	 }  
	@RequestMapping(value = "/contactform", method = RequestMethod.POST)  
	public String  contactform(Model model,@RequestParam("name")String name,@RequestParam("email")String email,@RequestParam("subject")String subject,@RequestParam("message")String message) {  
			Contact_Us contact=new Contact_Us();
			contact.setContact_name(name);
			contact.setContact_email(email);
			contact.setContact_message(message);
			contact.setContact_subject(subject);
		    loginservice.savecontactus(contact);          
	              return "contact";  
		
}
	@RequestMapping(value = "/LEblog", method = RequestMethod.GET)  
	public String LEblog(Model model,HttpServletRequest request) { 
	List<BlogArticals> articals=new ArrayList<BlogArticals>();
	int pageIndex = 0;
    int totalNumberOfRecords = 0;
    int recordsPerPage =3;
    String sPageIndex = request.getParameter("pageIndex");
    if (sPageIndex == null) {
        pageIndex = 1;
      } else {
        pageIndex = Integer.parseInt(sPageIndex);
      }
    int resultsFrom = (pageIndex * recordsPerPage) - recordsPerPage;
    totalNumberOfRecords = loginservice.getTotalBlog();
	List<BlogArticals> blogArticals = loginservice.totalblog(resultsFrom,recordsPerPage);	
    int noOfPages = totalNumberOfRecords / recordsPerPage;
    if (totalNumberOfRecords > (noOfPages * recordsPerPage)) {
      noOfPages = noOfPages + 1;
    }
    ArrayList <Integer> al = new ArrayList<Integer>();

    for (int i = 1; i <= noOfPages; i++) {
      al.add(i);

    }
	for(BlogArticals artical:blogArticals)
	{
		
		artical.setArtical_logo_path( cloudinary
                .url().secure(true)
                .transformation(
                    new Transformation().width(400).height(
                        240)).generate((artical.getArtical_logo_path())));
		articals.add(artical);
	}

	model.addAttribute("registerUser", new RegistrationCommand());
	model.addAttribute("loginCommand", new LoginCommand());
	model.addAttribute("blogArticals",articals);
	model.addAttribute("totalPages", al.size());
	  return "LEblog";  
	 } 

	@RequestMapping(value = "/enquiry", method = RequestMethod.GET)  
	public String enquiryform(Model model) {  
		model.addAttribute("command",new EnquiryCommand());
		model.addAttribute("states",loginservice.getStates());
	  return "enquiry";  
	}
	
	 @RequestMapping(value="/enquirySave")
	 public String enquirySave(EnquiryCommand enquiryCommand,RedirectAttributes model,HttpServletRequest request){
		 boolean flag =  userService.saveEnquiry(enquiryCommand);
			if(flag){
				 model.addFlashAttribute("success","success");
					return "redirect:"+request.getHeader("referer");
			}
			return  "redirect:LEhome.do";
	 }
	 @RequestMapping(value = "/check_enmail")
		public @ResponseBody Map<String, Boolean> check_enmail(@RequestParam("email") String mail) {
				    Map<String, Boolean> emailmap = null;
				    try {
				      boolean b = userService.CheckEnMail(mail);
				      emailmap = new HashMap<String, Boolean>();
				      System.out.println(b);
				      if (b) {
				        emailmap.put("valid", b);
				      } else
				        emailmap.put("invalid", b);
				    } catch (Exception e) {

				      e.printStackTrace();
				    }
				    return emailmap;
				  }
	 @RequestMapping(value = "/check_enphone")
		public @ResponseBody Map<String, Boolean> check_enphone(@RequestParam("phone")long phone) {
				    Map<String, Boolean> phonemap = null;
				    try {
				      boolean b = userService.CheckEnPhone(phone);
				      phonemap = new HashMap<String, Boolean>();
				      System.out.println(b);
				      if (b) {
				    	  phonemap.put("valid", b);
				      } else

				    	  phonemap.put("invalid", b);
				    } catch (Exception e) {

				      e.printStackTrace();
				    }

				    return phonemap;
				  }
	@RequestMapping(value = "/LEjobs", method = RequestMethod.GET)  
	public String LEjobs(Model model,HttpServletRequest request) {  
		/*List<Job_Post> jobList=  userService.getJobs();*/
		
		List<JobListCommand>  jobLists =  new ArrayList<JobListCommand>();
		
		int pageIndex = 0;
	    int totalNumberOfRecords = 0;
	    int recordsPerPage =3;
	    String sPageIndex = request.getParameter("pageIndex");
	    if (sPageIndex == null) {

	        pageIndex = 1;
	      } else {
	        pageIndex = Integer.parseInt(sPageIndex);
	      }
	    int resultsFrom = (pageIndex * recordsPerPage) - recordsPerPage;
	    totalNumberOfRecords = loginservice.getTotaljobs();
		List<Job_Post> jobList = loginservice.totaljobs(resultsFrom,recordsPerPage);	
	    
	    
	    
	    int noOfPages = totalNumberOfRecords / recordsPerPage;
	    if (totalNumberOfRecords > (noOfPages * recordsPerPage)) {
	      noOfPages = noOfPages + 1;
	    }
	    ArrayList <Integer> al = new ArrayList<Integer>();

	    for (int i = 1; i <= noOfPages; i++) {
	      al.add(i);

	    }
		for(Job_Post jP: jobList){
			JobListCommand jL = new JobListCommand();
			jL.setJob_id(jP.getJob_id());
			jL.setNo_openings(jP.getNo_openings());
			jL.setApplied_as(jP.getApplied());
			CompanyAddress cAddress  = jP.getAddress();
			Company cDetails =cAddress.getCompany();
			Location location =	cAddress.getLocation();
			City city = location.getCity();
			jL.setJob_title(jP.getJobtitle());
			jL.setExperience(jP.getExperience());
			jL.setCompany_name(cDetails.getCompany_name());
			jL.setCity_name(city.getCityname());
			jL.setLocation_name(location.getLocationname());
			jL.setQualification_details(jP.getQualification_details());
			jL.setSkills_required(jP.getSkills_required());
			SubIndustry subindustry =jP.getSubindustry_id();
			Industry industry =subindustry.getIndustry();
			jL.setIndustry(industry.getIndustryname());
			jL.setSub_industry(subindustry.getSubindustryname());
			jL.setJob_status(jP.getJob_status());
			jobLists.add(jL);
		}
		model.addAttribute("industry", loginservice.getIndustrys());
		model.addAttribute("registerUser", new RegistrationCommand());
		model.addAttribute("loginCommand", new LoginCommand());
		model.addAttribute("jobLists",jobLists);
		model.addAttribute("totalPages", al.size());
		model.addAttribute("joblistCommand",new JobListCommand());
	  return "LEjobs";  
	  
	 }  
	@SuppressWarnings("unchecked")
	@RequestMapping(value={"/jobSave","/guestJob"})
	 public String jobSave(JobListCommand joblistCommand,HttpServletRequest request,RedirectAttributes reAttributes){
		 try{
		System.out.println(joblistCommand.getCandidate_email());
		String restOfTheUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
	      if("/jobSave.do".equalsIgnoreCase(restOfTheUrl )){
	    	  joblistCommand.setApplied_as("RegisteredUser");
	      }
	      else{
	    	  joblistCommand.setApplied_as("GuestUser");
	      }
		Random rand = new Random();
		int imagerandom = rand.nextInt(9000000) + 1000000;
		 byte[] l = null;

			try {
				String filaename=joblistCommand.getCandidate_resume().getOriginalFilename();
				l = joblistCommand.getCandidate_resume().getBytes();
            Map params = ObjectUtils.asMap("public_id", "locusera/resume/"+filaename.split("\\.[^\\.]*$")[0]+"-locusera-"+imagerandom);
		    Map uploadResult = null;
		    String extension = FilenameUtils.getExtension(filaename);
		    params.put("resource_type", "auto");
	        params.put("format", extension);
	        uploadResult = cloudinary.uploader().upload(l, params);
			System.out.println((String) uploadResult.get("url"));
			joblistCommand.setResume_path((String) uploadResult.get("url"));
			}
			catch(Exception exception){
				exception.printStackTrace();
			}
		 userService.saveJob(joblistCommand);
		 }
		 catch(Exception exception){
			 exception.printStackTrace();
			 
		 }
		 reAttributes.addFlashAttribute("success","success");
		return "redirect:LEjobs.do";
	}
	
	@RequestMapping(value = "/userlogin", method = { RequestMethod.POST,
			RequestMethod.GET  })
	public @ResponseBody
	Map<String, Boolean> remoteUsernameCheck(@RequestParam("userid") String username,@RequestParam("pws") String password ) {
		
				
		
		Map<String, Boolean> usermap = new HashMap<String, Boolean>();
		int count = userService.checkUserCredentials(username,password);
		if(count!=0){
			usermap.put("valid",
					true);
		}
		else{
			usermap.put("invalid",
					false);

		}

		return usermap;
	}
	@RequestMapping(value="/logging",method={RequestMethod.GET,RequestMethod.POST})
	public String logging(LoginCommand loginCommand,HttpServletRequest request){
		
		UserRegistration list = userService.checkUser(loginCommand);
		System.out.println(list);
		if(list!=null){
			HttpSession  session = request.getSession(true);
			session.setAttribute("frontUserLoggedIn", "frontUserLoggedIn");
			session.setAttribute("userList", list);
			return "redirect:"+request.getHeader("referer");	
			
		}
		else{
			
			return "redirect:LEhome.do";
			
		}
} 
	
	@RequestMapping("/LEforgotPass")
	public String LEforgotPass(Model model,HttpServletRequest request){
		HttpSession session=request.getSession();
		if(session.getAttribute("frontUserLoggedIn")==null)
		{
	model.addAttribute("registerUser", new RegistrationCommand());
	model.addAttribute("loginCommand", new LoginCommand());	
	return "LEforgotPass";
		}
		else{
			return "redirect:LEhome.do";
			
		}
		
	}
	
	@RequestMapping(value = {"/check_userphone","/check_phoneforOTP"})
	public @ResponseBody Map<String, Boolean> check_userphone(@RequestParam("mobile") long mobile,HttpServletRequest request) {
			    Map<String, Boolean> phonemap = null;
			    try {
			    	String restOfTheUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
			      boolean b = userService.checkUserPhone(mobile);
			      phonemap = new HashMap<String, Boolean>();
			      if("/check_userphone.do".equalsIgnoreCase(restOfTheUrl )){
			    	  if (b) {
			    		  phonemap.put("valid", b);
					      } else

					    	  phonemap.put("invalid", b);
			        }
			        else{
			        	
			        	if (b) {
					    	  phonemap.put("invalid", false);
					      } else{

					    	  phonemap.put("valid", true);
					      }
			        	
			        }
			    } catch (Exception e) {

			      e.printStackTrace();
			    }

			    return phonemap;
			  }
	
	@RequestMapping("/send_OTP")
	public String send_OTP(@RequestParam("mobile") long mobile,HttpServletRequest request,RedirectAttributes redirectAttributes){
		
		
		try{
		OkHttpClient client = new OkHttpClient();
		
		Random rand = new Random();
	    int otp = rand.nextInt(90000) + 10000;
        Request requestt = new Request.Builder()
            .url("https://2factor.in/API/V1/a5bcf60f-d605-11e5-9a14-00163ef91450/SMS/"
                + mobile + "/" + otp + "").post(new okhttp3.RequestBody() {
              @Override
              public void writeTo(BufferedSink arg0)
                  throws IOException {
               

              }

              @Override
              public okhttp3.MediaType contentType() {
               
                return null;
              }
            }).build();
        try {
          Response response = client.newCall(requestt).execute();

          HttpSession session = request.getSession();
          if(response.isSuccessful()){
        	  System.out.println(otp);
        	  session.setAttribute("encryptOTP", otp);
        	  session.setAttribute("OTPmobile", mobile);
        	  redirectAttributes.addFlashAttribute("SentOTP","SentOTP");
        	  
          }
          else{
        	  
        	  redirectAttributes.addFlashAttribute("NotSentOTP","NotSentOTP");
          }
          
        } catch (IOException e) {
          
          e.printStackTrace();
          
          redirectAttributes.addFlashAttribute("NotSentOTP","NotSentOTP");
        }
		}
		catch(Exception exception){
			
			exception.printStackTrace();
			  redirectAttributes.addFlashAttribute("NotSentOTP","NotSentOTP");
		}
      
		return "redirect:LEforgotPass.do";
	}
	
	@RequestMapping("/check_OTP")
	public String checkOTP(HttpServletRequest request,RedirectAttributes redirectAttributes){
		try{
		
			HttpSession session = request.getSession();
		int sentOTP = (int) session.getAttribute("encryptOTP");
			
			System.out.println(sentOTP);
			
			int enteredOTP = Integer.parseInt(request.getParameter("otp"));
			if(sentOTP==enteredOTP){
				
			
				
				return "redirect:LEnewPassword.do";
			}
			else{
				
				redirectAttributes.addFlashAttribute("InvalidOTP","InvalidOTP");	
				return "redirect:LEforgotPass.do";
				
			}

		}
		catch(Exception exception){
			
		
			redirectAttributes.addFlashAttribute("InvalidOTP","InvalidOTP");
			
			return "redirect:LEforgotPass.do";
		}
	}
	
	@RequestMapping("/LEnewPassword")
	public String LEnewPassword(Model model,HttpServletRequest request){
		
		HttpSession session = request.getSession();
		
		if(session.getAttribute("OTPmobile")!=null){
			model.addAttribute("registerUser", new RegistrationCommand());
			model.addAttribute("loginCommand", new LoginCommand());	
		 model.addAttribute("changePwdCommand",new ChangePwdCommand());
		 return "LEnewPassword";
		}
		else{
			
			return "redirect:LEhome.do";
		}
		
		
	}
	
	@SuppressWarnings("unused")
	@RequestMapping("/newPassUpdate")
	public String newPassUpdate(@ModelAttribute("changePwdCommand")ChangePwdCommand command,HttpServletRequest request,RedirectAttributes redirectAttributes){
		HttpSession session = request.getSession();
		
		long mobile = (long) session.getAttribute("OTPmobile");
		
		boolean flag = userService.updateNewPass(mobile, command.getPwd());
		redirectAttributes.addFlashAttribute("newPass","newPass");
		return "redirect:LEhome.do";
		
	}

	
	@RequestMapping(value = "/check_useremail")
	public @ResponseBody Map<String, Boolean> check_useremail(@RequestParam("email") String mail,HttpServletRequest request) {
	    Map<String, Boolean> emailmap = null;
	    try {
	      boolean b = userService.checkUserEMail(mail);
	      emailmap = new HashMap<String, Boolean>();
	      System.out.println(b);
	      if (b) {
	        emailmap.put("valid", b);
	      } else

	        emailmap.put("invalid", b);
	    } catch (Exception e) {

	      e.printStackTrace();
	    }

	    return emailmap;
	  }
	
	@RequestMapping(value = "/check_username")
	public @ResponseBody Map<String, Boolean> check_username(@RequestParam("username") String username) {
      
			    Map<String, Boolean> usernamemap = null;
			    try {
			      boolean b = userService.checkUserName(username);
			      usernamemap = new HashMap<String, Boolean>();
			      if (b) {
			        usernamemap.put("valid", b);
			      } else

			        usernamemap.put("invalid", b);
			    } catch (Exception e) {

			      e.printStackTrace();
			    }
			    return usernamemap;
			  }

   @RequestMapping(value = "/check_currentPassword", method = {
		      RequestMethod.POST, RequestMethod.GET })
		  public @ResponseBody
		  Map<String, Boolean> check_currentPaswword(
		      @RequestParam("cpwd") String old_password,
		      HttpServletRequest request) {
	        Map<String, Boolean> usermap = new HashMap<String, Boolean>();
		    UserRegistration userRegistration = (UserRegistration) request.getSession().getAttribute("userList");

		      if(userRegistration.getLe_user_pwd().equals(old_password)){
		        
		    	  usermap.put("valid",true);
		      }
		      else{
		    	  
		    	  usermap.put("invalid",false);
		      }
		      return usermap;
		  }

	@RequestMapping(value = "/LEchangePass")  
	public String LEchangePass(Model model,HttpServletRequest request) { 
		HttpSession session=request.getSession();
		if(session.getAttribute("frontUserLoggedIn")!=null)
		{
         model.addAttribute("changePwdCommand",new ChangePwdCommand());
         model.addAttribute("registerUser", new RegistrationCommand());
 		model.addAttribute("loginCommand", new LoginCommand());
         return "LEchangePass";
		}
		else{
			
			return "redirect:LEhome.do";
		}
		
	}
	
	@RequestMapping(value="/pwdsave")
	public String userpwd(HttpServletRequest request,RedirectAttributes model,@ModelAttribute("changePwdCommand")ChangePwdCommand command){
		HttpSession session=request.getSession();
		if(session.getAttribute("frontUserLoggedIn")!=null)
		{
		UserRegistration data=(UserRegistration)session.getAttribute("userList");
		UserRegistration userdata=userService.updatepassword(data.getLe_user_id(), command.getPwd());
			session.setAttribute("userList", userdata);
            model.addFlashAttribute("changed","changed");
            
            return "redirect:LEchangePass.do";
		}
		else{
			return "redirect:LEhome.do";
			
		}
		 
	}
	   @RequestMapping("/saveUser")
	   public String saveUser(@ModelAttribute("registerUser")RegistrationCommand registrationCommand,RedirectAttributes model,HttpServletRequest request){
		String client_ip= IpCheck.getIpFromRequest(request);
		registrationCommand.setClient_ip(client_ip);
		boolean flag = userService.saveUser(registrationCommand);
		if(flag){
			
			 model.addFlashAttribute("success","success");
				return "redirect:LEhome.do";
		}
		else{
			
			model.addFlashAttribute("failure","failure");
			return "redirect:LEhome.do";
		}
	}
    
		@RequestMapping("/userLogout")
		public String userLogout(HttpServletRequest request){
			HttpSession  session = request.getSession(true);
			session.removeAttribute("frontUserLoggedIn");
			session.removeAttribute("userList");
			
			return "redirect:"+request.getHeader("referer");
		}
		
		@RequestMapping("/aboutNews")
		public String aboutNews(HttpServletRequest request,Model model){
			int news_id =Integer.parseInt(request.getParameter("id"));
			CommonView commonView = new CommonView();
			News news = loginservice.getNewsById(news_id);
			commonView.setHeading(news.getNews_heading());
			commonView.setTitle(news.getNews_title());
			commonView.setContent(news.getNews_content());
			commonView.setImage(cloudinary.url().transformation(new Transformation().width(350)).generate((news.getNews_logo())));
			model.addAttribute("data",commonView);
			model.addAttribute("registerUser", new RegistrationCommand());
			model.addAttribute("loginCommand", new LoginCommand());
			return "LEview";
		}
		@RequestMapping("/aboutEvent")
		public String aboutEvent(HttpServletRequest request,Model model){
			int event_id =Integer.parseInt(request.getParameter("id"));
			CommonView commonView = new CommonView();
			Events event = loginservice.getEventById(event_id);
			commonView.setHeading(event.getEvent_heading());
			commonView.setTitle(event.getEvent_title());
			commonView.setContent(event.getEvent_content());
			commonView.setImage(cloudinary.url().transformation(new Transformation().width(350)).generate((event.getEvent_logo())));
			model.addAttribute("data",commonView);
			model.addAttribute("registerUser", new RegistrationCommand());
			model.addAttribute("loginCommand", new LoginCommand());
			return "LEview";
		}
		@RequestMapping("/aboutBlog")
		public String aboutBlog(HttpServletRequest request,Model model){
			
			int blog_id =Integer.parseInt(request.getParameter("id"));
			CommonView commonView = new CommonView();
			BlogArticals blogArticals = loginservice.getBlogById(blog_id);
			commonView.setHeading(blogArticals.getArtical_heading());
			commonView.setTitle(blogArticals.getArtical_title());
			commonView.setContent(blogArticals.getArtical_content());
			commonView.setImage(cloudinary.url().transformation(new Transformation().width(350)).generate((blogArticals.getArtical_logo_path())));
			model.addAttribute("data",commonView);
			model.addAttribute("registerUser", new RegistrationCommand());
			model.addAttribute("loginCommand", new LoginCommand());
			return "LEview";
		}
	
		@RequestMapping(value = "/Lejobsearch", method = {RequestMethod.GET,RequestMethod.POST})
		public String searchindustry(HttpServletRequest request, Model model) {
			
			List<JobListCommand>  jobLists =  new ArrayList<JobListCommand>();
			/*int pageIndex = 0;
		    int totalNumberOfRecords = 0;
		    int recordsPerPage =3;
		    String sPageIndex = request.getParameter("pageIndex");
		    if (sPageIndex == null) {

		        pageIndex = 1;
		      } else {
		        pageIndex = Integer.parseInt(sPageIndex);
		      }
		    int resultsFrom = (pageIndex * recordsPerPage) - recordsPerPage;
		    totalNumberOfRecords = loginservice.getTotaljobs();
			List<Job_Post> jobListn = loginservice.totaljobs(resultsFrom,recordsPerPage);	
		    
		    
		    
		    int noOfPages = totalNumberOfRecords / recordsPerPage;
		    if (totalNumberOfRecords > (noOfPages * recordsPerPage)) {
		      noOfPages = noOfPages + 1;
		    }
		    ArrayList <Integer> al = new ArrayList<Integer>();

		    for (int i = 1; i <= noOfPages; i++) {
		      al.add(i);

		    }*/
			 Random random = new Random();

				int firstnumber = random.nextInt(10);
				int secondnumber = random.nextInt(10);
				model.addAttribute("firstnumber", firstnumber);
				model.addAttribute("secondnumber", secondnumber); 
			
				try {
					int id= Integer.parseInt(request.getParameter("industryid"));
								if( (id!=-1)) {
									List<Job_Post> jobList= userService.jobindustry(id);
									
									for(Job_Post jP: jobList){
										JobListCommand jL = new JobListCommand();
										jL.setJob_id(jP.getJob_id());
										jL.setNo_openings(jP.getNo_openings());
										CompanyAddress cAddress  = jP.getAddress();
										Company cDetails =cAddress.getCompany();
										Location location =	cAddress.getLocation();
										City city = location.getCity();
										jL.setJob_title(jP.getJobtitle());
										jL.setExperience(jP.getExperience());
										jL.setCompany_name(cDetails.getCompany_name());
										jL.setCity_name(city.getCityname());
										jL.setLocation_name(location.getLocationname());
										jL.setQualification_details(jP.getQualification_details());
										jL.setSkills_required(jP.getSkills_required());
										SubIndustry subindustry =jP.getSubindustry_id();
										Industry industry =subindustry.getIndustry();
										jL.setIndustry(industry.getIndustryname());
										jL.setSub_industry(subindustry.getSubindustryname());
										jL.setJob_status(jP.getJob_status());
										jobLists.add(jL);
									}
									
								
									model.addAttribute("industry", loginservice.getIndustrys());
									model.addAttribute("registerUser", new RegistrationCommand());
									model.addAttribute("loginCommand", new LoginCommand());
									model.addAttribute("jobLists",jobLists);

									model.addAttribute("joblistCommand",new JobListCommand());

								}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
						return "Lejobsearch";

						
		}
  }
		
