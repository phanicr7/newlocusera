package com.locusera.service;

public interface EmailService {
	public void sendEmail(String mailto,String body);
}
