package com.locusera.dao;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.locusera.command.ArticalCommand;
import com.locusera.command.Citycommand;
import com.locusera.command.ClientLogoCommand;
import com.locusera.command.CompanyAddressCommand;
import com.locusera.command.CompanyCommand;
import com.locusera.command.EventCommand;
import com.locusera.command.IndustryCommand;
import com.locusera.command.JobCommand;
import com.locusera.command.LocationCommand;
import com.locusera.command.NewsCommand;
import com.locusera.command.ProfileCommand;
import com.locusera.command.Statecommand;
import com.locusera.command.SubIndustryCommand;
import com.locusera.modal.AdminRegistration;
import com.locusera.modal.BlogArticals;
import com.locusera.modal.City;
import com.locusera.modal.Client_Logo;
import com.locusera.modal.Company;
import com.locusera.modal.CompanyAddress;
import com.locusera.modal.Contact_Us;
import com.locusera.modal.Events;
import com.locusera.modal.Industry;
import com.locusera.modal.Job_Post;
import com.locusera.modal.Location;
import com.locusera.modal.News;
import com.locusera.modal.State;
import com.locusera.modal.SubIndustry;
import com.locusera.modal.TestiMonial;
import com.locusera.modal.UserRegistration;

public interface AdminLoginDao {
	 public  AdminRegistration login(String username,String password);
	 public int saveartical(BlogArticals artical);
	 public int savenews(News news);
	 public int saveevent(Events event);
	 public int savejob(Job_Post job);
	 public int savesate(State state);
	 public int savecity(Citycommand command);
	 public Map<Integer,String> getcitys(int stateid);
	 public Map<Integer, String> getjobs(int id);
	 public int savelocation(LocationCommand command);
	 public List<State> getStates();
	 public Map<Integer, String> getlocations(int cityid);
	 public AdminRegistration updateprofileinfo(int id,ProfileCommand command);
  	 public AdminRegistration updatepassword(int id,String pwd);
  	 public List<UserRegistration> getusers();
  	 public int  saveindustry(Industry commnand);
  	 public List<Industry> getIndustrys();
  	 public int savesubindustry(SubIndustryCommand command);
  	 public List<ResumeDataDao> getResumes();
  	 public Location getLocation(int id);
	 public SubIndustry getsubindustry(int id);
	 public int  savecompany(Company company);
	 public List<Company> getCompanys();
	 public int address(CompanyAddressCommand command);
	 public Map<Integer,String> getcompanylocations(int id);
	 public CompanyAddress getCompanyAddress(int id);
	 public List<Events> getEventList();
	 public List<News> getNewsList();
	 public List<JobsDao> getjobs();
	 public List<EnquiryDao> getEnquirydetails();
	 public List<BlogArticals> getArticals();
	 public List<Object[]> getdetails(int id);
	 public News getNewsById(int news_id);
	 public Events getEventById(int event_id);
	 public int  saveTestimonial(TestiMonial monial);
	 public List<TestiMonial> getTestimonialsList();
	 public int  savecontactus(Contact_Us contact);
	 public BlogArticals getBlogById(int blog_id);
	 public List<Contact_Us> getConatactUsDetails();
	 public List<Client_Logo> getClienttLogos();
	 public int saveClientLogo(Client_Logo logo);
	 public boolean deleteBlog(int id);
     public boolean deleteNews(int id);
	 public boolean deleteClient(int id);
     public boolean deleteEvent(int id);
	 public boolean deletEnquiry(int id);
 	 public BlogArticals editArticle(int articalId);
	 public boolean deleteJob(int id);
	 public boolean updateArticle(ArticalCommand articalCommand);
	 public News editNews(int id);
     public boolean updateNews(NewsCommand newsCommand);
	 public Events editEvents(int id);
	 public boolean updateEvent(EventCommand eventCommand);
     public Job_Post editJob(int id);
   	 public Client_Logo editClient(int id);
	 public boolean updateClient(ClientLogoCommand clientCommand);
	 public List<Industry> getIndustryList();
	 public List<ResumeDataDao> getResumesByIndustry(int id);
	 public List<ResumeDataDao> getResumesBySubInd(List<Integer> allIds);
	 public List<ResumeDataDao> getResumesByDate(Date fromDate,Date toDate);
		public boolean updateJob(JobCommand jobCommand);
		public List<LocationCommand> getLocations();
		public List<LocationCommand> getCity();
		public List<SubIndustryCommand> getSubIndustryList();
		public boolean deleteState(int id);
		public State editState(int id);
		public boolean deleteCity(int id);
		public boolean deleteLocation(int id);
		public boolean deleteIndustry(int id);
		public boolean deleteCompany(int id);
		public boolean deleteSubIndustry(int id);
		public Industry editIndustry(int id);
		public boolean updateState(Statecommand statecommand);
		public boolean updateIndustry(IndustryCommand industryCommand);
		public Company editCompany(int id);
		public City editCity(int id);
		public boolean updateCompany(CompanyCommand companyCommand);
		public Location editLocation(int id);
		public boolean updatecity(LocationCommand command);
		public SubIndustry editSub(int id);
		public boolean updateSub(SubIndustryCommand subindustrycommand);
		public boolean updatelocation(LocationCommand locationCommand);
		public boolean testmonialdelete(int id);
		public int getTotalBlog();
		public List<BlogArticals> totalblog(int resultsFrom, int recordsPerPage);
		public int getTotaljobs();
		public List<Job_Post> totaljobs(int resultsFrom, int recordsPerPage);


   }
