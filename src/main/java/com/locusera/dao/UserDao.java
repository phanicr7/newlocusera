package com.locusera.dao;

import java.util.List;

import com.locusera.command.EnquiryCommand;
import com.locusera.command.JobListCommand;
import com.locusera.command.LoginCommand;
import com.locusera.command.RegistrationCommand;
import com.locusera.modal.Job_Post;
import com.locusera.modal.UserRegistration;

public interface UserDao {
    public boolean saveUser(RegistrationCommand registrationCommand);
	
	public UserRegistration checkUser(LoginCommand loginCommand);
	
	public boolean saveEnquiry(EnquiryCommand command);
	
	public boolean checkUserEMail(String mail);

	public boolean checkUserName(String username);

	public List<Job_Post> getJobs();

	public boolean saveJob(JobListCommand joblistCommand);

	public UserRegistration updatepassword(int id, String pwd);

	public boolean checkUserPhone(long phone);
	
	public boolean updateNewPass(long mobile, String pwd);

	public boolean CheckEnMail(String mail);

	public boolean CheckEnPhone(long phone);
	
	public int checkUserCredentials(String username, String password);
	public List<Job_Post> jobindustry(int id);




}
