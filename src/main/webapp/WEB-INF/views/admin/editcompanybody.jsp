<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--close-left-menu-stats-sidebar-->

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Company Management</a><a href="#" class="tip-bottom current">Edit Company</a> </div>
  
</div>


<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span7" >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5> Edit Company</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form id="editcompany_form" action="updateCompany.do" method="post" class="form-horizontal" commandName="companyCommand" enctype="multipart/form-data">
          
            <div class="control-group">
              <label class="control-label">Company Name :</label>
              <form:input path="company_id" type="hidden"/>
              <div class="controls">
                <form:input path="name" type="text" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email:</label>
              <div class="controls">
                <form:input path="mail" type="text" placeholder="E-Mail" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Number:</label>
              <div class="controls">
                <form:input path="number" type="text"  placeholder="Number" />
              </div>
            </div>
		
            <div class="control-group">
              <label class="control-label">Company Info :</label>
              <div class="controls">
                <form:textarea path="info"  ></form:textarea>
              </div>
            </div>
          
       <form:input type="hidden" path="company_id"/>
           
             <div class="control-group">
              <label class="control-label">Previous Image</label>
              <div class="controls">
                <img src="<c:url value="${companyCommand.oldpath}"/>"/>
              </div>
            </div>
            
			
			 <div class="control-group">
              <label class="control-label">Upload Company Logo :</label>
              <div class="controls">
                <form:input path="logo" type="file" class="field form-control"  />
              </div>
            </div>
			
			
            <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Update</form:button>
                                      <a href="viewCompany.do"><button type="button" class="btn btn-danger">Cancel</button></a>
              
            </div>
          </form:form>
        </div>
      </div>
	  
	  
	  
	  
  </div>
	
	
	
  </div>

</div></div>

</body>
</html>