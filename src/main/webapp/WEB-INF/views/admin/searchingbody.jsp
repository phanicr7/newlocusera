 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Resume Management</a> <a href="#" class="current">Filter Resumes</a> </div>
</div>
<div class="container-fluid">
       <h3>Filter Resumes</h3>

  <hr>
  <div class="row-fluid">
    <div class="span7">
     
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Search</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="#" method="get" class="form-horizontal">
          
            
            <div class="control-group">
              <label class="control-label">Resumes Search</label>
              <div class="controls">
                <label>
                  <input type="radio" name="radios" id="datewise" />
                  Date Search</label>
                <label>
                  <input type="radio" name="radios" id="categorywise" />
                  Industry Search</label>
                <label>
                  <input type="radio" name="radios" id="subcategorywise" />
                 Functional Area Search</label>
              </div>
            </div>
          
         
          </form>
        </div>
      </div>
      <div class="widget-box hidedate locuseraserch">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Date Search</h5>
        </div>
        <div class="widget-content nopadding">
         <form:form id="date_form"  action="dateSearch.do" method="post" class="form-horizontal" commandName="dateCommand">
            <div class="control-group">
              <label class="control-label">From Date</label>
              <div class="controls">
                <div class="">
                  <form:input type="date" path="from_date" class="span11" />
                  <span class="add-on"><i class="icon-th"></i></span> </div>
              </div>
            </div>
			<div class="control-group">
              <label class="control-label">To Date</label>
              <div class="controls">
                <div  class="">
                  <form:input type="date" path="to_date" class="span11" />
                  <span class="add-on"><i class="icon-th"></i></span> </div>
              </div>
            </div>
             <div class="form-actions">
              <button type="submit" class="btn btn-success">Search</button>
             
            </div>
          </form:form>
        </div>
      </div>
	   
	   <div class="widget-box hidecategory locuseraserch">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5> Industry Search</h5>
        </div>
        <div class="widget-content nopadding">
         <form:form id="<%-- ind_form --%>" action="searchinglist.do" method="post" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Select Industry</label>
              <div class="controls">
                <select  name="industryid" id="industryid">
                    <c:forEach items="${industry}" var="category" varStatus="status">
                                <c:choose>
                <c:when test="${status.index==0}"> <option value="0">Select Industry</option><option value="${category.industry_id}">${category.industryname}</option></c:when>
                   <c:otherwise><option value="${category.industry_id}">${category.industryname}</option></c:otherwise>
                                  </c:choose>
                
                 
                </c:forEach>
                </select>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Search</button>
            </div>
          </form:form>
        </div>
      </div>
	   <div class="widget-box hidesubcategory locuseraserch">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
     <h5>Functional Area Search</h5>
        </div>
        <div class="widget-content nopadding">
         <form:form id="sub_form" action="searchinglist.do" method="post" class="form-horizontal" >
            <div class="control-group">
       <label class="control-label">Select Functional Area</label>
          <div class="controls">
                <select  name="industryid" id="industry">
                   <c:forEach items="${industry}" var="category" varStatus="status">
           <c:choose>
           <c:when test="${status.index==0}"> <option value="0">Select Industry</option><option value="${category.industry_id}">${category.industryname}</option></c:when>
            <c:otherwise><option value="${category.industry_id}">${category.industryname}</option></c:otherwise>
     </c:choose>
     </c:forEach>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Multiple Select input</label>
              <div class="controls">
                <select multiple  name="subcatogory" id="subcatogory">
                </select>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Search</button>
            </div>
          </form:form>
        </div>
      </div>
    </div>
   
  </div>

</div></div>
</body>
</html>