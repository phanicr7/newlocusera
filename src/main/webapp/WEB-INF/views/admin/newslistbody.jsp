<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" >News</a> <a href="#" class="current">View Posted News</a></div>
     <c:if test="${deleted=='deleted'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong>Deleted successfully </strong> 
        </div>
        </c:if>
           <c:if test="${error=='error'}">
     <div class="alert alert-danger" id="alertFadeout">
        <strong>Try again</strong> 
        </div>
        </c:if>
  </div>
  <c:if test="${updated=='updated'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong>Updated successfully </strong> 
        </div>
        </c:if>
        <c:if test="${add=='add'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong>Posted a news successfully </strong> 
        </div>
        </c:if>
  <div class="container-fluid">
      <h3>View Posted News</h3>
  
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>View Posted News</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>News Logo</th>
                  <th>News Heading</th>
                  <th>News Title</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
               <c:forEach items="${news}" var="news" varStatus="status">
              <tr class="gradeA">
                  <td><img src="${news.news_logo}"></img></td>
                  <td>${news.news_heading}</td>
                  <td>${news.news_title}</td>
        <td class="center"><a href="editnews.do?NewsId=${news.news_id}" class="btn btn-success btn-mini">Edit</a>&nbsp;&nbsp;
       <a class="btn btn-danger btn-mini deleteClass" href="#deleteModal" news_id="${news.news_id}" data-toggle="modal">Delete</a></td>  
                </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 text-center"> 
<a href="addnews.do" class="btn  btn btn-primary" role="button">Add News</a>
</div>        
</div>
<div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm ">
     <div class="modal-content">
         <form action="newsdelete.do">
         <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <input type="hidden" id="delete_news_id"  name="delete_news_id" />
          <h5 class="modal-title">Are you sure to delete ?</h5>
        </div>
       <div class="modal-footer">
         <button type="submit"  class="btn btn-primary">Ok</button> 
       <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel
</button>       
   </div>
        </form>
      </div>
      </div>
      </div>
</body>
</html>