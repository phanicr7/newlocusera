<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--close-left-menu-stats-sidebar-->

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Location Management</a><a href="#" class="tip-bottom current">Edit City</a> </div>
  
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span7" >
              <h3>Edit City</h3>
    
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit City</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form id="editcity_form" action="updateCity.do" method="post" class="form-horizontal" commandName="command" >
          
			 <div class="control-group">
              <label class="control-label">State</label>
              <form:input path="city_id" type="hidden"/>
              <div class="controls">
                <form:select path="state" id="state">
                 <c:forEach items="${states}" var="state" varStatus="status">
                                        <form:option value="${state.state_id}">${state.statename}</form:option> 
                </c:forEach>
        </form:select>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">City Name :</label>
              <div class="controls">
                <form:input path="cityname" id="city" type="text" class="span11 field" placeholder="Enter CityName" />
              </div>
            </div>
             <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Update</form:button>
              <a href="viewCity.do"><button type="button" class="btn btn-danger">Cancel</button></a>
            </div>
          </form:form>
        </div>
      </div>
	 </div>
</div>
</div>
</div>

</body>
</html>