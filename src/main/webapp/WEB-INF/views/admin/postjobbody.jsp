<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--close-left-menu-stats-sidebar-->

<div id="content">
<div id="content-header">
  <div id="breadcrumb">
   <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
    <a href="#" class="tip-bottom">Jobs</a><a href="#" class="tip-bottom current">Post a Job</a> </div>
  
</div>
<c:if test="${alert!=null}">
<div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>${alert}</strong>
    
</div>
   
</c:if>

<div class="container-fluid">
            <h3>Post a Job</h3>
  <hr>
  <div class="row-fluid">
    <div class="span7" >
      <div class="widget-box">
         <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Post a Job</h5>
        </div> 
        <div class="widget-content nopadding">
          <form:form id="job_form" action="savejob.do" method="post" class="form-horizontal" modelAttribute="jobCommand" enctype="multipart/form-data" >

          <div class="control-group">
              <label class="control-label">Job Title :</label>
              <div class="controls">
              <form:input path="jobtitle"  id ="jobtitle" class="span11 field" placeholder="Title"/>
                
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Company Name :</label>
              <div class="controls">
                 <form:select path="companyname" id="companyname">
                  <c:forEach items="${company}" var="company" varStatus="status">
                  
                <c:choose>
                                         <c:when test="${status.index==0}"> <option value="0">Select Company</option><option value="${company.company_id}">${company.company_name}</c:when>
                                         <c:otherwise><option value="${company.company_id}">${company.company_name}</option></c:otherwise>
                                  </c:choose>
                </c:forEach>
                </form:select>
              </div>
            </div>
			 <div class="control-group">

              <label class="control-label">Location :</label>

              <div class="controls">
                <form:select path="locationname" id="locationname"  class="form-control selectpicker">
                 
                </form:select>
              </div>
            </div>
		
            <div class="control-group">
              <label class="control-label">No of Openings :</label>
              <div class="controls">
              <input name="openings"  id ="openings" value="" class="span11 field" placeholder="No of Openings" />
                
              </div>
            </div>
            
              <div class="control-group">
              <label class="control-label">Experience :</label>
              <div class="controls">
              <form:input path="experience"  id ="expr" value="" class="span11 field" placeholder="Experience" />
                <p style="color: red">Example : Fresher,1-2 Years,2-3 Years.,etc</p> 
              </div>
            </div>
            
			   <div class="control-group">
              <label class="control-label">Industry :</label>
              <div class="controls">
              <form:select name="catogory"  path="catogory"  id="category">
              <c:forEach items="${industry}" var="category" varStatus="status">
              <c:choose>
              <c:when test="${status.index==0}"> <option value="0">select Industry</option><option value="${category.industry_id}">${category.industryname}</option></c:when>
            <c:otherwise><option value="${category.industry_id}">${category.industryname}</option></c:otherwise>
             </c:choose>
              </c:forEach>
             </form:select>
              </div>
            </div>
            
               <div class="control-group">
              <label class="control-label">Functional Area :</label>
              <div class="controls">
                <form:select  path="subcatogory" id="subcategory">
                 </form:select>
              </div>
            </div>
         
          
            <div class="control-group">
              <label class="control-label">Qualification Details :</label>
              <div class="controls">
                <form:textarea path="qualification"  id="qualification" class="span11 field" ></form:textarea>
              </div>
            </div>
			<div class="control-group">
              <label class="control-label">Skills Required :</label>
              <div class="controls">
                <form:textarea path="skills"  id="skills" class="span11 field" ></form:textarea>
              </div>
            </div>
            
                       <div class="control-group">
              <label class="control-label">Job Posting Status :</label>
              <div class="controls">
              <form:select path="job_status"  id ="job_status" value="" class="span11 field">
               <form:option value="Active">Active</form:option>
               <form:option value="In-Active">In Active</form:option>
               </form:select>
              </div>
            </div>
            
            
       <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Save</form:button>
               <a href="postjob.do"><button type="button" class="btn btn-danger">Cancel</button></a>
              
            </div>
          </form:form>
        </div>
      </div>
</div>
 </div>

</div>
</div>

</body>
</html>
