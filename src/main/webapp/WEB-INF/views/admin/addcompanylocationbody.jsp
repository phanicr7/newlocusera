<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--close-left-menu-stats-sidebar-->

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Location Management</a><a href="#" class="tip-bottom current">Company Location</a> </div>
  
</div>


<div class="container-fluid">
          <h3>Add Location Details</h3>

  <hr>
  
  <div class="row-fluid">
    <div class="span7" >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <c:if test="${save=='save'}">
     <div class="alert alert-success">
        <strong> Company Location added successfully </strong> 
        </div>
        </c:if>
          <h5>Add Location Details</h5>
    
        </div>
          
        <div class="widget-content nopadding">
          <form:form id="companyloc_form" action="address.do" method="post" class="form-horizontal" modelAttribute="companylocationCommand" enctype="multipart/form-data">
          
          
           <div class="control-group">
              <label class="control-label">Company Name</label>
              <div class="controls"> 
                <form:select path="companyname" id="company" >
                  <c:forEach items="${company}" var="company" varStatus="status">
                  
                <c:choose>
                                         <c:when test="${status.index==0}"> <option value="0">Select Company Name</option><option value="${company.company_id}">${company.company_name}</c:when>
                                         <c:otherwise><option value="${company.company_id}">${company.company_name}</option></c:otherwise>
                                  </c:choose>
                </c:forEach>
                </form:select>
              </div>
            </div>
          
			 <div class="control-group">
              <label class="control-label">State</label>
              <div class="controls">
                <form:select path="statename" id="state">
                  <c:forEach items="${states}" var="state" varStatus="status">
                  
                <c:choose>
                                         <c:when test="${status.index==0}"> <option value="0">Select State</option><option value="${state.state_id}">${state.statename}</c:when>
                                         <c:otherwise><option value="${state.state_id}">${state.statename}</option></c:otherwise>
                                  </c:choose>
                </c:forEach>
                </form:select>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">City</label>
              <div class="controls">
                <form:select path="cityname" id="city" class="field">
                 
                </form:select>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Location</label>
              <div class="controls">
                <form:select path="locationname" id="location" class="field">
                 
                </form:select>
              </div>
            </div>
           
            <div class="control-group">
              <label class="control-label">Location Address:</label>
              <div class="controls">
                <form:textarea path="information"  id="information" class="span11 field" ></form:textarea>
              </div>
            </div>
          
           
            <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Save</form:button>
              <a href="companylocation.do"><button type="button" class="btn btn-danger">cancel</button></a>
               
            </div>
          </form:form>
        </div>
      </div>
	  
	  
	  
	  
  </div>
	
	
	
  </div>

</div></div>

</body>
</html>