<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title> <tiles:insertAttribute name="title" ignore="true"></tiles:insertAttribute></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="admin/css/bootstrap.min.css" />
<link rel="stylesheet" href="admin/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="admin/css/colorpicker.css" />
<link rel="stylesheet" href="admin/css/datepicker.css" />
<link rel="stylesheet" href="admin/css/uniform.css" />
<link rel="stylesheet" href="admin/css/select2.css" />
<link rel="stylesheet" href="admin/css/matrix-style.css" />
<link rel="stylesheet" href="admin/css/matrix-media.css" />
<link rel="stylesheet" href="admin/css/bootstrap-wysihtml5.css" />
<link href="admin/font-awesome/css/font-awesome.css" rel="stylesheet" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<style>
.logo {
   height: 77px;
    width: 220px;
}
.error{
color:red;
border-color: red !important;
}
.valid{
border-color: green!important;
}


</style>
</head>
<body>

<tiles:insertAttribute name="menu"></tiles:insertAttribute>
  <tiles:insertAttribute name="sidemenu"></tiles:insertAttribute>
  <tiles:insertAttribute name="body"></tiles:insertAttribute>
  <tiles:insertAttribute name="footer"></tiles:insertAttribute>     
<!--end-Footer-part--> 
<script src="admin/js/jquery.min.js"></script> 
<script src="admin/js/jquery.ui.custom.js"></script> 
<script src="admin/js/bootstrap.min.js"></script> 
<script src="admin/js/jquery.uniform.js"></script> 
<script src="admin/js/select2.min.js"></script> 
<script src="admin/js/jquery.validate.js"></script> 
<script src="admin/js/matrix.js"></script> 
<script src="admin/js/matrix.form_validation.js"></script>
<script src="admin/js/wysihtml5-0.3.0.js"></script> 
<script src="admin/js/jquery.peity.min.js"></script> 
<script src="admin/js/bootstrap-wysihtml5.js"></script> 
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js"></script> 
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>

$('#state').change(function(event) {
	
	 var $c=$("#state").val();
	   
	 
	   
	   if($c==0)
		   {
		      
		      var $city = $("#city");
		     var $location = $("#location"); 
		      $city.find('option').remove(); 
		     
		      
		      $('<option></option>').val("0").text("Select City").appendTo($city);
		      $location.val('');
		      
		      
		      
		   
		   }
	   else
		   {
		      
			
			$.ajax({
			    
			    url: "getcity.do",
			    type: 'GET',
			    dataType: 'json',
			    data: {id: $c },
			    contentType: 'application/json',
			    
			    success : function(response) {
			    	
			    	
			    	  var $select = $("#city");                           
				         
					  $select.find('option').remove();  
					 
					  
					  $('<option></option>').val("0").text("Select City").appendTo($select);
			        
			         $.each(response, function(key, value) {  
			        	   
			        	
							
			             $('<option></option>').val(key).text(value).appendTo($select);      
			              });
			    	
			    	
			    	
			    	
		           
			    },
			    error : function(error) {
			    	alert("some internal problem pls try again");
			    },
			
			});
			     
	     
		       
		       
		   
		   }
	
	

});






$('#city').change(function(event) {
	
	
	
	 var $c=$("#city").val();
	   
	 
	   
	   if($c==0)
		   {
		      
		      var $location = $("#location");
		    
		      $location.find('option').remove(); 
		     
		      
		      $('<option></option>').val("0").text("Select Location").appendTo($location);
		      
		      
		      
		      
		   
		   }
	   else
		   {
		      
			
			$.ajax({
			    
			    url: "getlocation.do",
			    type: 'GET',
			    dataType: 'json',
			    data: {id: $c },
			    contentType: 'application/json',
			    
			    success : function(response) {
			    	 
			    	
			    	  var $select = $("#location");                           
				         
					  $select.find('option').remove();  
					 
					  
					  $('<option></option>').val("0").text("Select City").appendTo($select);
			        
			         $.each(response, function(key, value) {  
			        	   
			        	
							
			             $('<option></option>').val(key).text(value).appendTo($select);      
			              });
			    	
			    	
			    	
			    	
		           
			    },
			    error : function(error) {
			    	alert("some internal problem pls try again");
			    },
			
			});
			     
	     
		       
		       
		   
		   }
	
	

});




$('#catogory').change(function(event) {
	
	
	
	 var $c=$("#catogory").val();
	   
	 
	   
	   if($c==0)
		   {
		      
		      var $location = $("#subcatogory");
		    
		      $location.find('option').remove(); 
		     
		      
		      $('<option></option>').val("0").text("Select Job Type").appendTo($location);
		      
		      
		      
		      
		   
		   }
	   else
		   {
		      
			
			$.ajax({
			    
			    url: "getjobtype.do",
			    type: 'GET',
			    dataType: 'json',
			    data: {id: $c },
			    contentType: 'application/json',
			    
			    success : function(response) {
			    	 
			    	
			    	  var $select = $("#subcatogory");                           
				         
					  $select.find('option').remove();  
					 
					  
					  $('<option></option>').val("0").text("Select Job Type").appendTo($select);
			        
			         $.each(response, function(key, value) {  
			        	   
			        	
							
			             $('<option></option>').val(key).text(value).appendTo($select);      
			              });
			    	
			    	
			    	
			    	
		           
			    },
			    error : function(error) {
			    	alert("some internal problem pls try again");
			    },
			
			});
			     
	     
		       
		       
		   
		   }
	
	

});

	$('.textarea_editor').wysihtml5();
	$('.field').val("");
</script>
<script type="text/javascript">


$(document).ready(function(){
	
	
	
	$('#editcompany_form').validate({ 
		
		        rules: {
		        	name: {
		                required: true

		            },
		           
		            mail: {
		                required: true,
		                email:true

		            },
		            number: {
		                required: true,
		                 number:true,
		                 minlength:10,
		                 maxlength:10
		                 },
		         info: {
		                required: true
		                
		                 
		         }
		        },
    messages: { 
    	name: 'CompanyName is required.',
    	mail: 'CompanyEmail is required.',
    	number: 'Company Mobilenumber is required.',
    	info: 'Company Information is required.'

    	
    	}
	        	
		    });

			
		});
		
		



</script>

</body>
</html>
