<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--sidebar-menu-->
   <div id="content">
   <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Welcome User</a> <a href="#" class="current">Change Password</a> </div>
    <h1>Change Password</h1>
    </div>
    <c:if test="${alert!=null}">
    <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>${alert}</strong>
    </div>
   
</c:if>
  <div class="container-fluid"><hr>

    <div class="row-fluid">
     
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i></span>
              <h5>Change Password</h5>
            </div>
            <div class="widget-content nopadding">
              <form:form class="form-horizontal" method="post" action="savepwd.do" modelAttribute="changeCommand" name="password_validate" id="password_validate" novalidate="novalidate">
                <div class="control-group">
                  <label class="control-label">Password</label>
                  <div class="controls">
                    <form:input type="password"  path="pwd" id="pwd" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Confirm Password</label>
                  <div class="controls">
                    <form:input type="password" path="pwd2" id="pwd2" />
                  </div>
                </div>
                <div class="form-actions">
                  <form:button type="submit"   class="btn btn-success modify">Submit</form:button>
                  
                </div>
              </form:form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</body>
</html>