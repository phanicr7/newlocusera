<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Resume Management</a> </div>
  </div>
  <div class="container-fluid">
      <h3>Search Resumes</h3>
  
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
  
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
          <h5>Search Resumes</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>S.no</th>
                  <th>Name</th>
                  <th>Industry</th>
                  <th>Functional Area</th>
                  <th>Company Name</th>
				  <th>Action</th>
				  
                </tr>
              </thead>
              <tbody>
              
              
               <c:forEach items="${resume}" var="user" varStatus="status">
              <tr class="gradeA">
                  <td>${status.index+1}</td>
                  <td>${user.name}</td>
                   <td>${user.catogory}</td>
                  <td class="center">${user.jobtype}</td>
                                   
                  
                  <td>${user.companyname}</td>
				 <td class="center"><a href="${user.resumeurl}"  class="btn btn-info btn-mini">Download Resume</a>&nbsp;&nbsp;</td>
				 
			
                </tr>
                </c:forEach>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
         <div class="col-md-4 text-center"> 
<a href="searchresume.do" class="btn  btn btn-success pull-right"  role="button">Back>></a>
</div> 
    </div>
  </div>
</div>

</body>
</html>