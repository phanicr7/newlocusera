<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
   <a href="#" class="tip-bottom">Manage Testimonials </a><a href="#" class="tip-bottom current">Add Testimonial</a> </div>
  
</div>

<div class="container-fluid">
              <h3>Add Testimonial</h3>

  <hr>
  <div class="row-fluid">
    <div class="span10" >
    
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add Testimonial</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form id="testimonial_form" action="addtestimonial.do" method="post" class="form-horizontal" modelAttribute="testCommand" enctype="multipart/form-data">
          
		
			
            <div class="control-group">
              <label class="control-label">Testimonial Name :</label>
              <div class="controls">
                <form:input type="text" path="name" class="span11 field" placeholder="Testimonial Name" />
              </div>
            </div>
            
            
              <div class="control-group">
              <label class="control-label">Content :</label>
              <div class="controls">
                <form:textarea path="contentbody" class="span11 field" ></form:textarea>
              </div>
            </div>
			
			
			
          
            
			 <div class="control-group">
              <label class="control-label">Upload Image</label>
              <div class="controls">
                <form:input type="file" path="logo" class="field"/>
              </div>
            </div>
			
		 <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Save</form:button>
                            <a href="testimonial.do"><button type="button" class="btn btn-danger">Cancel</button></a>
              
            </div>
          </form:form>
        </div>
      </div>
	  
	  
	  
</div>
	
	
	
  </div>

</div></div>

</body>
</html>
