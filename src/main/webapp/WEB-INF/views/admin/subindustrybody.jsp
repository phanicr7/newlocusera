<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Industry Management</a><a href="#" class="tip-bottom current">Add Functional Area</a> </div>
  
</div>
<div class="container-fluid">
              <h3>Add Functional Area</h3>

  <hr>
  <div class="row-fluid">
    <div class="span7" >
    
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add Functional Area</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form id="subindustry_form" action="savesubindustry.do" method="post" class="form-horizontal" modelAttribute="subindustryCommand" >
          
			 <div class="control-group">
              <label class="control-label">Industry Name</label>
              <div class="controls">
                <form:select path="id" id="industry">
                 <c:forEach items="${industry}" var="category" varStatus="status">
                                <c:choose>
                                         <c:when test="${status.index==0}"> <option value="0">Select Industry</option><option value="${category.industry_id}">${category.industryname}</option></c:when>
                                         <c:otherwise><option value="${category.industry_id}">${category.industryname}</option></c:otherwise>
                                  </c:choose>
                </c:forEach>
                </form:select>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label"> Functional Area :</label>
              <div class="controls">
                <form:input path="name"  id="subindustry" type="text" class="span11 field" placeholder="Enter Functional Area " />
              </div>
            </div>
            <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Save</form:button>
              <a href="addsubindustry.do"><button type="button" class="btn btn-danger">Cancel</button></a>
            </div>
          </form:form>
        </div>
      </div>
  </div>
  </div>
</div></div>
</body>
</html>