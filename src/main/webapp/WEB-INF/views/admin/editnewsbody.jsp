<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="#" class="tip-bottom">News</a><a href="#"  class="tip-bottom current">Edit News</a> </div>
  
</div>
<div class="container-fluid">
          <h3>Edit News</h3>

  <hr>
  <div class="row-fluid">
    <div class="span10" >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit News</h5>

          
        </div>
        <div class="widget-content nopadding">
          <form:form id="editnews_form" action="updateNews.do"  method="post" class="form-horizontal" commandName="newsCommand" enctype="multipart/form-data">

            <div class="control-group">
              <label class="control-label">News Title :</label>
              <div class="controls">
                <form:input type="text" class="span11" placeholder="News Title" path="title"/>
              </div>
            </div>
			
			 <div class="control-group">
              <label class="control-label">News Heading :</label>
              <div class="controls">
                <form:input type="text" class="span11" placeholder="News Heading" path="heading" />
              </div>
            </div>
			
          			 <form:input type="hidden" path="newsid"/>
          
            
             <div class="control-group">
              <label class="control-label">Previous Image</label>
              <div class="controls">
                <img src="<c:url value="${newsCommand.oldpath}"/>"/>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Upload Article Logo :</label>
              <div class="controls">
                <form:input type="file" path="logo"  />
              </div>
            </div>
			
			
			
			 
          <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Write News Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
          
            <div class="controls">
              <form:textarea name="content" class="textarea_editor span10" rows="6" placeholder="Enter text .." path="news_content"></form:textarea>
            </div>
          
        </div>
      </div>
    </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
       <a href="newslist.do"><button type="button" class="btn btn-danger">Cancel </button></a>
              
            </div>
          </form:form>
        </div>
      </div>
	  
	  
	  
	  
  </div>
	
	
	
  </div>

</div></div>
</body>
</html>