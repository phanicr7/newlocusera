<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">View Testimonial(s)</a> </div>
    
  </div>
  <div class="container-fluid">
      <h3>View Testimonials</h3>
  
    <div class="row-fluid">
      <div class="span12">
         <c:if test="${deleted=='deleted'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong>Deleted successfully </strong> 
        </div>
        </c:if>
           <c:if test="${error=='error'}">
     <div class="alert alert-danger" id="alertFadeout">
        <strong>Try again</strong> 
        </div>
        </c:if>
  </div>
  <c:if test="${add=='add'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong>Post a Testimonail successfully </strong> 
        </div>
        </c:if>
  
        <div class="widget-box">
     <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>View Testimonials</h5>
          
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Customer Name</th>
                  <th>Image</th>
                  <th>Action</th>
				  
                </tr>
              </thead>
              <tbody>
               <c:forEach items="${monials}" var="monial" varStatus="status">
              <tr class="gradeA">
                   <td>${monial.customer_name}</td>
                  <td><img src="${monial.testimonial_image}" height="45" width="45"></img></td>
               	<td class="center"><a href="#myModel${status.index.toString()}" data-toggle="modal" class="btn btn-success btn-mini">View Content</a>&nbsp;&nbsp;
    <a class="btn btn-danger btn-mini deleteClass" href="#deleteModal" testimonial_id="${monial.testimonial_id}" data-toggle="modal">Delete</a></td>  
    <div id="myModel${status.index.toString()}" class="modal hide">
              <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">�</button>
                <h3>Information</h3>
              </div>
              <div class="modal-body">
                <p>${monial.testimonial_content}</p>
                
              </div>
              </div>
				   
                </tr>
                </c:forEach>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
       <div class="col-md-4 text-center"> 
<a href="testimonial.do" class="btn  btn btn-primary">Add Testimonial</a>
</div> 
    </div>
  </div>

<div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm ">
    
     <div class="modal-content">
      
         <form action="testmonialdelete.do">
        
         <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <input type="hidden" id="delete_testimonial_id"  name="delete_testimonial_id" />
          <h5 class="modal-title">Are you sure to delete ?</h5>
        </div>
       <div class="modal-footer">
         <button type="submit"  class="btn btn-primary">Ok</button> 
         <button type="button" class="btn btn-danger"   data-dismiss="modal">Cancel</button> 
         </div>
       
        </form>
       
      </div>
      </div>
      </div>

</body>
</html>