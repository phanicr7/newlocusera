<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="#" class="tip-bottom">Events</a><a href="#" class="tip-bottom current">Edit an  Event</a> </div>
  
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span10" >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit an Event </h5>
        </div>
        <div class="widget-content nopadding">
          <form:form id="editevent_form" action="updateEvent.do" method="post" class="form-horizontal" commandName="eventCommand" enctype="multipart/form-data">
		
			
            <div class="control-group">
              <label class="control-label">Event Title :</label>
              <div class="controls">
                <form:input type="text" class="span11" placeholder="Event Title" path="title"/>
              </div>
            </div>
			
			 <div class="control-group">
              <label class="control-label">Event Heading  :</label>
              <div class="controls">
                <form:input type="text" class="span11" placeholder="Event Heading" path="heading" />
              </div>
            </div>
			
          			 <form:input type="hidden" path="eventid"/>
           
             <div class="control-group">
              <label class="control-label">Previous Image</label>
              <div class="controls">
                <img src="<c:url value="${eventCommand.oldpath}"/>"/>
              </div>
            </div>
            
			 <div class="control-group">
              <label class="control-label">Upload Logo :</label>
              <div class="controls">
                <form:input type="file" path="logo"/>
              </div>
            </div>	 
          <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Write Event Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
          
            <div class="controls">
              <form:textarea name="artical" class="textarea_editor span10" rows="6" placeholder="Enter text ..." path="eventcontent"></form:textarea>
            </div>
          
        </div>
      </div>
    </div>
      <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </form:form>
        </div>
      </div>
	 </div>
  </div>

</div>
</div>

</body>
</html>