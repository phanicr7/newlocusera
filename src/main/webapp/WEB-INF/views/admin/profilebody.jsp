<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">welcome user</a> <a href="#" class="current">My profile</a> </div>
  <h1>Profile</h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Personal-info</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form action="update.do" method="post" class="form-horizontal" modelAttribute="profileCommand">
          <div class="control-group">
            
            
            </div>
            <div class="control-group">
              <label class="control-label">User Name :</label>
              <div class="controls">
                <form:input type="text" path="name" value="${user.le_name}" class="span11" placeholder="First name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <form:input type="text" path="email" value="${user.le_email}" class="span11" placeholder="Last name"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Phone Number</label>
              <div class="controls">
                <form:input type="text"  path="number" value="${user.le_phone_number}" class="span11" placeholder="Enter Password"  />
              </div>
            </div>
          
            <div class="form-actions">
              <form:button type="submit"   class="btn btn-success modify">Save</form:button>
              <form:button type="button"  class="btn btn-info edit" onclick="edit()">Edit</form:button>
              <form:button type="button"  class="btn btn-danger cancle">Cancel</form:button>
            </div>
          </form:form>
        </div>
      </div>
      
  
    </div>

  </div>

</div>
</div>


</body>
</html>