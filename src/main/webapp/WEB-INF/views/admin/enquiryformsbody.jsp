<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Conatct Us Forms</a> </div>
  </div>
  <div class="container-fluid">
      <h3>Contact Us Forms</h3>
  
    <hr>
    <div class="row-fluid">
      <div class="span12">
         <c:if test="${deleted=='deleted'}" >
     <div class="alert alert-success" id="alertFadeout">
        <strong>Deleted successfully</strong> 
        </div>
        </c:if>
           <c:if test="${error=='error'}">
     <div class="alert alert-danger" id="alertFadeout">
        <strong>Try again</strong> 
        </div>
        </c:if>
          
                  <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                <h5>Contact Us Forms</h5>
            
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Number</th>
                  <th>Location</th>
                  <th>State</th>
				  <th>Action</th>
			   </tr>
              </thead>
              <tbody>
               <c:forEach items="${enquiry}" var="enquiry" varStatus="status">
              <tr class="gradeA">
                  <td>${enquiry.name}</td>
                  <td>${enquiry.email}</td>
                  <td>${enquiry.number}</td>
                  <td class="center">${enquiry.location}</td>
                   <td class="center">${enquiry.state}</td>
                  
	<td class="center"><a href="#myModel${status.index.toString()}" data-toggle="modal" class="btn btn-success btn-mini">View Content</a>&nbsp;&nbsp;
    <a class="btn btn-danger btn-mini deleteClass" href="#deleteModal" enquiry_id="${enquiry.enquiry_id}" data-toggle="modal">Delete</a></td>  
    <div id="myModel${status.index.toString()}" class="modal hide">
          <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">�</button>
                <h3>Enquiry Description</h3>
              </div>
              <div class="modal-body">
                <p>${enquiry.des}</p>
              </div>
            </div>
				   
                </tr>
                </c:forEach>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm ">
    
     <div class="modal-content">
      
         <form action="enquirydelete.do">
        
         <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <input type="hidden" id="delete_enquiry_id"  name="delete_enquiry_id" />
          <h5 class="modal-title">Are you sure to delete ?</h5>
        </div>
       <div class="modal-footer">
         <button type="submit"  class="btn btn-primary">Ok</button> 
         <button type="button" class="btn btn-danger"   data-dismiss="modal">Cancel</button> 
         </div>
       
        </form>
       
      </div>
      </div>
      </div>

</body>
</html>