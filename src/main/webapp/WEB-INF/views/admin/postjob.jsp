<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title> <tiles:insertAttribute name="title" ignore="true"></tiles:insertAttribute></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="admin/css/bootstrap.min.css" />
<link rel="stylesheet" href="admin/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="admin/css/colorpicker.css" />
<link rel="stylesheet" href="admin/css/datepicker.css" />
<link rel="stylesheet" href="admin/css/uniform.css" />
<link rel="stylesheet" href="admin/css/select2.css" />
<link rel="stylesheet" href="admin/css/matrix-style.css" />
<link rel="stylesheet" href="admin/css/matrix-media.css" />
<link rel="stylesheet" href="admin/css/bootstrap-wysihtml5.css" />
<link href="admin/font-awesome/css/font-awesome.css" rel="stylesheet" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<style>
.logo {
   height: 77px;
    width: 220px;
}
.error{
color:red;
border-color: red !important;
}
.valid{
border-color: green!important;
}


</style>
</head>
<body>

<tiles:insertAttribute name="menu"></tiles:insertAttribute>
  <tiles:insertAttribute name="sidemenu"></tiles:insertAttribute>
  <tiles:insertAttribute name="body"></tiles:insertAttribute>
  <tiles:insertAttribute name="footer"></tiles:insertAttribute>     
<!--end-Footer-part--> 
<script src="admin/js/jquery.min.js"></script> 
<script src="admin/js/jquery.ui.custom.js"></script> 
<script src="admin/js/bootstrap.min.js"></script> 
<script src="admin/js/jquery.uniform.js"></script> 
<script src="admin/js/select2.min.js"></script> 
<script src="admin/js/jquery.validate.js"></script> 
<script src="admin/js/matrix.js"></script> 
<script src="admin/js/matrix.form_validation.js"></script>
<script src="admin/js/wysihtml5-0.3.0.js"></script> 
<script src="admin/js/jquery.peity.min.js"></script> 
<script src="admin/js/bootstrap-wysihtml5.js"></script> 
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js"></script> 
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>
$(document).ready(function(){

$('#companyname').change(function(event) {
	
	
	
	 var $c=$("#companyname").val();
	   
	 
	   
	   if($c==0)
		   {
		      
		      var $location = $("#locationname");
		    
		      $location.find('option').remove(); 
		     
		      
		      $('<option></option>').val("0").text("Select Location").appendTo($location);
		      
		      
		      
		      
		   
		   }
	   else
		   {
		      
			
			$.ajax({
			    
			    url: "getcompanylocations.do",
			    type: 'GET',
			    dataType: 'json',
			    data: {id: $c },
			    contentType: 'application/json',
			    
			    success : function(response) {
			    	 
			    	
			    	  var $select = $("#locationname");                           
				         
					  $select.find('option').remove();  
					  var anc= $('#s2id_locationname').find('a');
					  anc.find('span').text('');
					  
					  $('<option></option>').val("0").text("Select Location").appendTo($select);
			        
			         $.each(response, function(key, value) {  
			        	   
			        	
							
			             $('<option></option>').val(key).text(value).appendTo($select);      
			              });
			    	
			    	
			    	
			    	
		           
			    },
			    error : function(error) {
			    	alert("some internal problem pls try again");
			    },
			
			});
			     
	     
		       
		       
		   
		   }
	
	

});




$('#category').change(function(event) {
	
	
	
	 var $c=$("#category").val();
	   
	 
	   
	   if($c==0)
		   {
		      
		      var $location = $("#subcategory");
		    
		      $location.find('option').remove(); 
		     
		      
		      $('<option></option>').val("0").text("Select Functional Area").appendTo($location);
		      
		      
		      
		      
		   
		   }
	   else
		   {
		      
			
			$.ajax({
			    
			    url: "getjobtype.do",
			    type: 'GET',
			    dataType: 'json',
			    data: {id: $c },
			    contentType: 'application/json',
			    
			    success : function(response) {
			    	 
			    	
			    	  var $select = $("#subcategory");                           
				         
					  $select.find('option').remove();  
					 
					  
					  $('<option></option>').val("0").text("Select Functional Area").appendTo($select);
			        
			         $.each(response, function(key, value) {  
			        	   
			        	
			             $('<option></option>').val(key).text(value).appendTo($select);      
			              });
			    	
			    	
			    	
			    	
		           
			    },
			    error : function(error) {
			    	alert("some internal problem pls try again");
			    },
			
			});
			   }
	
	

});

});

	


	
</script>
<script>
$(document).ready(function(){
	$('#job_form').validate({ 
        rules: {
        	jobtitle: {
                required: true   
             },
        	openings: {
              required: true,
               number:true
 
           },
           experience: {
               required: true
  
            },
        	qualification: {
                required: true
           
                
           },
           skills: {
                required: true
                

            }
        },
        messages:{
        	
        	jobtitle: 'Title is required',
        	openings: 'Openings is required',
        	skills: 'Skills is required',
        	experience:'Experience is required',
        	qualification: 'Qualification is required.'
        }
	
        
     }); 

	
	$("#job_form").submit(function(e){
		
		var company = $("#companyname").val(); 
		var catogory=$("#category").val();
        var loc     =$("#locationname").val();
       
        if(company=="" || company==null || company ==0)
        { 
       	 alert("Please Select Company");  
       	 e.preventDefault();
       	 return false;
           }
        if(loc=="" || loc==null || loc==0)
	     { 
	    	 alert("Please Select Location");  
	    	 e.preventDefault();
	    	 return false;
	        }
       
         if(catogory=="" || catogory==null || catogory==0)
	     { 
	    	 alert("Please Select Indusrty");  
	    	 e.preventDefault();
	    	 return false;
	 
	     }
	    

	     
	});
});
</script>


	  <script language="Javascript">
$(document).ready(function(){
    $('#job_form').submit(function() {
        var error = 0;
        var subcategory = $('#subcategory').val();

        if (subcategory == '0') {
            error = 1;
            alert(' Select  Functional Area');
        }

        if (error) {
            return false;
        } else {
            return true;
        }

    });
});
        
</script>             

</body>
</html>
