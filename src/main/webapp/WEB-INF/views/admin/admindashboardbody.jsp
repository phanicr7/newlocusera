<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="admindashboard.do" title="Go to Home" class="tip-bottom current"><i class="icon-home "></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb"> <a href="admindashboard.do"> <i class="icon-dashboard"></i>  My Dashboard </a> </li>
      
        <li class="bg_ly"> <a href="users.do"> <i class="icon-inbox"></i><span class="label label-success">${list}</span> users </a> </li>
        <li class="bg_lo"> <a href="listofjobs.do"> <i class="icon-th"></i> jobs</a> </li>
      
        <li class="bg_ls"> <a href="articallist.do"> <i class="icon-tint"></i> Blog</a> </li>
        <li class="bg_lb"> <a href="newslist.do"> <i class="icon-pencil"></i>News</a> </li>
        <li class="bg_lg"> <a href="eventslist.do"> <i class="icon-calendar"></i>Events</a> </li>
         <li class="bg_lo"> <a href="addstate.do"> <i class="icon-table"></i>Location Management</a> </li>
        <li class="bg_lg"> <a href="addindustry.do"> <i class=" icon-tags"></i>Industry Management</a> </li>
        <li class="bg_lg"> <a href="resume.do"> <i class="icon-inbox"></i>Resume Management</a> </li>
         <li class="bg_ls"> <a href="viewCompany.do"> <i class="icon-pencil"></i>Company Management</a> </li>
         <li class="bg_ls"> <a href="enquiryforms.do"> <i class="icon-envelope"></i>Contact Us Form</a> </li>
         

      </ul>
    </div>
<!--End-Action boxes-->    


   
  </div>
</div>

<!--end-main-container-part-->

</body>
</html>