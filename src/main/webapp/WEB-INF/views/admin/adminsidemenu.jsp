<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<!--sidebar-menu-->
	<div id="sidebar">
		<a href="#" class="visible-phone"><i class="icon icon-home"></i>
			Dashboard</a>
		<ul>
			<li class="active"><a href="admindashboard.do"><i
					class="icon icon-home"></i> <span>Dashboard</span></a></li>



			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Jobs</span> <span class="label label-important">2</span></a>
				<ul>
					<li><a href="postjob.do">Post a Job</a></li>
					<li><a href="listofjobs.do"> View Posted Jobs</a></li>

				</ul></li>
			<li><a href="users.do"><i class="icon icon-home"></i> <span>Users</span></a></li>
			
			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Blog</span> <span class="label label-important">2</span></a>
				<ul>
					<li><a href="addartical.do">Post an Article</a></li>
					<li><a href="articallist.do">View Posted Articles</a></li>

				</ul></li>

			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>News</span> <span class="label label-important">2</span></a>
				<ul>
					<li><a href="addnews.do">Post News</a></li>
					<li><a href="newslist.do">View Posted News</a></li>

				</ul></li>


			<!-- <li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Events</span> <span class="label label-important">2</span></a>
				<ul>
					<li><a href="addevents.do">Post an Event</a></li>
					<li><a href="eventslist.do">View Events</a></li>

				</ul></li> -->

			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span> Testimonials</span> <span
					class="label label-important">2</span></a>
				<ul>
					<li><a href="testimonial.do">Add Testimonial</a></li>
					<li><a href="testimoniallist.do">View Testimonials</a></li>

				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Client Management </span> <span
					class="label label-important">2</span></a>
				<ul>
				              <li><a href="addClient.do">Add Client</a></li>
				              <li><a href="viewlogos.do">View Clients</a></li>

				</ul></li>


			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Location Management</span> <span
					class="label label-important">3</span></a>
				<ul>
					<li><a href="viewState.do">View State</a></li>
					<li><a href="viewCity.do">View City</a></li>
					<li><a href="viewLocation.do">View Location</a></li>

				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Industry Management</span> <span
					class="label label-important">2</span></a>
				<ul>
					<li><a href="viewIndustry.do">View Industries</a></li>
					<li><a href="viewSubIndustry.do">View Functional Area</a></li>


				</ul></li>



			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Resume Management</span> <span class="label label-important">2</span></a>
				<ul>
					<li><a href="resume.do">View Resumes</a></li>
					<li><a href="searchresume.do">Filter Resumes</a></li>
					<li><a href="searchinglist.do">Search Resumes</a></li>

				</ul></li>

			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>Company Management</span> <span class="label label-important">2</span></a>
				<ul>
									<li><a href="companylocation.do">Add Company Location</a></li>
									<li><a href="viewCompany.do">View  Companies</a></li></ul></li>
			<li><a href="email.do"><i class="icon icon-home"></i> <span>EMAIL
						Management</span></a></li>
			<li><a href="sms.do"><i class="icon icon-home"></i> <span>SMS
						Management</span></a></li>
						<li><a href="enquiryforms.do"><i class="icon icon-home"></i>
					<span>Contact Us Forms</span></a></li>
			






		</ul>
	</div>
	<!--sidebar-menu-->

</body>
</html>