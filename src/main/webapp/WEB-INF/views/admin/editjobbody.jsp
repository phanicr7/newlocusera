<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--close-left-menu-stats-sidebar-->

<div id="content">
<div id="content-header">
  <div id="breadcrumb">
   <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
    <a href="#" class="tip-bottom">Jobs</a><a href="#" class="tip-bottom current">Edit Job</a> </div>
  
</div>

<div class="container-fluid">
          <h3>Edit Job</h3>

  <hr>
  <div class="row-fluid">
    <div class="span7" >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Job</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form id="editjob_form" action="updateJob.do" method="post" class="form-horizontal" commandName="jobCommand" enctype="multipart/form-data" >

          <div class="control-group">
              <label class="control-label">Job Title :</label>
              <div class="controls">
              <form:input path="jobtitle"  class="span11 field" placeholder="Job Title" />
                
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Company Name :</label>

              <div class="controls">
             <form:select path="companyname" name="companyname" id="companyname">
             <c:forEach items="${company}" var="company">
            <form:option value="${company.company_id}">${company.company_name}</form:option>
             
                </c:forEach>
                </form:select> 
              </div>
            </div>
			 <div class="control-group">

              <label class="control-label">Location :</label>

              <div class="controls">
                <form:select path="locationname" class="field">
                  <c:forEach items="${locations}" var="loca">
            <form:option value="${loca.getKey()}">${loca.getValue()}</form:option>
             
                </c:forEach>
                </form:select> -
              </div>
            </div>
		
            <div class="control-group">
              <label class="control-label">No of Openings :</label>
              <div class="controls">
              <form:input path="openings"  id ="openings" class="span11 field" placeholder="No of Openings" />
                
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Experience :</label>
              <div class="controls">
              <form:input path="experience"  id ="expr" value="" class="span11 field" placeholder="Experience" />
                <p style="color: red">Example : Fresher,1-2 Years,2-3 Years.,etc</p>
              </div>
              
            </div>
			   <div class="control-group">
              <label class="control-label">Industry :</label>
              <div class="controls">
              <form:select  path="catogory" id="category">
              <c:forEach items="${industry}" var="category">
   
             <form:option value="${category.industry_id}">${category.industryname}</form:option>
           
              </c:forEach>
             </form:select>
             
              </div>
            </div>
            
               <div class="control-group">
              <label class="control-label">Functional Area:</label>
              <div class="controls">
                <form:select  path="subcatogory" id="subcategory">
                              <c:forEach items="${jobtype}" var="jobtype">
   
             <form:option value="${jobtype.getKey()}">${jobtype.getValue()}</form:option>
           
              </c:forEach>
                 </form:select>
              </div>
            </div>
         
                    			 <form:input type="hidden" path="jobid"/>
          
            <div class="control-group">
              <label class="control-label">Qualification Details :</label>
              <div class="controls">
                <form:textarea path="qualification"  id="qualification" class="span11 field" ></form:textarea>
              </div>
            </div>
			<div class="control-group">
              <label class="control-label">Skills Required :</label>
              <div class="controls">
                <form:textarea path="skills"  id="skills" class="span11 field" ></form:textarea>
              </div>
            </div>
                        
                       <div class="control-group">
              <label class="control-label">Job Posting Status :</label>
              <div class="controls">
            <form:select path="job_status"  id ="job_status" value="" class="span11 field">
               <form:option value="Active">Active</form:option>
               <form:option value="In-Active">In Active</form:option>
               </form:select> 
              </div>
            </div>
            
       <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Update</form:button>
             <a href="listofjobs.do"><button type="button" class="btn btn-danger">Cancel </button></a>
              
            </div>
          </form:form>
        </div>
      </div>
</div>
 </div>

</div>
</div>

</body>
</html>
