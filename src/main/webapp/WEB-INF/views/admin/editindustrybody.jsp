<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--close-left-menu-stats-sidebar-->

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Industry Management</a><a href="#" class="tip-bottom current">Edit Industry</a> </div>
  
</div>
<c:if test="${alert!=null}">
<div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>${alert}</strong>
    
</div>
   
</c:if>
<div class="container-fluid">
<h3>Edit Industry</h3>
  <hr>
  <div class="row-fluid">
    <div class="span7" >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Industry</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form  id="editindustry_form" action="updateindustry.do" method="post" class="form-horizontal" modelAttribute="industryCommand">
            <div class="control-group">
              <label class="control-label">Industry:</label>
                      <form:input path="id" type="hidden"/>
              
              <div class="controls">
                <form:input path="name" type="text" class="span11 field" placeholder="Enter Industry Name" />
              </div>
            </div>
			
            <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Update</form:button>
              <a href="viewIndustry.do"><button type="button" class="btn btn-danger">Cancel</button></a>
            </div>
          </form:form>
        </div>
      </div>
  </div></div>
</div></div>

</body>
</html>