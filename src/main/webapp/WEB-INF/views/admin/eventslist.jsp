<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title> <tiles:insertAttribute name="title" ignore="true"></tiles:insertAttribute></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="admin/css/bootstrap.min.css" />
<link rel="stylesheet" href="admin/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="admin/css/uniform.css" />
<link rel="stylesheet" href="admin/css/select2.css" />
<link rel="stylesheet" href="admin/css/matrix-style.css" />
<link rel="stylesheet" href="admin/css/matrix-media.css" />
<link href="admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<style>
.logo {
   height: 77px;
    width: 220px;
}


</style>
</head>
<body>

<tiles:insertAttribute name="menu"></tiles:insertAttribute>
  <tiles:insertAttribute name="sidemenu"></tiles:insertAttribute>
  <tiles:insertAttribute name="body"></tiles:insertAttribute>
  <tiles:insertAttribute name="footer"></tiles:insertAttribute>     
<script src="admin/js/jquery.min.js"></script> 
<script src="admin/js/jquery.ui.custom.js"></script> 
<script src="admin/js/bootstrap.min.js"></script> 
<script src="admin/js/jquery.uniform.js"></script> 
<script src="admin/js/select2.min.js"></script> 
<script src="admin/js/jquery.dataTables.min.js"></script> 
<script src="admin/js/matrix.js"></script> 
<script src="admin/js/matrix.tables.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	$('.deleteClass').on('click', function(){
        var id=$(this).attr('event_id');
           $("#delete_event_id").val(id);
      });
	
});


</script>
</body>
</html>
