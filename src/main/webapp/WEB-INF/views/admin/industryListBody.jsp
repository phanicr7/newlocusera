<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" >Industry Management</a> 
    <a href="#" class="current">View Industries</a></div>
        <c:if test="${deleted=='deleted'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong> Deleted successfully </strong> 
        </div>
        </c:if>
         <c:if test="${save=='save'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong> Category saved successfully </strong> 
        </div>
        </c:if>
           <c:if test="${error=='error'}">
     <div class="alert alert-danger" id="alertFadeout">
        <strong>Try again</strong> 
        </div>
        </c:if>
  </div>
  <c:if test="${updated=='updated'}">
     <div class="alert alert-success" id="alertFadeout">
        <strong> Updated successfully</strong> 
        </div>
        </c:if>
  <div class="container-fluid">
      <h3>View Industries</h3>
  
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
  
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                 <h5>View Industries</h5>
           

          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                <th>Category</th>

				  <th>Action</th>
				  
                </tr>
              </thead>
              <tbody>
              
               <c:forEach items="${industryList}" var="industry" >
              <tr class="gradeA" >
              <td style="text-align: center;">${industry.industryname}</td>

				 <td class="center" style="text-align: center;"><a href="editindustry.do?industryId=${industry.industry_id}" class="btn btn-success btn-mini">Edit</a>&nbsp;&nbsp;

				 <a class="btn btn-danger btn-mini deleteClass" href="#deleteModal" industry_id="${industry.industry_id}"  data-toggle="modal">Delete</a></td>  
 				 
                </tr>
                </c:forEach>
                
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-4 text-center"> 
<a href="addindustry.do" class="btn  btn btn-primary">Add Industry</a>
</div> 
      </div>
      </div>
      </div>
   
   <div class="modal fade" id="deleteModal">
        <div class="modal-dialog modal-sm ">
    
     <div class="modal-content">
      
         <form action="deleteindustry.do">
        
         <div class="modal-header">
                         <button data-dismiss="modal" class="close" type="button">�</button>
           <input type="hidden" id="delete_industry_id"  name="delete_industry_id" />
          <h5 class="modal-title">Are you sure to delete ?</h5>
        </div>
       <div class="modal-footer">
         <button type="submit"  class="btn btn-primary">Ok</button> 
       <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
     </form>
       
      </div>
      </div>
      </div>
</body>
</html>
