<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!--Header-part-->
<div id="header">
  <img src="admin/img/admin.jpg" class="logo" class="img-responsive" alt="Logo"/>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
        <div id="user-nav" class="navbar navbar-inverse">
       <ul class="nav">
       <li  class="dropdown" id="profile-messages" >
       <a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle">
       <i class="icon icon-user"></i>  <span class="text">Welcome User</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="profile.do"><i class="icon-user"></i> My Profile</a></li>
        <li class="divider"></li>
        <li><a href="change.do"><i class="icon-check"></i>Change Password</a></li>
       
        
      </ul>
    </li>
   
   
    <li class=""><a title="" href="logout.do"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->



</body>
</html>