<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Users</a> </div>
  </div>
  <div class="container-fluid">
          <h3>View Users</h3>
  
    <hr>
    <div class="row-fluid">
      <div class="span12">
  
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Registered Users</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Phone Number</th>
                  <th>UserName</th>
                  <th>Email</th>
				  <th>Action</th>
				  
                </tr>
              </thead>
              <tbody>
               <c:forEach items="${list}" var="user" varStatus="status">
              <tr class="gradeA">
                  <td>${status.index+1}</td>
                  <td>${user.le_user_phone}</td>
                  <td>${user.le_username}</td>
                  <td class="center">${user.le_user_email}</td>
				 <td class="center"><a href="#myModel${status.index.toString()}" data-toggle="modal" class="btn btn-info btn-mini">View Content</a>&nbsp;&nbsp;</td>
				 
				 <div id="myModel${status.index.toString()}" class="modal hide">
              <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">�</button>
                <h3>User Information</h3>
              </div>
              <div class="modal-body">
                <p>Username      :${user.le_username}</p>
                <p>Firstname     :${user.le_user_firstname}</p>
                <p>LastName      :${user.le_user_lastname}</p>
                <p>Email         :${user.le_user_email} </p>
                <p>PhoneNumber   :${user.le_user_phone}</p>
               
              </div>
            </div>
                </tr>
                </c:forEach>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>