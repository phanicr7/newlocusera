<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Email Management</a> <a href="#" class="current">Send an Email</a> </div>
</div>
<div class="container-fluid">
  <h3>Send an Email to Candidate(s)</h3>
  <hr>
  <div class="row-fluid">
  <c:if test="${sent=='sent'}">
     <div class="alert alert-success">
        <strong>  Deleted Successfully</strong> 
        </div>
        </c:if>
           <c:if test="${notsent=='notsent'}">
     <div class="alert alert-danger">
        <strong>Try again</strong> 
        </div>
        </c:if>
    <div class="span7">
    <div class="widget-box hidesubcategory ">
       <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Send an Email </h5>
        </div>
        <div class="widget-content nopadding">
         <form  id="EmailForm" action="emailsending.do" method="post" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Industry</label>
              <div class="controls">
                <select  name="catogory" id="catogory">
                   <c:forEach items="${industry}" var="category" varStatus="status">
                                <c:choose>
                                         <c:when test="${status.index==0}"> <option value="0">Select Industry</option><option value="${category.industry_id}">${category.industryname}</option></c:when>
                                         <c:otherwise><option value="${category.industry_id}">${category.industryname}</option></c:otherwise>
                                  </c:choose>
                
                 
                </c:forEach>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Functional Area</label>
              <div class="controls">
                <select   name="subcatogory" id="subcatogory">
                 
                </select>
              </div>
            </div>
           
      <div class="widget-content">
        <div class="control-group">
         <label class="control-label">Message</label>
       <div class="controls">
                <textarea class="span11" name="data" id="message"></textarea>
            </div>
          
        </div>
      </div>
      
           <div class="form-actions">
              <button type="submit" class="btn btn-success">Send Email </button>
                        <a href="email.do"><button type="button" class="btn btn-danger">Cancel</button></a>
              
            </div>
    </form>
        </div>
      </div>
    </div>
   
  </div>

</div></div>
</body>
</html>