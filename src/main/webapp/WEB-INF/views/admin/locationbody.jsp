<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>

<!--close-left-menu-stats-sidebar-->

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Location Management</a><a href="#" class="tip-bottom current">Add Location</a> </div>
  
</div>

<div class="container-fluid">
              <h3>Add Location</h3>

  <hr>
  <div class="row-fluid">
    <div class="span7" >
    
       <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5> Add Location </h5>
        </div>
        <div class="widget-content nopadding">
          <form:form  id="location_form" action="savelocation.do" method="post" class="form-horizontal" modelAttribute="locationCommand" >
           
			 <div class="control-group">
              <label  for="myslect" class="control-label">State</label>
              <div class="controls">
                <form:select path="state" id="state">
                  <c:forEach items="${states}" var="state" varStatus="status">
                <c:choose>
                                         <c:when test="${status.index==0}"> <option value="0">Select State</option><option value="${state.state_id}">${state.statename}</c:when>
                                         <c:otherwise><option value="${state.state_id}">${state.statename}</option></c:otherwise>
              </c:choose>
                </c:forEach>
                </form:select>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label"> City</label>
              <div class="controls">
                <form:select path="city" id="city" class="field">
                 
                </form:select>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Location Name :</label>
              <div class="controls">
                <form:input path="location" id ="location"  type="text" class="span11 field" placeholder="Enter Location" />
              </div>
            </div>
           
            <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Save</form:button>
              <a href="addlocation.do"><button type="button" class="btn btn-danger">Cancel</button></a>
            </div>
          </form:form>
        </div>
      </div>
  </div>

  </div>

</div></div>


</body>
</html>