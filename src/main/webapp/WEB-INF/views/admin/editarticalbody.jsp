<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
   <a href="#" class="tip-bottom">Blog</a><a href="#" class="tip-bottom current">Edit  Article</a> </div>
  
</div>
<div class="container-fluid">
          <h3>Edit Article </h3>

  <hr>
  <div class="row-fluid">
    <div class="span10" >
      <div class="widget-box">
        <div class="widget-title"> 
        <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Article </h5>
        </div>
        <div class="widget-content nopadding">
        
          <form:form id="editart_form" action="updateArtical.do" method="post" class="form-horizontal" commandName="articalCommand" enctype="multipart/form-data">
           
          
            <div class="control-group">
              <label class="control-label">Article Writer Name :</label>
              <div class="controls">
                <form:input type="text" class="span11" placeholder="Writer Name" path="writername"/>
              </div>
            </div>
		
			
            <div class="control-group">
              <label class="control-label">Article Title :</label>
              <div class="controls">
                <form:input type="text" class="span11" placeholder="Artical Title" path="title" />
              </div>
            </div>
			
			 <div class="control-group">
              <label class="control-label">Article Heading  :</label>
              <div class="controls">
               <form:input type="text" class="span11" placeholder="Artical Heading" path="heading"/>
              </div>
            </div>
			 <form:input type="hidden" path="blogid"/>
           
            <div class="control-group">
              <label class="control-label">Article Writer Info :</label>
              <div class="controls">
                <form:textarea class="span11"  path="info"></form:textarea>
              </div>
            </div>
          
            <div class="control-group">
              <label class="control-label">Previous Image</label>
              <div class="controls">
                <img src="<c:url value="${articalCommand.oldpath}"/>"/>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Upload Article Logo :</label>
              <div class="controls">
                <form:input type="file" path="logo" />
              </div>
            </div>
			
			
			
			 
          <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Write Article Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
          
            <div class="controls">
              <form:textarea name="artical" class="textarea_editor span10" rows="6" placeholder="Enter text .." path="art_content"></form:textarea>
            </div>
          
        </div>
      </div>
    </div>
     
  <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
               <a href="articallist.do"><button type="button" class="btn btn-danger">Cancel </button></a>
              
            </div>
          </form:form>
        </div>
      </div>
	  
	  
	  
	  
  </div>
	
	
	
  </div>

</div></div>
</body>
</html>