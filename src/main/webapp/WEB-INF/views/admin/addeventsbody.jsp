<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Events</a><a href="#" class="tip-bottom current">Post an Event</a> </div>
  
</div>

<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span10" >
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Post an Event</h5>
        </div>
        <div class="widget-content nopadding">
          <form:form id="event_form" action="saveevent.do" method="post" class="form-horizontal" modelAttribute="eventCommand" enctype="multipart/form-data">
          
		
			
            <div class="control-group">
              <label class="control-label">Event Title :</label>
              <div class="controls">
                <form:input type="text" path="title" class="span11 field" placeholder="Title" />
              </div>
            </div>
			
			 <div class="control-group">
              <label class="control-label">Event Heading :</label>
              <div class="controls">
                <form:input type="text" path="heading" class="span11 field" placeholder="Heading" />
              </div>
            </div>
		
			 <div class="control-group">
              <label class="control-label">Upload Logo :</label>
              <div class="controls">
                <form:input type="file" path="logo" class="field"/>
              </div>
            </div>
          <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Write Event Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
          
            <div class="controls">
              <form:textarea name="content" class="textarea_editor span10 field" rows="6" placeholder="Enter text ..." path="eventcontent"></form:textarea>
            </div>
        </div>
      </div>
    </div>
     <div class="form-actions">
              <form:button type="submit" class="btn btn-success">Save</form:button>
            </div>
          </form:form>
        </div>
      </div>
</div>
  </div>
</div></div>
</body>
</html>