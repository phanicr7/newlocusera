<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Locusera solution pvt ltd,Service,Recruitment / Talent Acquisition, Out Sourcing & Permanent Staffing,Typography,Payroll management,Compliance Management,HR & Legal Audits,Patents, IPR, Trademark & Copyrights filling" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<!--// css -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
     <link rel="stylesheet" href="css/bootstrapValidator.css"/>
<link rel="shortcut icon" href="images/favicon.png">
<!-- //font-awesome icons -->
<!-- font -->
<!-- <link href="//fonts.googleapis.com/css?family=Playball&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'> -->

<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
 <script src='js/bootstrapValidator.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->

<style>


input, section {
  clear: both;
  padding-top: 10px;
  display: none;
}

label {
 
  cursor: pointer;
  text-decoration: none;
  text-align: center;
  background: #f0f0f0;
  color: black;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4,
#tab5:checked ~ #content5 {
  display: block;
  padding: 20px;
  background: #fff;
  color: #999;
  
}

.tab_container .tab-content p,
.tab_container .tab-content h3 {
  -webkit-animation: fadeInScale 0.7s ease-in-out;
  -moz-animation: fadeInScale 0.7s ease-in-out;
  animation: fadeInScale 0.7s ease-in-out;
}
.tab_container .tab-content h3  {
  text-align: center;
}

.tab_container [id^="tab"]:checked + label {
  background: #fff;
  box-shadow: inset 0 3px #0CE;
}

.tab_container [id^="tab"]:checked + label .fa {
  color: #0CE;
}
.modal-header
{
    background-color: #b3bf06;
    font-size: 2em;
    text-align: center;
    color: #212121;
    font-weight: 600;
    text-transform: uppercase;
}
/* .small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
} */
</style>
</head>
<body>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
			$().UItoTop({ easingType: 'easeOutQuart' });
			$('#submit_Login')
			.on(
					'click',
					function(e) {

						var user_id = $("#username").val();
						var password = $("#password").val();

						/*<![CDATA[*/

						if (user_id.toString() != ""
								&& password.toString() != "") {
							$('#login_regsiterGif').show();
							$
									.ajax({
										type : "POST",
										url : "userlogin.do",
										data : {
											"userid" : user_id,
											"pws" : password
										},
										success : function(
												responseData) {

											for ( var key in responseData) {

												if (responseData[key] == false) {
													$(
															'#invalid_login')
															.show();
													$(
															'#login_regsiterGif')
															.css(
																	'display',
																	'none');
					

													return false;
												} else {

													$(
															'#login_regsiterGif')
															.show();

													$(
															'#login_form')
															.trigger(
																	'submit');
												}

											}

										}
									});
						}

						return false;
						/*]]>*/

					});
			 $('#register_form').bootstrapValidator({
			        feedbackIcons: {
			            valid: 'glyphicon glyphicon-ok',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			            firstname: {
			                validators: {
			                    notEmpty: {
			                        message: 'First Name is required'
			                    },
			                    regexp : {
									regexp : /^[a-zA-Z\s]+$/,
									message : 'Sorry !! Only alphabets are allowed.'
								}
			                }
			            },
			            lastname: {
			                validators: {
			                    notEmpty: {
			                        message: 'Last Name is required'
			                    },
			                    regexp : {
									regexp : /^[a-zA-Z\s]+$/,
									message : 'Sorry !! Only alphabets are allowed.'
								}
			                }
			            },
			            username: {
			                validators: {
			                    notEmpty: {
			                        message: 'Username is required'
			                    },
			                 
								remote: {
		                               message: 'Username already existed.',
		                               url: 'check_username.do',
		                               delay: 2000,
		                               type: 'POST'
		                           }
								

			                }
			            },
			            email : {
						
							validators : {

								notEmpty : {
									message : 'Email is required'
								},
								regexp : {
									regexp : /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
									message : 'Invalid Email'
								},
								remote: {
		                               message: 'Email is already existed.',
		                               url: 'check_useremail.do',
		                               delay: 2000,
		                               type: 'POST'
		                           }

							}
						},
			                 
			                 password : {

									validators : {
										notEmpty : {
											message : 'Password is required.'
										},

										identical : {
											field : 'confirmPassword',
											message : 'Password and its Confirm Password are not the same.'
										},
						                   stringLength: {
						                	   min:6,
						                             max:8,
						                             message: 'Please enter a valid password of minimum 6 and 8 characters'
						                         } 
				                       
									}
								},
								confirmPassword : {

									validators : {

										notEmpty : {
											message : 'Confirm Password is required.'
										},
										identical : {
											field : 'password',
											message : 'Password and its Confirm Password are not the same.'
										}
									}
								},

			                 
			         mobile: {
			        	 validators: {
			        		 notEmpty: {
		                           message: 'Mobile number is required.'
		                       },

		                       

		                       regexp: {
		                           regexp:/^[789]\d{9}$/,
		                           message: 'Please enter a valid 10 digit mobile number'
		                       },/* ,
			                    
			                   stringLength: {
			                	   min:10,
			                             max: 10,
			                             message: 'The mobile  numbers must be 10 numbers'
			                         }, */
			                         
			                         remote: {
			                               message: 'Mobile number already existed',
			                               url: 'check_userphone.do',
			                               delay: 2000,
			                               type: 'POST'
			                           }
			                        
			                      }
			                
			                  }
			                 }
			 });
								
			});
	</script>
<div class="modal fade" id="myLogin" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-header"  style="background-color: #b3bf06;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login and Register</h4>
        </div>
        <div class="modal-body">
		<div class="tab_container">
			<input id="tab1" type="radio" name="tabs" checked>
			<label for="tab1" style="padding: 30px 35px";><i class="fa fa-sign-in"></i><span>Login</span></label>

			<input id="tab2" type="radio" name="tabs">
			<label for="tab2" style="padding: 30px 35px";><i class="fa fa-pencil-square-o"></i><span>Register</span></label>

			
			<section id="content1" class="tab-content">

		     <div class="alert alert-danger" style="display: none;" id="invalid_login">
        <center><strong style ="color:#FF4500"> <b>Invalid Credentials</b></strong> </center>
        </div>
 
    <form:form id="login_form" action="logging.do" class="form-horizontal" role="form" commandName="loginCommand">
  <div class = "col-md-12">
 <div class="form-group">
	<div class="form-group input-group">
	<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>

     <form:input type="text" class="form-control"  path="username" value="" placeholder="Username"   style="text-transform: capitalize;"  required="required"/>  
    </div>
    </div>
    <div class="form-group">
    <div class="form-group input-group">
		<span class="input-group-addon"><i class="fa fa-lock">&nbsp;</i></span>
     <form:input type="password" class="form-control" path="password" placeholder="Password" required="required" />
    </div>
    </div>
	<button type="submit" class="btn btn-primary" id="submit_Login" style="background-color:#b3bf06;border-color: #b3bf06">Login</button>
	<a href="LEforgotPass.do">Forgot password ?</a>
	
	<img id="login_regsiterGif"src="images/register-load.gif" style="display: none;" />
	</div>
	
</form:form>
</section>

			<section id="content2" class="tab-content">
 <form:form id="register_form" action="saveUser.do"  role="form" modelAttribute="registerUser">
   <div class = "col-md-12" style="color: black;">
    <div class="form-group">
	<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>
       <form:input type="text" class="form-control"  path="firstname" value="" placeholder="First Name"  style="text-transform: capitalize;" required="required"/>
    </div>
    </div>
    <div class="form-group">
    <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>
     <form:input  type="text" class="form-control" path="lastname" value="" placeholder="Last Name" style="text-transform: capitalize;" required="required"/>                                     
     </div> 
    </div>
	 <div class="form-group">
          <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>                                
      <form:input type="text" class="form-control" path="username" value="" placeholder="Username"  style="text-transform: capitalize;" required="required"/>      
    </div>
    </div>
	  <div class="form-group">
	<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
       <form:input type="email" class="form-control" path="email" placeholder="Email" required="required"/>
    </div>
    </div>
    <div class="form-group">
    <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-lock">&nbsp;</i></span>
        <form:input  type="password" class="form-control" path="password" placeholder="Password" required="required"/>
    </div>
    </div>
	
	  <div class="form-group">
	<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-lock">&nbsp;</i></span>
      <input id="confirmpassowrd" type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password" required="required"/>
    </div>
    </div>
    <div class="form-group">
    <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-mobile fa-lg">&nbsp;</i></span>
     <input  type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required="required"/>
    </div>
    </div>
	<button type="submit" class="btn btn-primary" style="background-color:#b3bf06;border-color: #b3bf06">Submit</button>
	</div>
</form:form>
		      
			</section>

		
		</div>
        </div>
        <div class="modal-footer" style="padding: 15px;border-top:none;">
        
        </div>
      </div>
      
    </div>
  </div>

	
	<tiles:insertAttribute name="body"></tiles:insertAttribute>
	
	<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="agile-footer-grids">
				<div class = "col-md-1"></div>
				<div class="col-md-3 w3-agile-footer-grid">
					<h3>Quick Links</h3>
					<ul>
					<a href = "LEhome.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>HOME</li></a>
					<a href = "LEabout.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>ABOUT</li></a>
					<a href = "LEservices.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>SERVICES</li></a>
					<a href = "LEjobs.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>JOB SEEKER</li></a>
					<a href = "LEblog.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>OUR BLOG</li></a>
					<a href = "LEcontact.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>CONTACT US</li></a>
					</ul>
				</div>
				
					<h3 style="color: white;font-family: 'Open Sans', sans-serif;">Locusera Solutions Pvt. Ltd</h3>	
				<div class="col-md-4 w3-agile-footer-grid">
			
							
		
			<h5 style="color: white;"><u style="font-family: 'Open Sans', sans-serif;">Corporate Office Address:</u></h5>
				 <h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">Road No:13,Snehapuri Colony, Nagole , Hyderabad </h5>
				<h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">Telephone Number:<span>040-2414 5626. </span></h5>
				</div>
				<div class="col-md-4 w3-agile-footer-grid">
	
					
			
			<h5 style="color: white;"><u style="font-family: 'Open Sans', sans-serif;">Branch Office Address:</u></h5>
				 <h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">#Door No:8-18-18, Indira Nagar, Vizianagaram </h5>
				<h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">Telephone Number:<span> 08922-272715, 272716.</span></h5>
				</div>
	
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

		<div class="copyright">
			<div class="container">
				<p style = "font-family: 'Open Sans', sans-serif;">� 2017  All Rights Reserved | Design by <a href="http://www.jagahunt.com/" style="color: white;font-family: 'Open Sans', sans-serif;"> Planet-E Software Solutions Pvt Ltd</a> </p>
			</div>
		</div>
	<!-- //footer -->

	<!-- //here ends scrolling icon -->
</body>	
</html>
