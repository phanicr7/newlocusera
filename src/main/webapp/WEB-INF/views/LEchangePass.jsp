      <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<style>
body{
font-family: 'Open Sans', sans-serif !important;
}

.form-control-feedback {

    text-align: start;

}
.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}
</style>
<div class="header">
				
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>
			
						<div class="header-bottom">
	
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
															<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black"  role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
							
											
										</ul>
									</li>	
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
              <c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do" style="font-size: 14px;color:black;"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" style="font-size: 14px;color:black;"><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
											
								</ul>	
								
		</div>	

							
						</nav>		
				</div>
						
			
				
			
		</div>
				
			</div>
		</div>
		<!-- //banner -->

      <div class="container" style="margin-top: 2%;">
      <div class="col-md-4 col-md-offset-4">
          <c:if test="${changed=='changed'}">
        <div class="alert alert-success">
        <strong>Password Updated</strong> 
        </div>
        </c:if>
              
        <div class="panel panel-primary">
        <form:form id="change_password" action="pwdsave.do"  method="post" role="form" modelAttribute="changePwdCommand">
       
            <h1 class="panel-heading" color:black>ChangePassword</h1>
            <div class="panel-body">
                 <div class="row">
                    <div class="form-group col-xs-12">
                        <div class="input-group">
                        <span class="input-group-btn">
                        <label class="btn btn-primary"><span class="glyphicon glyphicon-lock" ></span></label>
                        </span>
                        <form:input id="password"  path="cpwd" class="form-control" type="password" placeholder="Current Password" required="required"/>
                         </div>
                         </div>
                         </div>
                 <div class="row">
                    <div class="form-group col-xs-12">
                        <div class="input-group">
                        <span class="input-group-btn">
                        <label class="btn btn-primary"><span class="glyphicon glyphicon-lock" ></span></label>
                        </span>
                        <form:input id="password"  path="pwd" class="form-control" type="password" placeholder="Password" required="required"/>
                         </div>
                         </div>
                         </div>
                    <div class="row">
                    <div class="form-group col-xs-12">
                       <div class="input-group">
                       <span class="input-group-btn">
                       <label class="btn btn-primary"><span class="glyphicon glyphicon-lock" ></span></label>
                        </span>
                       <form:input  id=" Confirm_password"  path="pwd2" class="form-control"  type="password" placeholder="Confirm Password" required="required"/>
                        </div>
                    </div>
                </div>
          
                      <div class="row" style="margin-left: 0px;">
                      
                             <button type="submit" class="btn btn-success">Save</button>
                      
                             
                          <a href="LEhome.do" class="btn btn-danger">Cancel</a>
                      
                    </div>
                </div>
                 </div>
                 
                   </form:form>    
                 
    </div>
    </div>
    </div>
      <script src='js/bootstrapValidator.js'></script>
      					<script type="text/javascript">
					              
						$(document).ready(function() {
					    $('#change_password').bootstrapValidator({
					        feedbackIcons: {
					            valid: 'glyphicon glyphicon-ok',
					            validating: 'glyphicon glyphicon-refresh'
					        },
					    	 fields: {
						            cpwd: {
						                validators: {
						                    notEmpty: {
						                        message: 'Enter Current Password'
						                    },
						                	remote: {
					                               message: 'Invalid current password',
					                               url: 'check_currentPassword.do',
					                               delay: 2000,
					                               type: 'POST'
					                           }
											
						         
						                }
						            },
					    	pwd: {
				                validators: {
				                    notEmpty: {
				                        message: 'Password is required '
				                    },
				                    identical: {
				                        field: 'pwd2',
				                        message: 'Password and its confirm are not the same'
				                    },
				                    stringLength: {
					                	   min:6,
					                             max:8,
					                             message: 'Please enter a valid password of minimum 6 and 8 characters'
					                         } 
			                        
				                }
				            },
				          pwd2: {
				                validators: {
				                    notEmpty: {
				                        message: 'Confirm password is required'
				                    },
				                    identical: {
				                        field: 'pwd',
				                        message: 'password and its confirm are not the same'
				                    }
				                }
				            }
					    	 }
						
							});
						});
					</script>