<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>

<div class="modal fade" id="myLogin" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
     
        <div class="modal-header" style="background-color: #b3bf06;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login and Register</h4>
        </div>
        <div class="modal-body">
		<div class="tab_container">
			<input id="tab1" type="radio" name="tabs" checked>
			<label for="tab1" style="padding: 30px 35px";><i class="fa fa-sign-in"></i><span>Login</span></label>

			<input id="tab2" type="radio" name="tabs">
			<label for="tab2" style="padding: 30px 35px";><i class="fa fa-pencil-square-o"></i><span>Register</span></label>

			
			<section id="content1" class="tab-content">

					  <div class="alert alert-danger" style="display: none;" id="invalid_login">
        <center><strong style ="color:#FF4500"> <b>Invalid Credentials</b></strong> </center>
        </div>
    
  <form:form id="login_form" action="logging.do" class="form-horizontal" role="form" commandName="loginCommand">
  <div class = "col-md-12">
    <div class="form-group">
	<div class="form-group input-group">
	<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>

     <form:input type="text" class="form-control"  path="username" value="" placeholder="Username"   style="text-transform: capitalize;"  required="required"/>  
    </div>
    </div>
    <div class="form-group">
      <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-lock">&nbsp;</i></span>
     <form:input type="password" class="form-control" path="password" placeholder="Password" required="required" />
    </div>
    </div>
	<button type="submit" class="btn btn-primary" id="submit_Login" style="background-color:#b3bf06;border-color: #b3bf06">Login</button>
	
	<img id="login_regsiterGif"src="images/register-load.gif" style="display: none;" />
	</div>
</form:form>
    <a href="LEforgotPass.do">Forgot password ?</a>	
			</section>

			<section id="content2" class="tab-content">
				  <form:form id="register_form" action="saveUser.do"  role="form" modelAttribute="registerUser">
  <div class = "col-md-12" style="color: black;">
    <div class="form-group">
	<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>
       <form:input type="text" class="form-control"  path="firstname" value="" placeholder="First Name"  style="text-transform: capitalize;" required="required"/>
    </div>
    </div>
    <div class="form-group">
    <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>
     <form:input  type="text" class="form-control" path="lastname" value="" placeholder="Last Name" style="text-transform: capitalize;" required="required"/>                                     
     </div> 
    </div>
	 <div class="form-group">
          <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-user">&nbsp;</i></span>                                
      <form:input type="text" class="form-control" path="username" value="" placeholder="Username"  style="text-transform: capitalize;" required="required"/>      
    </div>
    </div>
	  <div class="form-group">
	<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
       <form:input type="email" class="form-control" path="email" placeholder="Email" required="required"/>
    </div>
    </div>
    <div class="form-group">
    <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-lock">&nbsp;</i></span>
        <form:input  type="password" class="form-control" path="password" placeholder="Password" required="required"/>
    </div>
    </div>
	
	  <div class="form-group">
	<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-lock">&nbsp;</i></span>
      <input id="confirmpassowrd" type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password" required="required"/>
    </div>
    </div>
    <div class="form-group">
    <div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-mobile fa-lg">&nbsp;</i></span>
     <input  type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required="required"/>
    </div>
    </div>
	<button type="submit" class="btn btn-primary" style="background-color:#b3bf06;border-color: #b3bf06">Submit</button>
	</div>
</form:form>
			</section>
		</div>
        </div>
        <div class="modal-footer" style="padding: 15px;border-top:none;">
        
        </div>
      </div>
      
    </div>
  </div>
  <div class="banner" style="background-color: white;">
		<div class="header">
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"/></a>
						</h1>
					</div>
				</div>
	<div class="header-bottom">
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
														<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black"  role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
								</ul>
									</li>
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
										<c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do" style="font-size: 14px;color:black;"><span class="glyphicon glyphicon-lock" style="font-size: 14px;color:black "></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" style="font-size: 14px;color:black;"><span class="	glyphicon glyphicon-off" style="font-size: 14px;color:black"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
		</div>					
		
	      </nav>		
				</div>
		        </div>
        	</div>
       </div>
    </div>
<div class="banner-top">

		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides callbacks callbacks1" id="slider4">
					<li>
						<div class="w3layouts-banner-top img-responsive" style="width: 100%;height: auto;">
							<div class="container">
								<div class="agileits-banner-info">
									<h3 class ="wow animated lightSpeedIn" id = "service1">We will get you ready for the ride of your Life</h3>
								
									<div class="w3-button">
										<a href="LEabout.do">Explore</a>
									</div>
								</div>	
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top w3layouts-banner-top1 img-responsive" style="width: 100%;height: auto;">
							<div class="container">
								<div class="agileits-banner-info">
									<h3 class ="wow animated bounceInDown" id = "service2">We provide more than a Client ask</h3>
									
									<div class="w3-button">
										<a href="LEabout.do">Explore</a>
									</div>
								</div>	
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top w3layouts-banner-top2 img-responsive" style="width: 100%;height: auto;">
							<div class="container">
								<div class="agileits-banner-info">
									<h3 class ="wow animated bounceInUp" id = "service3">We are not diffrent , We make it happen diffrently</h3>
									
									<div class="w3-button">
										<a href="LEabout.do">Explore</a>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="clearfix"> </div>
			<script src="js/responsiveslides.min.js"></script>
			<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider4").responsiveSlides({
							auto: true,
							pager:true,
							nav:true,
							speed: 500,
							namespace: "callbacks",
							before: function () {
							  $('.events').append("<li>before event fired.</li>");
							},
							after: function () {
							  $('.events').append("<li>after event fired.</li>");
							}
						  });
					
						});
			</script>
			<!--banner Slider starts Here-->
		</div>

	</div>
	<!-- banner -->
<%-- 	<div class="banner">
		<div class="header" style="background-color: white;">
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locusera.png" class = "img-responsive" alt = "Locusera" style = "width:500px;"></a>
						</h1>
					</div>
				</div>
				<div class="header-right">
					<div class="agileinfo-social-grids">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-rss" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus" style="color: black;"></i></a></li>
							<c:if test="${empty frontUserLoggedIn}">
								<li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="color: black;margin-left: 3em;"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
								</c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 15px;">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" ><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="container">
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav" style="background-color:black;opacity: 0.8;">
									<li><a class="active list-border" href="LEhome.do">Home</a></li>
									<li><a href="LEabout.do">About</a></li>
									<li class=""><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Recruitment / Talent Acquisition</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Out Sourcing & Permanent Staffing</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Training And Development</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Typography</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Payroll management</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Compliance Management</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Green Field Project Execution</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">HR & Legal Audits</a></li>
												<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Patents, IPR, Trademark & Copyrights filling</a></li>
										</ul>
									</li>								
									<li><a href="LEjobs.do">Job Seeker</a></li>
									<li><a href="LEblog.do">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do">Contact Us</a></li>
								</ul>	
								<div class="clearfix"> </div>
							</div>	
						</nav>		
				</div>
			</div>
		</div>
	</div> --%>
	<!-- //banner -->
	<!-- welcome -->
	<div class="welcome">
		<div class="container bg-2">
			<div class="w3-welcome-grids">
				<div class="col-md-7 w3l-welcome-left">
					<h2>Welcome to Locusera</h2>
					<p>Locusera Solutions Pvt. Ltd. is an organization that provide innovative and strategic path in making Human Resource more customized to the needs of various Industries. Nevertheless, focusing also on the career prospects of the personnel and has been recognized as the most resourceful entity in consulting services Pan India.We work as a collaborative, caring partner to ensure the efficient delivery of HR Services in meeting the needs of those we serve..</p>
					<div class="w3ls-button">
						<a href="LEabout.do">View More</a>
					</div>
				</div>
		
				<div class="col-md-5 w3l-welcome-right">
					<div class="w3l-welcome-right-grids">
						<div class="col-sm-5 w3l-welcome-right-img">
							<img src="images/16.jpg" alt="" />
						</div>
						<div class="col-sm-7 w3l-welcome-right-img1">
							<img src="images/5.jpg" alt="" />
						</div>
						<div class="clearfix"> </div>
					</div>
				
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	</div>
	<!-- //welcome -->
	<!-- services -->
	<div class="services">
		<div class="container ">
			<div class="agileits-services-grids bg-3">
				<div class="col-md-4 agileinfo-services-left">
				<h3 style="text-align: center;padding: 20px 0px">What's New ?</h3>
				<div class="green">
		
		
				<div class="centered">
					<div id="nt-example1-container">
						<i class="fa fa-chevron-circle-up" id="nt-example1-prev" style="color: white;font-size: 30px;"></i>
		                <ul id="nt-example1" style="border-radius: 5px;">
		                  <c:forEach items="${newsList}" var="nList">
		                    <li><h3 style="font-size: 16px;">${nList.news_heading}</h3><p style="color:#000;white-space: nowrap; width: 100%;max-width: 28em;overflow: hidden;text-overflow: ellipsis;font-size: 15px;">${nList.news_content}</p><a href="aboutNews.do?id=${nList.news_id}" style="color: red;">Read more...</a></li>

		                    </c:forEach>
		                </ul>
		                <i class="fa fa-chevron-circle-down" id="nt-example1-next" style="color: white;font-size: 30px;"></i>
		            </div>
				</div>
		
	</div>
				</div>

						<div class="col-md-4 agileinfo-services-left">
				<h3 style="text-align: center;padding: 20px 0px">Services</h3>
				<div class="green">
		
		
				<div class="centered">
					<div id="nt-example2-container">
						<i class="fa fa-chevron-circle-up" id="nt-example2-prev" style="color: white;font-size: 30px;"></i>
		                <ul id="nt-example2" style="border-radius: 5px;">
		                
		                 <li><h3 style="font-size: 16px;">Recruitment / Talent Acquisition</h3><p style="color: black;">Recruiting isn't just about filling open positions</p> <a href="LEservices.do" style="color: red;">Read more...</a>
		                    <li><h3 style="font-size: 16px;">Out Sourcing & Permanent Staffing</h3><p style="color: black;">We are providers of recruitment </p> <a href="LEservices.do" style="color: red;"> Read more...</a></li>
		                    <li><h3 style="font-size: 16px;">Training And Development</h3> <p style="color: black;">We are concerned with organizational </p><a href="LEservices.do" style="color: red;"> Read more...</a></li>
<!-- 		                 <li><h3 style="font-size: 20px;">Typography</h3> <a href="LEservices.do" style="color: black;">Read more...</a></li>
 -->		                 <li><h3 style="font-size: 16px;">Payroll management</h3> <p style="color: black;">we are here by provide you a comprehensive </p><a href="#" style="color: red;">Read more...</a> </li>
		                    <li><h3 style="font-size: 16px;">Compliance Management</h3><p style="color: black;">We had built a reputation of maintaining </p><a href="LEservices.do" style="color: red;">Read more...</a></li>
		                    <li><h3 style="font-size: 16px;">Green Field Project Execution</h3><p style="color: black;">We believe that the ideal aim of the Greenfield</p><a href="LEservices.do" style="color: red;">Read more...</a> </li>
		                    <li><h3 style="font-size: 16px;">HR & Legal Audits</h3><p style="color: black;">We provide HR & Legal audit in a comprehensive </p><a href="LEservices.do" style="color: red;">Read more...</a> </li>
		                    <li><h3 style="font-size: 16px;">Patents, IPR, Trademark & Copyrights filling</h3><p style="color: black;">We are also having  </p><a href="LEservices.do" style="color: red;">Read more...</a> </li>
		               
		                </ul>
		                <i class="fa fa-chevron-circle-down" id="nt-example2-next" style="color: white;font-size: 30px;"></i>
		            </div>
				</div>
		
	</div>
				</div>

		<div class="col-md-4 agileinfo-services-left">
				<h3 style="text-align: center;padding: 20px 0px">Careers</h3>
				<div class="green">
		
		
				<div class="centered">
					<div id="nt-example3-container">
						<i class="fa fa-chevron-circle-up" id="nt-example3-prev" style="color: white;font-size: 30px;"></i>
		                <ul id="nt-example3" style="border-radius: 5px;">
		                  <c:forEach items="${jobLists}" var="jList">
		          <li><h3 style="font-size: 16px;">${jList.industry},${jList.sub_industry}</h3><p style="color:#000;white-space: nowrap; width: 100%;max-width: 28em;overflow: hidden;text-overflow: ellipsis;font-size: 15px;"><a href="LEjobs.do" style="color: red;">Read more...</a></li>
		                    
		                    </c:forEach>
		                </ul>
		                <i class="fa fa-chevron-circle-down" id="nt-example3-next" style="color: white;font-size: 30px;"></i>
		            </div>
				</div>
				

			
	</div>
				</div>

		
			
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //services -->
<!-- 	<div class = "container">
		<div id="wrapper" class = "agileinfo-services-left">
	<h3 style="text-align: center;">Our <span style="color: black;">Job Posts</span></h3>
		<ul id="ticker">
			<li>Job Opening in <mark>Locousera</mark> Solutions Private Limited <a href = "">Find a Job , Click Here</a></li>
			<li>Job Opening in <mark>Cognigent</mark> Solutions Private Limited <a href = "">Find a Job , Click Here</a></li>
			<li>Job Opening in <mark>Hindustan</mark> Solutions Private Limited <a href = "">Find a Job , Click Here</a></li>
			<li>Job Opening in <mark>Jagahunt</mark> Solutions Private Limited <a href = "">Find a Job , Click Here</a></li>
		</ul>
	</div>
	</div> -->
		<div class="testimonials" >
		<div class="container">
			<h3 class="w3_agile_head" style="color: #b3bf06;font-size: 32px;">Testimonials</h3>
			<div class="w3_agileits_testimonial_grids">
				<section class="slider" style="display: block;">
					<div class="flexslider">
						<ul class="slides">
						 <c:forEach items="${monials}" var="monial" varStatus="status">
							<li>
								<div class="w3_agileits_testimonial_grid">
									<p><i class="fa fa-quote-right" aria-hidden="true"></i>${monial.testimonial_content}</p>
									<img src="${monial.testimonial_image}" alt=" " class="img-responsive" style="height:75px;" />
									<h4><span>${monial.customer_name}</span></h4>
								</div>
							</li>
					</c:forEach>
						</ul>
					</div>
				</section>
				<!-- flexSlider -->
					<script defer src="js/jquery.flexslider.js"></script>
					<script type="text/javascript">
					$(window).load(function(){
					  $('.flexslider').flexslider({
						animation: "slide",
						start: function(slider){
						  $('body').removeClass('loading');
						}
					  });
					});
				  </script>
				<!-- //flexSlider -->    	
			</div>
		</div>
	</div>
	<!-- news -->
		 

   
	
        <div class="container-fluid services1">
				 <h2 style = "text-align: center;color: #b3bf06;font-size: 32px;" id = "service5">Our Prestigeous Clients</h2>
          <div class="row">
            <div class="span12">

              <div id="owl-demo" class="owl-carousel">
              <c:forEach items="${logo}" var="logo" varStatus="status">
                <div class="item"><img src="${logo.client_logo}" alt="client" class = "imge"></div>
   
          </c:forEach>
              </div>
              
            </div>
          </div>
        </div>
         <script src='js/bootstrapValidator.js'></script>
        <script>
        $(document).ready(function(){
     
        
			$('#login_form').on('click',function(e) {

								var user_id = $("#username").val();
								var password = $("#password").val();
								

								/*<![CDATA[*/

								if (user_id.toString() != ""
										&& password.toString() != "") {
									$('#login_regsiterGif').show();
									$
											.ajax({
												type : "POST",
												url : "userlogin.do",
												data : {
													"userid" : user_id,
													"pws" : password
												},
												success : function(
														responseData) {
													for ( var key in responseData) {

														if (responseData[key] == false) {
															$(
																	'#invalid_login')
																	.show();
															$(
																	'#login_regsiterGif')
																	.css(
																			'display',
																			'none');
							

															return false;
														} else {

															$(
																	'#login_regsiterGif')
																	.show();

															$(
																	'#login_form')
																	.trigger(
																			'submit');
														}

													}

												}
											});
								}

								return false;
								/*]]>*/

							});
        
			 $('#register_form').bootstrapValidator({
			        feedbackIcons: {
			            valid: 'glyphicon glyphicon-ok',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			            firstname: {
			                validators: {
			                    notEmpty: {
			                        message: 'First Name is required'
			                    },
			                    regexp : {
									regexp : /^[a-zA-Z\s]+$/,
									message : 'Sorry !! Only alphabets'
								}
			                }
			            },
			            lastname: {
			                validators: {
			                    notEmpty: {
			                        message: 'Last Name is required'
			                    },
			                    regexp : {
									regexp : /^[a-zA-Z\s]+$/,
									message : 'Sorry !! Only alphabets are allowed.'
								}
			                }
			            },
			            username: {
			                validators: {

			                    notEmpty: {
			                        message: 'Username is required'
			                    },
			                 
								remote: {
		                               message: 'Username already existed.',
		                               url: 'check_username.do',
		                               delay: 2000,
		                               type: 'POST'
		                           }
			                }
			            },
			            email : {
						
							validators : {

								notEmpty : {
									message : 'Email is required'
								},
								regexp : {
									regexp : /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
									message : 'Invalid Email'
								},
								remote: {
		                               message: 'Email is already existed.',
		                               url: 'check_useremail.do',
		                               delay: 2000,
		                               type: 'POST'
		                           }

							}
						},
			                    
						 password : {

								validators : {
									notEmpty : {
										message : 'Password is required.'
									},

									identical : {
										field : 'confirmPassword',
										message : 'Password and its Confirm Password are not the same.'
									},
					                   stringLength: {
					                	   min:6,
					                             max:8,
					                             message: 'Please enter a valid password of minimum 6 and 8 characters'
					                         } 
			                       
								}
							},
							confirmPassword : {

								validators : {

									notEmpty : {
										message : 'Confirm Password is required.'
									},
									identical : {
										field : 'password',
										message : 'Password and its Confirm Password are not the same.'
									}
								}
							},

			                 
			         mobile: {
			        	 validators: {
			        		 notEmpty: {
		                           message: 'Mobile number is required.'
		                       },
                      regexp: {
		                           regexp:/^[789]\d{9}$/,
		                           message: 'Please enter a valid 10 digit mobile number'
		                       },
			                    
			                  /*  stringLength: {
			                	   min:10,
			                             max: 10,
			                             message: 'Mobile Numbers must be 10 numbers'
			                         },  */
			                         
			                         remote: {
			                               message: 'Mobile number already exits',
			                               url: 'check_userphone.do',
			                               delay: 2000,
			                               type: 'POST'
			                           }
			                        
			                      }
			                
			                  }
			                 }
			 });
        });
        
        
        </script>
