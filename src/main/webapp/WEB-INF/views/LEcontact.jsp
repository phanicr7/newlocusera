	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!-- changed Here -->
<!-- <div class="banner about-banner" style="background: url(images/contactus.jpg) no-repeat 0px 0px;background-size: cover;"> -->


<style>
.small{
    width: 170px;
    margin: 15px 20px;
}
body{
font-family: 'Open Sans', sans-serif !important;
}

.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}
</style>
			<div class="banner" style="background-color: white url(images/contactus.jpg);">
		<div class="header">
				
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class ="img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>
			
						<div class="header-bottom">
	
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
												<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
							
											
										</ul>
									</li>							
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
 <c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do" style="font-size: 14px;color:black;"><span class="glyphicon glyphicon-lock" style="font-size: 14px;color:black"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" style="font-size: 14px;color:black;" ><span class="	glyphicon glyphicon-off" style="font-size: 14px;color:black"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
								</ul>	
								
		</div>	
			</nav>		
				</div>
		</div>
				
			</div>
		</div>
</div>
<!-- changed Here -->
	<!-- //banner -->
	<div class="about-heading">	
		<!-- <div class="container">
			<h2>Contact</h2>
		</div> -->
	</div>
	<!-- contact -->
<div class="codes icons main-grid-border">
   
<div class = "container">
<div class="about-heading">	
	
			<h2 style ="color: #b3bf06;padding:1.5em;">Contact Us</h2>
 <c:if test="${success=='success'}" >
        <div class="alert alert-success" id="alertFadeout">
        <center><strong style ="color:green"> <b>Thanks For Contacting Us, We Will Get Back To You Shortly</b></strong> </center>
        </div>
        </c:if>
	</div>
		<div class = "col-md-6">
				<form:form id="contact_form"  action="enquirySave.do" method="post" class="form-horizontal" commandName="command" > 
				  <div class="form-group">
    <label for="name">Name :</label>
    <form:input type="text" path="username" class="form-control"  style="text-transform: capitalize;" id="name"/>
  </div>
  <div class="form-group">
    <label for="email">Email address:</label>
    <form:input  path="email" class="form-control" id="email"/>
  </div>
  <div class="form-group">
    <label for="pwd">Mobile Number:</label>
    <input type="text" class="form-control" id="phone" name="phone"/>
  </div>
        <div class="form-group"> 
        <label >State</label>
         <form:select path="state_id" id="state" class="form-control selectpicker">
          <c:forEach items="${states}" var="state" varStatus="status" >
          <c:choose>
           <c:when test="${status.index==0}"> 
            <option value="0">Select State</option>
           <option value="${state.state_id}">${state.statename}</option></c:when>
                     <c:otherwise><option value="${state.state_id}">${state.statename}</option></c:otherwise>
                 </c:choose>
                </c:forEach>
                </form:select>
        </div>
   <div class="form-group"> 
  <label >City</label>
<form:select path="city_id" id="city" class="form-control selectpicker">
                 
                </form:select>
        </div>
        <div class="form-group"> 
  <label  >Location</label>
    <form:select path="location_id" id="location" class="form-control selectpicker">
                 
                </form:select>
        </div>
<div class="form-group">
  <label >Description</label>

       
        	<form:textarea class="form-control"  path ="description" name="description" placeholder="Description"></form:textarea>
</div>
  <button type="submit" class="btn btn-default" style="background-color:#b3bf06;border-color: #b3bf06;color: #000;">Submit</button>
</form:form>
			</div>

<div class = "col-md-3">
					<h2>Corporate Office</h2>	
			<h4 >Locusera Solutions Pvt. Ltd</h4>	
			<h5><u style="line-height: 40px;">Corporate Office Address:</u></h5>
				 <h5 style="line-height: 35px;font-size:12px;">Road No:13,Snehapuri Colony, Nagole , Hyderabad </h5>
				<h5 style="line-height: 35px;font-size:12px;">Telephone Number:<span>040-2414 5626. </span></h5>
	
				
			</div>

					<div class = "col-md-3">
					<h2>Branch Office</h2>
			<h4>Locusera Solutions Pvt. Ltd</h4>	
			<h5><u style="line-height: 40px;">Branch Office Address:</u></h5>
				 <h5 style="line-height: 35px;font-size:12px;">#Door No:8-18-18, Indira Nagar,Near Maa Gardens, Vizianagaram </h5>
				<h5 style="line-height: 35px;font-size:12px;">Telephone Number:<span> 08922-272715, 272716.</span></h5>
			</div>
			
	
</div>
 <div class="container">
  <div class="row">
 <div class="col-md-6">
      <h2>Corporate Office</h2>
    <iframe width="100%" height="400" src="https://www.maps.ie/create-google-map/map.php?width=100%&amp;height=400&amp;hl=en&amp;q=Road%20No%3A13%2CSnehapuri%20Colony%2C%20Nagole%20%2C%20Hyderabad+(Locusera%20Solutions%20Pvt%20Ltd)&amp;ie=UTF8&amp;t=&amp;z=19&amp;iwloc=A&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/fr/mesurer-distance-surface-google-maps.html">Mesurer distance avec Google Maps</a></iframe>    </div> 
	<div class="col-md-6">
      <h2>Branch Office</h2>
      <iframe width="100%" height="400" src="https://www.maps.ie/create-google-map/map.php?width=100%&amp;height=400&amp;hl=en&amp;q=Near%20Maa%20Gardens%2C%20Vizianagaram+(Locusera%20Solutions%20Pvt%20Ltd)&amp;ie=UTF8&amp;t=&amp;z=19&amp;iwloc=A&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/fr/mesurer-distance-surface-google-maps.html">Mesurer distance avec Google Maps</a></iframe>
      
    </div>
</div>
  </div> 

  </div>
   <script src='js/bootstrapValidator.js'></script>
  <script type="text/javascript">
					              
						 $(document).ready(function() {
						 $('#contact_form').bootstrapValidator({
						        feedbackIcons: {
						            valid: 'glyphicon glyphicon-ok',
						            validating: 'glyphicon glyphicon-refresh'
						        },
						        excluded: ':disabled',
						        fields:{
						        	username: {
						                validators: {
						                     stringLength: {
						                       min:6,
						                       max:8,
						                        message: 'Name should have maxmimum 6 and 8 characters '

						                    },
						                    notEmpty: {
						                        message: 'Name is required '
						                    },
						                    regexp : {
												regexp : /^[a-zA-Z\s]+$/,
												message : 'Sorry !! Only alphabets are allowed.'
											}
						               
										
						                }
						            },
						            email : {
									
										validators : {

											notEmpty : {
												message : 'Email is required'
											},
											regexp : {
												regexp : /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
												message : 'Please enter a valid email address'
											}/* ,
											remote: {
					                               message: 'Email is already existed.',
					                               url: 'check_enmail.do',
					                               delay: 2000,
					                               type: 'POST'
					                           } */
										}
									},
							            city_id: {
							                validators: {
							                    notEmpty: {
							                        message: 'Select City'
							                    }
							                }
							            }, 
							            location_id: {
							                validators: {
							                    notEmpty: {
							                        message: 'Select Location'
							                    }
							                }
							            }, 
							            description: {
							                validators: {
							                    notEmpty: {
							                        message: 'Description is required'
							                    }
							                }
							            },
						
									phone: {
						        	 validators: {
						        		 notEmpty: {
					                           message: 'Mobile Number is required.'
					                       },

					                    regexp: {
					                           regexp:/^[789]\d{9}$/,
					                           message: 'Please enter a valid 10 digit mobile number'
					                       }/* 
					                
						                    
						                   stringLength: {
						                	   min:10,
						                             max: 10,
						                             message: 'The mobile  numbers must be 10 numbers'
						                         } *//* ,
						                   	remote: {
						                               message: 'Mobile Number is already existed.',
						                               url: 'check_enphone.do',
						                               delay: 2000,
						                               type: 'POST'
						                           }
 */
						                       
						                        
						                      }
						                
						                  }
						                 }
						 });
					    });
						  $('#state').change(function(event) {
						    	
							   	 var $c=$("#state").val();
							   	   
							   	 
							   	   
							   	   if($c==0)
							   		   {
							   		      
							   		      var $city = $("#city");
							   		     var $location = $("#location"); 
							   		      $city.find('option').remove(); 
							   		     
							   		      
							   		      $('<option></option>').val("0").text("Select City").appendTo($city);
							   		      $location.val('');
							   		      
							   		      
							   		      
							   		   
							   		   }
							   	   else
							   		   {
							   		      
							   			
							   			$.ajax({
							   			    
							   			    url: "getcity.do",
							   			    type: 'GET',
							   			    dataType: 'json',
							   			    data: {id: $c },
							   			    contentType: 'application/json',
							   			    
							   			    success : function(response) {
							   			    	
							   			    	
							   			    	  var $select = $("#city");                           
							   				         
							   					  $select.find('option').remove();  
							   					 
							   					  
							   					  $('<option></option>').val("0").text("Select City").appendTo($select);
							   			        
							   			         $.each(response, function(key, value) {  
							   			        	   
							   			        	
							   							
							   			             $('<option></option>').val(key).text(value).appendTo($select);      
							   			              });
							   			    	
							   			    	
							   			    	
							   			    	
							   		           
							   			    },
							   			    error : function(error) {
							   			    	alert("some internal problem pls try again");
							   			    },
							   			
							   			});
							   			     
							   	     
							   		       
							   		       
							   		   
							   		   }
							   	
							   	

							   });






							   $('#city').change(function(event) {
							   	
							   	
							   	
							   	 var $c=$("#city").val();
							   	   
							   	 
							   	   
							   	   if($c==0)
							   		   {
							   		      
							   		      var $location = $("#location");
							   		    
							   		      $location.find('option').remove(); 
							   		     
							   		      
							   		      $('<option></option>').val("0").text("Select Location").appendTo($location);
							   		      
							   		      
							   		      
							   		      
							   		   
							   		   }
							   	   else
							   		   {
							   		      
							   			
							   			$.ajax({
							   			    
							   			    url: "getlocation.do",
							   			    type: 'GET',
							   			    dataType: 'json',
							   			    data: {id: $c },
							   			    contentType: 'application/json',
							   			    
							   			    success : function(response) {
							   			    	 
							   			    	
							   			    	  var $select = $("#location");                           
							   				         
							   					  $select.find('option').remove();  
							   					 
							   					  
							   					  $('<option></option>').val("0").text("Select Location").appendTo($select);
							   			        
							   			         $.each(response, function(key, value) {  
							   			        	   
							   			        	
							   							
							   			             $('<option></option>').val(key).text(value).appendTo($select);      
							   			              });
							   			    	
							   			    	
							   			    	
							   			    	
							   		           
							   			    },
							   			    error : function(error) {
							   			    	alert("some internal problem pls try again");
							   			    },
							   			
							   			});
							   			     
							   	     
							   		       
							   		       
							   		   
							   		   }
							   	
							   	

							   });
							
						</script>	
					<script>


$(document).ready(function(){
	
	$("#contact_form").submit(function(e){
		var st = $("#state").val(); 
		
	     if(st=="" || st==null || st==0)
	     { 
	    	 alert("Please Select State");  
	    	 e.preventDefault();
	    	 return false;
	        }


	});
    $("#alertFadeout").delay(2000).fadeOut();

});
	</script> 
