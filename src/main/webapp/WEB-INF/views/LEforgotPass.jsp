 <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<style>
body{
font-family: 'Open Sans', sans-serif !important;
}

.form-control-feedback {

    text-align: start;

}
.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}
</style>

<<%-- !-- 		<div class="banner about-banner" style="background: url(images/forgot1.jpg) no-repeat 0px 0px;background-size: cover;">
 -->			<div class="header" style="background-color: white;">
				<div class="container">
					<div class="header-left">
						<div class="w3layouts-logo">
							<h1>
								<a href="LEhome.do"><img class = "img-responsive" src = "images/locusera.png" alt = "Locusera" style = "width:500px;"></a>
							</h1>
						</div>
					</div>
					<div class="header-right">
						<div class="agileinfo-social-grids">
							<ul>
								<li><a href="#"><i class="fa fa-facebook" style="color: black;"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" style="color: black;"></i></a></li>
								<li><a href="#"><i class="fa fa-rss" style="color: black;"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus" style="color: black;"></i></a></li>
								<c:if test="${empty frontUserLoggedIn}">
								<li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="color: black;margin-left: 3em;"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
								</c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
       							 <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 15px;">
        					<i class="icon icon-user"></i>
       						 <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
         						 <ul class="dropdown-menu">
         						 <li><a href="LEchangePass.do"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
         						 <li class="divider"></li>
         						 <li class="dropdowm menu"><a href="userLogout.do" ><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
						      </ul>
						      </li>
						      </c:if>
							</ul>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="header-bottom">
				<div class="container">
					<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav" style="background-color:black;opacity: 0.8;">
									<li><a class=" list-border" href="LEhome.do">Home</a></li>
									<li ><a  href="LEabout.do">About</a></li>
									<li class=""><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Recruitment / Talent Acquisition</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Out Sourcing & Permanent Staffing</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Training And Development</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Typography</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Payroll management</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Compliance Management</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Green Field Project Execution</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">HR & Legal Audits</a></li>
												<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Patents, IPR, Trademark & Copyrights filling</a></li>
										</ul>
									</li>								
									<li><a href="LEjobs.do">Job Seeker</a></li>
									<li><a href="LEblog.do">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do">Contact Us</a></li>
								</ul>	
								<div class="clearfix"> </div>
							</div>	
						</nav>		
					</div>
				</div>
			</div>
		</div> --%>
		<!-- //banner -->
		<div class="header">
				
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>
			
						<div class="header-bottom">
	
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
						<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black"  role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
							
											
										</ul>
									</li>						
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
<c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do" style="font-size: 14px;color:black;"><span class="glyphicon glyphicon-lock" style="font-size: 14px;color:black"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" style="font-size: 14px;color:black;"><span class="	glyphicon glyphicon-off" style="font-size: 14px;color:black"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
											
								</ul>	
								
		</div>	

							
						</nav>		
				</div>
						
			
				
			
		</div>
				
			</div>
		</div>
		
		
  <div class="container">    
  <div id="forgotbox" style="margin-top:2%;" class="mainbox col-md-5 col-md-offset-3 col-sm-6 col-sm-offset-2">                    
  <div class="panel panel-info" >
  	   
			 <c:if test="${SentOTP=='SentOTP'}">
            <div class="alert alert-success">
           <strong>OTP Sent</strong> 
           </div>
           </c:if>
           <c:if test="${InvalidOTP=='InvalidOTP'}">
            <div class="alert alert-danger">
           <strong>Invalid OTP</strong> 
           </div>
           </c:if>
            <c:if test="${SentOTP=='SentOTP' || InvalidOTP=='InvalidOTP'}">
              <div class="panel-heading"  style="background-color:#ADD8E6;color:black;">
               <div class="panel-title" >
               <b >Enter OTP</b>
              </div>
              </div>
  
           <div style="padding-top:30px" class="panel-body" >
               
                 
             <form id="otp_form" role="form" action="check_OTP.do">
                 <div class="form-group">
                 <div style="margin-bottom: 25px" class="input-group">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
              <input id="otp"  name="otp" placeholder="Enter OTP"class = "form-control" required="required"/>
                 </div>
                 </div>
                
               
            <div class="row" style="margin-left: 0px;">
              <button class="btn icon-btn-save btn-success" type="submit">
                            <span class="btn-save-label"></span>Submit</button>
                             <a href="LEhome.do" class="btn icon-btn-danger btn-danger">Cancel</a>
                     </div> 
                     </form>
                    </div>
                   
           </c:if>
            <c:if test="${SentOTP!='SentOTP' && InvalidOTP!='InvalidOTP'}">
            
            <c:if test="${NotSentOTP=='NotSentOTP'}">
            <div class="alert alert-danger">
           <strong>Please Try Again</strong> 
           </div>
           </c:if>
              <div class="panel-heading"  style="color:black;">
               <div class="panel-title" >
               <b >Recovery Password</b>
              </div>
               </div>
                <div style="padding-top:30px" class="panel-body" >
                <div style="display:none" id="register-alert" class="alert alert-danger col-sm-12"></div>
                 <label class="control-label" style="margin:5px; color:black;">Please Enter Your  Mobile Number:</label>
                 
                <form id="forgot_form" role="form" action="send_OTP.do">
                 <div class="form-group">
                 <div style="margin-bottom: 25px" class="input-group">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                 <input id="mobile"  name="mobile" placeholder="Mobile Number"class = "form-control" required="required"/>
                 </div>
                 </div>
            <div class="row">
              <button class="btn icon-btn-save btn-success" type="submit">
                            <span class="btn-save-label"></span>Submit</button>
                             <a href="LEforgotPass.do" class="btn icon-btn-danger btn-danger">Cancel</a>
                    </div>
                    </form>   
                </div>             
                  </c:if> 
			   </div>
			   </div>
    </div>
     <script src='js/bootstrapValidator.js'></script>
    					<script type="text/javascript">
					              
						 $(document).ready(function() {
						 $('#forgot_form').bootstrapValidator({
						        feedbackIcons: {
						            valid: 'glyphicon glyphicon-ok',
						            validating: 'glyphicon glyphicon-refresh'
						        },
						        fields: {
						        	mobile: {
							        	 validators: {
							        		 notEmpty: {
						                           message: 'Mobile number is required.'
						                       },
						                       regexp: {
						                           regexp:/^[789]\d{9}$/,
						                           message: 'Mobile number is invalid.'
						                       },
							                    
							                   stringLength: {
							                	   min:10,
							                             max: 10,
							                             message: 'mobile  numbers must be 10 numbers'
							                         },
							                         
							                         remote: {
							                               message: 'Phone Number not registered',
							                               url: 'check_phoneforOTP.do',
							                               delay: 2000,
							                               type: 'POST'
							                           }
							                        
							                      }
							                
							                  }
							                 }
						 });
					    });
						    
						 
						</script>	