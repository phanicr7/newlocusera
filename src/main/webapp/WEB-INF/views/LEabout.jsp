		<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
	<style>
	html,body
	{
	font-family: 'Open Sans', sans-serif !imporant;
	}

	input, section {
	  clear: both;
	  padding-top: 10px;
	  display: none;
	}

	label {
	 
	  cursor: pointer;
	  text-decoration: none;
	  text-align: center;
	  background: #f0f0f0;
	  color: black;
	}
	@media (min-width: 768px) {

	    .brand-pills > li > a {
	        border-top-right-radius: 0px;
	    	border-bottom-right-radius: 0px;
	    }
	    
	    li.brand-nav.active a:after{
	    	content: " ";
	    	display: block;
	    	width: 0;
	    	height: 0;
	    	border-top: 20px solid transparent;
	    	border-bottom: 20px solid transparent;
	    	border-left: 9px solid #2d2a23;
	    	position: absolute;
	    	top: 50%;
	    	margin-top: -20px;
	    	left: 100%;
	    	z-index: 2;
	    }
	}

	#tab1:checked ~ #content1,
	#tab2:checked ~ #content2,
	#tab3:checked ~ #content3,
	#tab4:checked ~ #content4,
	#tab5:checked ~ #content5 {
	  display: block;
	  padding: 20px;
	  background: #fff;
	  color: #999;
	  border-bottom: 2px solid #f0f0f0;
	}

	.tab_container .tab-content p,
	.tab_container .tab-content h3 {
	  -webkit-animation: fadeInScale 0.7s ease-in-out;
	  -moz-animation: fadeInScale 0.7s ease-in-out;
	  animation: fadeInScale 0.7s ease-in-out;
	}
	.tab_container .tab-content h3  {
	  text-align: center;
	}

	.tab_container [id^="tab"]:checked + label {
	  background: #fff;
	  box-shadow: inset 0 3px #0CE;
	}

	.tab_container [id^="tab"]:checked + label .fa {
	  color: #0CE;
	}

.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}
	</style>
<!-- banner -->

<!-- 		<div class="banner about-banner" style="background: url(images/aboutus.jpg) no-repeat 0px 0px;background-size: cover;"> 
 -->

			<div class="banner" style="background-color: white url(images/aboutus.jpg);">
		<div class="header">
				
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"/></a>
						</h1>
					</div>
				</div>
			
						<div class="header-bottom">
	
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
															<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
							
											
										</ul>
									</li>				
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
									<c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do"style="font-size: 14px;color:black;" ><span class="glyphicon glyphicon-lock" style="font-size: 14px;color:black"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" style="font-size: 14px;color:black;"><span class="	glyphicon glyphicon-off" style="font-size: 14px;color:black"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
								</ul>	
		      </div>
		      </nav>		
				</div>
						
			
				</div>
			
		</div>
				
			</div>
		</div>
<%-- 		<div class="banner about-banner" style="background: url(images/hheader.jpg) no-repeat 0px 0px;background-size: cover;">

			<div class="header" style="background-color: white;">
				<div class="container">
					<div class="header-left">
						<div class="w3layouts-logo">
							<h1>
								<a href="LEhome.do"><img class = "img-responsive" src = "images/locusera.png" alt = "Locusera" style = "width:500px;"></a>
							</h1>
						</div>
					</div>
					<div class="header-right">
						<div class="agileinfo-social-grids">
							<ul>
								<li><a href="#"><i class="fa fa-facebook" style="color: black;"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" style="color: black;"></i></a></li>
								<li><a href="#"><i class="fa fa-rss" style="color: black;"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus" style="color: black;"></i></a></li>
								<c:if test="${empty frontUserLoggedIn}">
								<li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="color: black;margin-left: 3em;"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
								</c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
       							 <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 15px;">
        					<i class="icon icon-user"></i>
       						 <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
         						 <ul class="dropdown-menu">
         						 <li><a href="LEchangePass.do"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
         						 <li class="divider"></li>
         						 <li class="dropdowm menu"><a href="userLogout.do" ><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
						      </ul>
						      </li>
						      </c:if>
							</ul>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="header-bottom">
				<div class="container">
					<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav" style="background-color:black;opacity: 0.8;">
									<li><a class=" list-border" href="LEhome.do">Home</a></li>
									<li ><a  class="active" href="LEabout.do">About</a></li>
									<li class=""><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Recruitment / Talent Acquisition</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Out Sourcing & Permanent Staffing</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Training And Development</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Typography</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Payroll management</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Compliance Management</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Green Field Project Execution</a></li>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do">HR & Legal Audits</a></li>
												<li><a class="hvr-bounce-to-bottom" href="LEservices.do">Patents, IPR, Trademark & Copyrights filling</a></li>
										</ul>
									</li>								
									<li><a href="LEjobs.do">Job Seeker</a></li>
									<li><a href="LEblog.do">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do">Contact Us</a></li>
								</ul>	
								<div class="clearfix"> </div>
							</div>	
						</nav>		
					</div>
				</div>
			</div>
		</div> --%>
		<!-- //banner -->
		<!-- a-about -->

	<div class="codes icons main-grid-border">
	<div class="container" >
			<div class="about-heading">	
	
			<h2 style ="color: #b3bf06;padding:1.5em;">About Us</h2>

	</div>
		<div class="row">
			
	        <div role="tabpanel">
	        
	            <div class="col-sm-3" style="font-family: 'Open Sans', sans-serif;">
	                <ul class="nav nav-pills brand-pills nav-stacked" role="tablist" style="margin-top: 25px;">
	                    <li role="presentation" class="brand-nav active"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab" style="line-height: 30px;background-color:#2d2a23;color:white;">Over View</a></li>
	                    <li role="presentation" class="brand-nav"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab" style="line-height: 30px;background-color:#2d2a23;color:white;">Core Values</a></li>
	                    <li role="presentation" class="brand-nav"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab" style="line-height: 30px;background-color:#2d2a23;color:white;">Why Locusera</a></li>
	                   
	                </ul>
	         
	            </div>
	      
	            <div class="col-sm-9">
	            <h2 style="font-family: 'Open Sans', sans-serif;">WELCOME TO LOCUSERA SOLUTIONS PVT LTD</h2>
	                <div class="tab-content">
	                    <div role="tabpanel" class="tab-pane active" id="tab4">
	                       <div class="" style = "width:100%">
	  

	  <div class="panel-group" id="accordion">
	    <div class="panel panel-default">
	      <div class="panel-heading" style = "background: #bdcd00;font-family: 'Open Sans', sans-serif;">
	        <h4 class="panel-title">
	          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">VISION</a>
	        </h4>
	      </div>
	      <div id="collapse1" class="panel-collapse collapse in">
	        <div class="panel-body">
			<ul style = "color:#222222;font-family: 'Open Sans', sans-serif;">
			<li>To provide innovative, professional and personalized services to the Corporate World through HR Solutions.</li>
			<li>To maintain Quality Management System as per ISO guidelines with an objective to achieve the descried quality in every activity.</li>
			<li>To practice quality improvement process and achieve a significant improvement in business results.</li>	
			
			</ul>
			</div>
		  </div>
	    </div>
	    <div class="panel panel-default">
	      <div class="panel-heading" style = "background: #bdcd00;font-family: 'Open Sans', sans-serif;">
	        <h4 class="panel-title">
	          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">MISSION</a>
	        </h4>
	      </div>
	      <div id="collapse2" class="panel-collapse collapse">
			<div class="panel-body">
			<ul style = "color:#222222;font-family: 'Open Sans', sans-serif;">
			<li>To provide HR Solutions of International standards through value added services</li>
			<li>To establish our company amongst the Best HR Solutions players in all departments of our core competency.</li>
			</ul>
			</div>
	      </div>
	    </div>
	    <div class="panel panel-default">
	      <div class="panel-heading" style = "background: #bdcd00;font-family: 'Open Sans', sans-serif;">
	        <h4 class="panel-title">
	          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">COMMITMENT</a>
	        </h4>
	      </div>
	      <div id="collapse3" class="panel-collapse collapse">
	     <div class="panel-body">
			<p style = "text-align:justify;color:#222222;font-family: 'Open Sans', sans-serif;">We at LOCUSERA are committed to provide high-end services to our esteemed clients in terms of Recruitments, Staffing Solutions, Payroll Management, Compliance Management, HR Audits, Green Field Project Execution, Patent Filings (National & International), Trademark & Copyrights Filings.</p>
			<p style = "text-align:justify;color:#222222;font-family: 'Open Sans', sans-serif;">We focus our commitment by contributing to our society in terms of providing HR Solutions to potential Clients in the market, generating awareness and providing customized solutions to corporate Houses.</p>
			<p style = "text-align:justify;color:#222222;font-family: 'Open Sans', sans-serif;">We always ensure to follow all cardinal rules and regulations of the Indian Constitution, State Legislatures including Laws of the Land imposed by the State and Central Govt. as applicable.</p>
			</div>
	      </div>
	    </div>
	  </div> 
	</div>
	                    </div>
	                    <div role="tabpanel" class="tab-pane" id="tab5">
	                 		<ul style="font-family: 'Open Sans', sans-serif;">
	                           <li>At Locusera Solutions, we believe quality is not just another goal, it is our basic strategy for continuous growth.</li>
	                           <li>With strong focus on quality, trust and demonstrate strong commitment towards process excellence resulting in enhanced productivity with the best professionals and effective implementation of best practices at all levels.</li> 
	                     	</ul>
	                   
	                    </div>
	                    <div role="tabpanel" class="tab-pane" id="tab6">
	                    	<ul style="font-family: 'Open Sans', sans-serif;">	

	                          <li>We have the flexibility to deliver services quickly and cost-effectively.</li>
	                          <li> Our confidence in our ability to think ahead makes us willing to be measured against any business outcomes.</li>
	                          <li> Which means that with Locusera solutions, you get the advantage of acquiring quality services along with the advantage of being taken the best professionals within the stipulated time.</li>
	                        </ul>
	                      
	                    </div>
	                
	                </div>
	            </div>
	         
	        </div>
		</div>
	</div>
</div>
		<!-- //different -->
