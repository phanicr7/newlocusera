		<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
	<style>
body{
font-family: 'Open Sans', sans-serif !important;
}

	input, section {
	  clear: both;
	  padding-top: 10px;
	  display: none;
	}

	label {
	 
	  cursor: pointer;
	  text-decoration: none;
	  text-align: center;
	  background: #f0f0f0;
	  color: black;
	}
	@media (min-width: 768px) {

	    .brand-pills > li > a {
	        border-top-right-radius: 0px;
	    	border-bottom-right-radius: 0px;
	    }
	    
	    li.brand-nav.active a:after{
	    	content: " ";
	    	display: block;
	    	width: 0;
	    	height: 0;
	    	border-top: 20px solid transparent;
	    	border-bottom: 20px solid transparent;
	    	border-left: 9px solid #2d2a23;
	    	position: absolute;
	    	top: 50%;
	    	margin-top: -20px;
	    	left: 100%;
	    	z-index: 2;
	    }
	}

	#tab1:checked ~ #content1,
	#tab2:checked ~ #content2,
	#tab3:checked ~ #content3,
	#tab4:checked ~ #content4,
	#tab5:checked ~ #content5 {
	  display: block;
	  padding: 20px;
	  background: #fff;
	  color: #999;
	  border-bottom: 2px solid #f0f0f0;
	}

	.tab_container .tab-content p,
	.tab_container .tab-content h3 {
	  -webkit-animation: fadeInScale 0.7s ease-in-out;
	  -moz-animation: fadeInScale 0.7s ease-in-out;
	  animation: fadeInScale 0.7s ease-in-out;
	}
	.tab_container .tab-content h3  {
	  text-align: center;
	}

	.tab_container [id^="tab"]:checked + label {
	  background: #fff;
	  box-shadow: inset 0 3px #0CE;
	}

	.tab_container [id^="tab"]:checked + label .fa {
	  color: #0CE;
	}

.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}
	</style>
<!-- banner -->
<!-- 	<div class="banner about-banner" style="background: url(images/servic.jpeg) no-repeat 0px 0px;background-size: cover;">
 -->		<div class="header" style="background-color: white;">
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img class = "img-responsive" src = "images/locusera.png" alt = "Locusera" style = "width:500px;"/></a>
						</h1>
					</div>
				</div>
			<%-- 	<div class="header-right">
					<div class="agileinfo-social-grids">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-rss" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus" style="color: black;"></i></a></li>
								<c:if test="${empty frontUserLoggedIn}">
								<li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="color: black;margin-left: 3em;"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
								</c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
       							 <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 15px;">
        					<i class="icon icon-user"></i>
       						 <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
         						 <ul class="dropdown-menu">
         						 <li><a href="LEchangePass.do"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
         						 <li class="divider"></li>
         						 <li class="dropdowm menu"><a href="userLogout.do" ><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
						      </ul>
						      </li>
						      </c:if>							
						</ul>
					</div>
				</div> --%>
				<div class="clearfix"> </div>
			</div>
		</div>
<div class="banner" style="background-color: white;">
		<div class="header">
				
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>
			
						<div class="header-bottom">
	
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
														<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black"   role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
							
											
										</ul>
									</li>
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
              <c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do" style="font-size: 14px;color:black;"><span class="glyphicon glyphicon-lock" style="font-size: 14px;color:black"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" style="font-size: 14px;color:black;" ><span class="	glyphicon glyphicon-off" style="font-size: 14px;color:black " style="background-color:orange"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
	       </ul>	
		</div>
		</nav>		
	</div>
		</div>
				
			</div>
		</div>
</div>
	<!-- //banner -->
	<div class="about-heading">	
		<div class="container">
			<h2>Services</h2>
		</div>
	</div>
	<!-- icons -->
	<div class="codes icons main-grid-border">
		<div class="container"> 
			

			        <div role="tabpanel">
            <div class="col-sm-3">
                <ul class="nav nav-pills brand-pills nav-stacked" role="tablist" style="margin-top: 25px;">
                    <li role="presentation" class="brand-nav active"><a href="#tab4" aria-controls="tab1" role="tab" data-toggle="tab" style="line-height: 30px;background-color:#2d2a23;color:white;">Services</a></li>
                   
                   
                </ul>
            </div>

            <div class="col-sm-9">
            <h2>WELCOME TO LOCUSERA SOLUTIONS PVT LTD</h2>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab4">
                       <div class="" style = "width:100%">
  

  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Recruitment / Talent Acquisition</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service1.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>Recruiting isn't just about filling open positions. It's about understanding your aspirations so that we can find the right people to help transform your business.</li>
								<li>We dig deep to learn about the company culture, competitive landscape and ideal prospect.</li>
								<li>Armed with this information, we develop and execute a recruiting strategy that gets results.</li>
						</ul>
				</div>
		</div>
	  </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Out Sourcing & Permanent Staffing</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
		    <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service2.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>We are providers of recruitment process outsourcing in the employment services industry, enabling our clients to outsource the entire recruitment process for permanent staff to us, so they can focus on other areas of human resources.</li>
								<li>We also provide outsourcing for High Circle Recruitments & Bulk Recruitments through Advertisement</li>
								
						</ul>
				</div>
		</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Training And Development</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
    <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service3.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>We are concerned with organizational activities aimed at bettering the performance of individuals and groups in organizational settings.</li>
								<li>Training is now considered as more of retention tool than a cost.</li>
								<li>The training system in Indian Industry has been changed to create a smarter workforce and yield the best results.</li>
						</ul>
				</div>
		</div>
      </div>
    </div>

        <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Payroll management</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
    <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service4.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>we are here by provide you a comprehensive services in payroll, you can say goodbye to extra time spent administering your payroll, eliminates cost of initial hardware and software investment and its subsequent maintenance So that you can focus on more important things like, running your business.</li>
							
						</ul>
				</div>
		</div>
      </div>
    </div>

        <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Compliance Management</a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
    <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service5.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>We had built a reputation of maintaining a 100% statutory compliance record with regards to both its employees and its clients.	</li>
								<li>Our strong focus on Statutory Management and Statutory Compliance in HR has caused us to develop a highly sophisticated infrastructure designed to manage large scale and diverse requirements of our clients across India.</li>
								
						</ul>
				</div>
		</div>
      </div>
    </div>


        <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Green Field Project Execution</a>
        </h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
    <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service6.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>We believe that the ideal aim of the Greenfield Project is to provide end-to-end solutions for a business start-up.</li>
								<li>we offer you successful Project management including Right from Acquisition of land, registration with the concerned authority, and specific expertise in registering even in SEZ sites taking Stamp Duty and Registration Fee exemptions from the concern department and also preference being given to time and cost control of our clients.</li>
								
						</ul>
				</div>
		</div>
      </div>
    </div>

        <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">HR & Legal Audits</a>
        </h4>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
    <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service7.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>We provide HR & Legal audit in a comprehensive method to review current HR policies, procedures, documentation and systems to identify needs for improvement and enhancement of the HR functions as well as to assess compliance with ever changing rules and regulations.</li>
								<li>The purpose of an HR audit is to recognise strengeths and identify any needs for improvement in the HR functions.</li>
						</ul>
				</div>
		</div>
      </div>
    </div>

        <div class="panel panel-default">
      <div class="panel-heading" style = "background: #bdcd00;">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">Patents, IPR, Trademark & Copyrights filing</a>
        </h4>
      </div>
      <div id="collapse8" class="panel-collapse collapse">
    <div class="panel-body">
				<div class = "col-md-5">
						<img src = "images/service8.jpg" style="width: 320px;">
				</div>

				<div class="col-md-7">
						<ul>
								<li>We are also having expertise in providing guidance and consultation in core area of Trademarks, Copyrights, Patents & Designs, further extending assistance for registration & protection of intellectual property.</li>
								<li>Moreover Locusera facilitate guidance, consultation & assistance for incorporation of Partnership Firms, Pvt. Ltd. /Ltd. Companies, Societies, Co-operative Societies, Trust etc. Locusera has extended its activity to the core area of protection of IPR through litigation (for & against).</li>
							
						</ul>
				</div>
		</div>
      </div>
    </div>
  </div> 


    
                    </div>
                   
               
                
                </div>
            </div>
        </div>
	</div>


	</div>
	</div>
