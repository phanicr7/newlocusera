
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Locusera | Home </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Locusera solution pvt ltd,Service,Recruitment / Talent Acquisition, Out Sourcing & Permanent Staffing,Typography,Payroll management,Compliance Management,HR & Legal Audits,Patents, IPR, Trademark & Copyrights filling" /> 

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<link rel="stylesheet" href="css/testimonial_style.css" type="text/css" media="all" />
<!--// css -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />

 <link href="css/news_main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/magic.css">

    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    	<link href="css/breakingStyles.css" rel="stylesheet" >
		<link rel="shortcut icon" href="images/favicon.png">


<!-- //font-awesome icons -->
<!-- font -->
<!-- <link href="//fonts.googleapis.com/css?family=Playball&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'> -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
	<script type="text/javascript" src="js/tickerme.min.js"></script>
	<script type="text/javascript">
	$(function(){
		$('#ticker').tickerme();
	});
	</script>


    <script src="js/owl.carousel.js"></script>


    <!-- Demo -->




    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
      });

    });
    </script>

       <script>
$(document).ready(function(){
$(".bg-2").hide().delay(1000).fadeIn(2500).addClass('magictime tinUpIn');
$(".bg-3").hide().delay(1500).fadeIn(4500).addClass('magictime tinUpIn');

$("#service1").hide().delay(1000).fadeIn(1000).addClass('magictime tinLeftIn');
$("#service2").hide().delay(3000).fadeIn(2000).addClass('magictime spaceInLeft');
$("#service3").hide().delay(4000).fadeIn(2000).addClass('magictime spaceInRight')
});
</script>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
<style>
.news-grid-left p {
  
    color: #ef2e46;
    margin: 0;
    border: 1px solid #ef2e46;
    padding: .5em 0;
    text-align: center;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    -o-border-radius: 20px;
    -ms-border-radius: 20px;
}
body{
font-family: 'Open Sans', sans-serif !important;
}

/*   .imge {
    height: 100px;
    width: 260px;
} */
.services1{
 background-image: url("images/Consultancy.jpeg")
}
/*.tab_container {
	width: 90%;
	margin: 0 auto;
	padding-top: 70px;
	position: relative;
}*/

input, section {
  clear: both;
  padding-top: 10px;
  display: none;
}

label {
 
  cursor: pointer;
  text-decoration: none;
  text-align: center;
  background: #f0f0f0;
  color: black;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4,
#tab5:checked ~ #content5 {
  display: block;
  padding: 20px;
  background: #fff;
  color: #999;

}

.tab_container .tab-content p,
.tab_container .tab-content h3 {
  -webkit-animation: fadeInScale 0.7s ease-in-out;
  -moz-animation: fadeInScale 0.7s ease-in-out;
  animation: fadeInScale 0.7s ease-in-out;
}
.tab_container .tab-content h3  {
  text-align: center;
}

.tab_container [id^="tab"]:checked + label {
  background: #fff;
  box-shadow: inset 0 3px #0CE;
}

.tab_container [id^="tab"]:checked + label .fa {
  color: #0CE;
}
.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}

@media only screen and (min-width:1366px) and (max-width:1919px)
{
	.imge{
	height: 100px !important;
    width: 200px !important;
	}
	
	
}

@media only screen and (min-width:1920px)
{
	.imge{
	height: 100px !important;
    width: 260px !important;
	}
	
}

.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
	
@media only screen and (max-width:768px)
{
.banner {
	
    
  top: 0; 
     
     text-align: center;
     left: 0;
     width: 100%;
       background: rgba(51, 51, 51, 0.24) !important;
	}
}
@media (max-width: 800px)
.banner {
     min-height:0px !important;
}
</style>

</head>
<body>



 <tiles:insertAttribute name="body"></tiles:insertAttribute>
	<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="agile-footer-grids">
				<div class = "col-md-1"></div>
				<div class="col-md-3 w3-agile-footer-grid">
					<h3>Quick Links</h3>
					<ul>
					<a href = "LEhome.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>HOME</li></a>
					<a href = "LEabout.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>ABOUT</li></a>
					<a href = "LEservices.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>SERVICES</li></a>
					<a href = "LEjobs.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>JOB SEEKER</li></a>
					<a href = "LEblog.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>OUR BLOG</li></a>
					<a href = "LEcontact.do"><li style="color: white;"><i class="fa fa-chevron-right" aria-hidden="true" style="padding: 0px 8px;"></i>CONTACT US</li></a>
					</ul>
				</div>
							<h3 style="color: white;font-family: 'Open Sans', sans-serif;">Locusera Solutions Pvt. Ltd</h3>	
				<div class="col-md-4 w3-agile-footer-grid">
			
							
		
			<h5 style="color: white;"><u style="line-height: 40px;font-family: 'Open Sans', sans-serif;">Corporate Office Address:</u></h5>
				 <h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">Road No:13,Snehapuri Colony, Nagole , Hyderabad </h5>
				<h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">Telephone Number:<span>040-2414 5626. </span></h5>
				</div>
				<div class="col-md-4 w3-agile-footer-grid">
	
					
			
			<h5 style="color: white;"><u style="line-height: 40px;font-family: 'Open Sans', sans-serif;">Branch Office Address:</u></h5>
				 <h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">#Door No:8-18-18, Indira Nagar, Vizianagaram </h5>
				<h5 style="color: white;line-height: 35px;font-size:12px;font-family: 'Open Sans', sans-serif;">Telephone Number:<span> 08922-272715, 272716.</span></h5>
				</div>
	
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

		<div class="copyright">
			<div class="container">
				<p>� 2017  All Rights Reserved | Design by <a href="http://www.jagahunt.com/" style="color: white;"> Planet-E Software Solutions Pvt Ltd</a> </p>
			</div>
		</div>
	<!-- //footer -->

	<script src="js/responsiveslides.min.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	  <script src="js/jquery.newsTicker.js"></script>
    <script>
    	

    		$(window).load(function(){
	            $('code.language-javascript').mCustomScrollbar();
	        });
      
            var nt_example1 = $('#nt-example1').newsTicker({
                row_height: 80,
                max_rows: 4,
                duration: 3000,
                prevButton: $('#nt-example1-prev'),
                nextButton: $('#nt-example1-next')
            });
       
             var nt_example2 = $('#nt-example2').newsTicker({
                row_height: 80,
                max_rows: 4,
                duration: 4000,
                prevButton: $('#nt-example2-prev'),
                nextButton: $('#nt-example2-next')
            });
         

          var nt_example3 = $('#nt-example3').newsTicker({
                row_height: 80,
                max_rows: 4,
                duration: 5000,
                prevButton: $('#nt-example3-prev'),
                nextButton: $('#nt-example3-next')
            });
       
        </script>

        
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>	
</html>
