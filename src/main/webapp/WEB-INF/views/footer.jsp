<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<body>
<h3 class = "client-section" style="text-align:center;">our clients</h3>
<div class="container">
        <div class="row">
            <div class="span12">
                <div class="well">
                    <div id="myCarousel" class="carousel fdi-Carousel slide">
                     
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                            <c:forEach items="${logo}" var="logo" varStatus="status">
                             <c:choose>
                                
                                      
                                           <c:when test="${status.index==0}">
                                                      <div class="item active">
                                                            <div class="col-md-3">
                                                            <a href="#"><img src="${logo.client_logo}" class="img-responsive center-block"></a>
                                                            <div class="text-center">${status.index+1}</div>
                                                         </div>
                                                     </div>
                                          
      
                                           </c:when>
   
                                           <c:otherwise>
                                           
                                                      <div class="item">
                                                            <div class="col-md-3">
                                                            <a href="#"><img src="${logo.client_logo}" class="img-responsive center-block"></a>
                                                            <div class="text-center">${status.index+1}</div>
                                                           </div>
                                                     </div>
       
                                           </c:otherwise>
                                      
                                </c:choose>
                           </c:forEach>
                                
                            </div>
                            <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                            <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
	
		   <div class="categories-section video">
		   <div class="container">
		   <div class="footer-grids">

		 <div class="col-md-4 cat wow bounceIn animated" data-wow-delay="0.4s"  style="visibility: visible; -webkit-animation-delay: 0.4s;">
		   <h3>Quick Links</h3>
		   <ul>
	<li>HOME</li>
	<li>ABOUT</li>
	<li>SERVICES</li>
	<li>JOB SEEKER</li>
	<li>BUSINESS ENQUIRY</li>
	<li>OUR BLOG</li>
	<li>CONTACT US</li>
	</ul>
	</div>
		 <div class="col-md-4 cont wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		 <h3>contact</h3>
		 <ul>
		<li><i class="phone"></i></li>
		<li><p>1-000-000-0000</p>
		<p>1-000-000-0000</p></li>
		</ul>
		<ul>
	   <li><i class="smartphone"></i></li>
		<li><p>Seventh Avenue</p>
		<p> Chelsea, Manhattan</p></li>
		</ul>
		<ul>
		<li><i class="message"></i></li>
		<li><a href="mailto:example@mail.com">bcdefg@hijs.dfh</a>
         <a href="mailto:example@mail.com">fjashfaf@jkfs.ckd</a></li>
		</ul>
		</div>
		
				 <div class="col-md-4 cont wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		 <h3 style="color:white;font-size: 20px;font-family: 'Open Sans', sans-serif;">Corporate Office Address:</h3>	
			<h4>Locusera Solutions Pvt. Ltd</h4>	
				 <h5 style="line-height: 35px;font-family: 'Open Sans', sans-serif;">Road No:13,Snehapuri Colony, Nagole , Hyderabad </h5>
				<h5 style="line-height: 35px;font-family: 'Open Sans', sans-serif;">Telephone Number:<span>040-2414 5626 </span></h5>
	
			</div>
	<!-- 	 <div class="col-md-4 cont wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		 <embed width="420" height="315" src="https://www.youtube.com/embed/NpGiUJrN6Sw">
		</div> -->
		 <div class="clearfix"></div>
		  </div>
		   </div>
		   </div>
		   <div class="footer-section">
		   <div class="container">
		   <div class="footer-top">
		 <div class="social-icons wow bounceInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		<a href="https://www.facebook.com/locuserasolutions/" target="_blank"><i class="icon1"></i></a>
		
		<a href="https://plus.google.com/u/0/103626879475843346393" target="_blank"><i class="icon3"></i></a>
		
		</div>
		</div>
	
		<div class="footer-bottom wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
									<p> Copyright &copy;2017  All rights  Reserved | Design by:<a href="#" target="target_blank">Planet-E Software Solutions Pvt Ltd</a></p>
									</div>
					<script type="text/javascript">
						$(document).ready(function() {
							
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
				</div>
		   </div>
</body>
</html>
