		<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<script type="text/javascript">

$(document).ready(function(){
	
	
    $("#alertFadeout").delay(2000).fadeOut();

});


</script>>
<style>
div.center {
  text-align: center;
}

body{
font-family: 'Open Sans', sans-serif !important;
}
</style>
<!-- <div class="banner about-banner" style="background: url(images/ss.jpeg) no-repeat 0px 0px;background-size: cover;">
		<div class="header" style="background-color: white;"> -->


<style>
.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}
</style>

			<div class="banner" style="background-color: url(images/ss.jpeg) white;">
		<div class="header">
				

		<%-- 	<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>

				<div class="header-right">
					<div class="agileinfo-social-grids">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-rss" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus" style="color: black;"></i></a></li>
								<c:if test="${empty frontUserLoggedIn}">
								<li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="color: black;margin-left: 3em;"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
								</c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
       							 <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 15px;">
        					<i class="icon icon-user"></i>
       						 <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
         						 <ul class="dropdown-menu">
         						 <li><a href="LEchangePass.do"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
         						 <li class="divider"></li>
         						 <li class="dropdowm menu"><a href="userLogout.do" ><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
						      </ul>
						      </li>
						      </c:if>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div> --%>
		</div>
		
		<div class="header">
				
			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>
			
						<div class="header-bottom">
	
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
								<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
							
											
										</ul>
									</li>		
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
              <c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do" style="font-size: 14px;color:black;"><span class="glyphicon glyphicon-lock" style="font-size: 14px;color:black"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do"style="font-size: 14px;color:black;" ><span class="	glyphicon glyphicon-off" style="font-size: 14px;color:black"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
		</ul>	
		</div>
		</nav>		
				</div>
		</div>
			</div>
		</div>
				
			</div>
		

	<!-- //banner -->
	<div class="about-heading">	
		<!-- <div class="container">
			<h2>Careers</h2>
		</div> -->
    
        <hr>
        <hr>
        <div class="modal fade" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog " role="document">
            <div class="modal-content">
           <form:form id="jobs_form" action="jobSave.do" method="post" class="form-horizontal" commandName="joblistCommand" enctype="multipart/form-data">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Apply For Job</h4>
            </div>
            <div class="modal-body">
           <div class="col-sm-4 col-md-12">
            <div class="form-group">
            <div  class="input-group">
            <input type="hidden" value="" name="job_id" id="job_id"/>
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
           <form:input  path="candidate_name"   id="username"  placeholder="User Name" class="form-control"  type="text"  value="${userList.le_username}" readonly="true"/>
      
            </div>
            </div>
            </div>
              <div class="col-sm-4 col-md-12">
		    <div class="form-group">
		    <div  class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
           <form:input path="candidate_email" placeholder="E-Mail@" class="form-control"  type="email" value="${userList.le_user_email}" readonly="true"/>
            </div>
            </div>
            </div>
              <div class="col-sm-4 col-md-12">
            <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon" ><i class="glyphicon glyphicon-phone"></i></span>
            <span class="input-group-addon" style="font-size: 12px;">
			<button value="IND +91">IND +91</button>
		    </span>
            <form:input path="candidate_mobilenumber" class="form-control" placeholder="Phone Number" type="text"  value="${userList.le_user_phone}"  readonly="true"/>
            </div>
            </div>
            </div>
         <div class="col-sm-4 col-md-12">
            <div class="form-group">
            <div class="input-group">
	<label>Resume:</label><form:input type="file" path="candidate_resume" class="form-control" />
	</div>
         </div>
         </div>
         </div>
         
         <div class="modal-footer">
         <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-success">Send</button>
         </div>
             </form:form>
        </div>
        </div>
         </div>

<div class="about-heading">	
			<h2 style ="color: #b3bf06;padding: 1.5em;">Careers</h2>

	</div>
	<c:if test="${success=='success'}">
<div class="alert alert-success" id="alertFadeout">
			<a href="#" class="close"  data-dismiss="alert" aria-label="close">&times;</a>

		<center><strong>Success Appiled</strong></center>
        </div>
        </c:if> 
       
  <div class="codes icons main-grid-border">
<form id="form" class="form-inline" action="Lejobsearch.do" method="post" commandName="joblistCommand"  >
<div class = "container">
    <div class="form-group">
      <label for=""><h4>SEARCH:</h4></label>
     <select class="form-control" id="industryid" name="industryid" style="width: 335px;">
             <c:forEach items="${industry}" var="category" varStatus="status">
                                <c:choose>
                <c:when test="${status.index==0}"> <option value="0" >Select Industry</option>
                <option value="${category.industry_id}">${category.industryname}</option></c:when>
                   <c:otherwise><option value="${category.industry_id}">${category.industryname}</option></c:otherwise>
                                  </c:choose>
                
                </c:forEach>
       
      </select> 
             
     </div> 
     <button type="submit"  class="btn btn-success" >Submit</button>
 </div>    
 </form>    

        
<hr> 

<div class = "container">
		<div class="row">
		<c:forEach  items="${jobLists}" var="jobList" varStatus="theCount">
		<c:if test="${jobList.job_status=='Active'}">
						<div class="col-md-12  well box" style="background-color: white;box-shadow: 5px 3px 30px #888888;">

							
							
							<div class="col-md-12">

								<h4><b style = "color: #b3bf06;font-size: 20px;">${jobList.job_title}</b></h4>
								<h5 style = "font-size: 12px;" > <span class="glyphicon glyphicon-briefcase"></span>&nbsp;${jobList.experience}   / 
								 <span class="glyphicon glyphicon-map-marker"></span>&nbsp;${jobList.city_name},${jobList.location_name}
								</h5>
								<h6>Functional area : ${jobList.industry} ,${jobList.sub_industry}</h6>
 								<h6>No of openings : ${jobList.no_openings}</h6> 
 								   <c:if test="${not empty frontUserLoggedIn}">
							<button class="btn btn-primary " job_id="${jobList.job_id}"  type="button" data-toggle="modal" data-target="#applyModal" style="background-color:#b3bf06;border-color: #b3bf06" >Apply</button>	

								</c:if>
								<c:if test="${empty frontUserLoggedIn}">
											<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog " role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Apply For Job</h4>
           </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab" aria-controls="uploadTab" role="tab" data-toggle="tab">Registered User</a>

                        </li>
                        <li role="presentation"><a href="#browseTab" aria-controls="browseTab" role="tab" data-toggle="tab">Guest User</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                   <br>
  
      <div role="tabpanel" class="tab-pane active" id="uploadTab">
                       <center>
						<p style="color: red; display: none;" id="jobinvalid_login">Invalidcredentials</p>
					</center> 
      <form:form id="joblogin_form" action="logging.do" class="form-horizontal" role="form" commandName="loginCommand">
    <div style="margin-bottom: 25px" class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <form:input type="text" class="form-control" path="username" id="job_username" value="" placeholder="username"  required="required"/>                                        
     </div>
    <div style="margin-bottom: 25px" class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <form:input type="password" class="form-control" path="password" id="job_password" placeholder="password" required="required"/>
     </div>
    
     <div style="margin-top:10px" class="form-group">
     <div class="col-sm-12 controls">

     <button type="submit" class="btn btn-primary" id="job_Login">Login</button>
	<img id="joblogin_regsiterGif" src="images/register-load.gif" style="display: none;" />
	
     </div>
     </div>
     </form:form>
        </div>
      <div role="tabpanel" class="tab-pane" id="browseTab">
      <form:form id="jobss_form" action="guestJob.do" method="post" class="form-horizontal" commandName="joblistCommand" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="col-sm-4 col-md-12">
      <div class="form-group">
            <div  class="input-group">
            <input type="hidden" value="" name="job_id" id="jobs_id"/>
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
           <form:input  path="candidate_name"    placeholder="Name" class="form-control"  type="text"  />
      
            </div>
            </div>
            </div>
              <div class="col-sm-4 col-md-12">
		    <div class="form-group">
		    <div  class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
           <form:input path="candidate_email" placeholder="Email" class="form-control"  type="email" />
            </div>
            </div>
            </div>
              <div class="col-sm-4 col-md-12">
            <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon" ><i class="glyphicon glyphicon-phone"></i></span>
            <span class="input-group-addon" style="font-size: 12px;">
			<button value="IND +91">IND +91</button>
		    </span>
            <input  type="text" class="form-control" id="candidate_mobilenumber" name="candidate_mobilenumber" placeholder="Mobile Number" required="required"/>
            </div>
            </div>
            </div>
         <div class="form-group">
		<form:input type="file" path="candidate_resume" class="form-control" />
         </div>
         </div>
         
         <div class="modal-footer">
         <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         <button type="submit" id="sub" class="btn btn-success">Send</button>
         </div>
             </form:form>
                        
                     </div>
                    </div>
                </div>
            </div>
         
         <div class="modal-footer">
       
         </div>
             </div>
             </div>
             </div>
								
								<button type="button"  job_id="${jobList.job_id}"  class="btn btn-primary applyJOB" data-toggle="modal" data-target="#loginModal" data-whatever="@mdo">Apply</button>
								
								</c:if>

						
							</div>
	

				</div>
				</c:if>
				</c:forEach>

		</div>
  
                <div class="center">
          <ul class="paginationforjob">

            <input type="hidden" value="${totalPages}" id="totalPages" />


          </ul>
        </div>
			</div>
			</div>
			<script type="text/javascript" src="js/jquery.twbsPagination.min.js"></script>
			<script type="text/javascript">
			
		$(document).ready(function() {
			var totalPages = $("#totalPages").val();
			
		    $('.paginationforjob').twbsPagination({
		    	
		      
		    	
		        totalPages: totalPages,
		        visiblePages:10,
		        
                 href: 'LEjobs.do?pageIndex={{number}}'
                		 
                	
		        		  
		        	  
		    });
		});
	
		
		</script>

</div>
<script>


/* $('#industry').change(function(event) {
	
	
	
	 var $c=$("#industry").val();
	   
	 
	   
	   if($c==0)
		   {
		      
		      var $location = $("#subcatogory");
		    
		      $location.find('option').remove(); 
		     
		      
		      $('<option></option>').val("0").text("").appendTo($location);
		      
		      
		      
		      
		   
		   }
	   else
		   {
		      
			
			$.ajax({
			    
			    url: "getjobtype.do",
			    type: 'GET',
			    dataType: 'json',
			    data: {id: $c },
			    contentType: 'application/json',
			    
			    success : function(response) {
			    	 
			    	
			    	  var $select = $("#subcatogory");                           
				         
					  $select.find('option').remove();  
					 
					  
					  
			        
			         $.each(response, function(key, value) {  
			        	   
			        	
							
			             $('<option></option>').val(key).text(value).appendTo($select);      
			              });
			    	
			    	
			    	
			    	
		           
			    },
			    error : function(error) {
			    	alert("some internal problem pls try again");
			    },
			
			});
			     
	     
		       
		       
		   
		   }
	
	

}); */
	$('.textarea_editor').wysihtml5();
	
	$(".locuseraserch").hide();
	  $("#datewise").click(function(){
		  
		   $(".hidedate").show();
		  $(".hidecategory").hide();
		   $(".hidesubcategory").hide();
		   
		});
	  $("#categorywise").click(function(){
		  $(".hidedate").hide();
		  $(".hidecategory").show();
		   $(".hidesubcategory").hide();
		   
		});
	  $("#subcategorywise").click(function(){ 
		  
		  $(".hidedate").hide();
		  $(".hidecategory").hide();
		   $(".hidesubcategory").show();
		    
		});
	  
	  
</script>

 <script src='js/bootstrapValidator.js'></script>
<script>

$(document).ready(function(){
	$('#job_Login')
	.on(
			'click',
			function(e) {

				var user_id = $("#job_username").val();
				var password = $("#job_password").val();

				/*<![CDATA[*/

				if (user_id.toString() != ""
						&& password.toString() != "") {
					
					$('#joblogin_regsiterGif').show();
					$
							.ajax({
								type : "POST",
								url : "userlogin.do",
								data : {
									"userid" : user_id,
									"pws" : password
								},
								success : function(
										responseData) {

									for ( var key in responseData) {

										if (responseData[key] == false) {
											$(
													'#jobinvalid_login')
													.show();
											$(
													'#joblogin_regsiterGif')
													.css(
															'display',
															'none');
			

											return false;
										} else {

											$(
													'#joblogin_regsiterGif')
													.show();

											$(
													'#joblogin_form')
													.trigger(
															'submit');
										}

									}

								}
							});
				}

				return false;
				/*]]>*/

			});
	
	$('.apply').on("click",function(){
		$("#job_id").attr("value",$(this).attr("job_id"));
		
		
			
		});
	$('.applyJOB').on("click",function(){
		$("#jobs_id").attr("value",$(this).attr("job_id"));
		
		
			
		});

		 $('#jobs_form,#jobss_form').bootstrapValidator({
		        feedbackIcons: {
		            valid: 'glyphicon glyphicon-ok',
		           
		            validating: 'gflyphicon glyphicon-refresh'
		        },
		        fields:{
		        	candidate_name: {
		                validators: {
		                    notEmpty: {
		                        message: ' Name is required'
		                    },
		                    
		                    regexp : {
								regexp : /^[a-zA-Z\s]+$/,
								message : 'Sorry !! Only alphabets are allowed.'
							}

		                }
		            },
		            candidate_email : {
					
						validators : {

							notEmpty : {
								message : 'Email is required'
							},
							regexp : {
								regexp : /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
								message : 'Please enter a valid email address'
							}
						
						}
					},
					
					candidate_resume: {
						   validators: {
							   file: {
								      extension: 'doc,docx,pdf,zip,rtf',												      
								      message: 'preferred formats should be (doc,docx,pdf,zip,rtf) and 5 MB at maximum.'
								},
						    notEmpty: {
						                    message: 'Resume is required.'
						              }
						    }
						  },

					candidate_mobilenumber: {
		        	 validators: {
		        		 notEmpty: {
	                           message: 'Mobile number is required.'
	                       },

	                 regexp: {
	                           regexp:/^[789]\d{9}$/,
	                           message: 'Mobile number is invalid.'
	                       },
		                     
		                   stringLength: {
		                	   min:10,
		                             max: 10,
		                             message: 'Please enter a valid 10 digit mobile number'
		                         },
		                        
		                      }
		                
		                  }
		                 }
		 });
});


</script>
<script language="Javascript">
$(document).ready(function(){
    $('#form').submit(function() {
        var error = 0;
        var industryid = $('#industryid').val();

        if (industryid == '0') {
            error = 1;
            alert(' Select  Industry.');
        }

        if (error) {
            return false;
        } else {
            return true;
        }

    });
});
        
</script>