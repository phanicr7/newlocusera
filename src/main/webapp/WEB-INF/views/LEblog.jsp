	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 --%><%@ page isELIgnored="false" %>
<style>
div.center {
  text-align: center;
}
body{
font-family: 'Open Sans', sans-serif !important;
}
</style>

	<!-- banner -->
	<!-- changed Here -->

<!-- 	<div class="banner about-banner" style="background: url(images/b2.jpg) no-repeat 0px 0px;background-size: cover;">
		<div class="header" style="background-color: white;"> -->

<style>
.small{
    width: 170px;
    margin: 15px 20px;
}
.full-width.dropdown {
    position: static;
}
.full-width.dropdown > .dropdown-menu {
    left: 0;
    right: 0;
    position: absolute;
    margin-top: -10px;
}
.full-width.dropdown > .dropdown-menu > li > a {
   white-space: normal; 
}
.navbar-nav > li > a {
    padding-top: 22px;
}
</style>
			<div class="banner" style="background-color: white  url(images/b2.jpg);">
			<!-- changed end -->
		<%-- <div class="header">

			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>

				

				<div class="header-right">
					<div class="agileinfo-social-grids">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-rss" style="color: black;"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus" style="color: black;"></i></a></li>
															<c:if test="${empty frontUserLoggedIn}">
								<li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="color: black;margin-left: 3em;"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
								</c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 15px;">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu"><a href="userLogout.do" ><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div> --%>
	<div class="header">

			<div class="container">
				<div class="header-left">
					<div class="w3layouts-logo">
						<h1>
							<a href="LEhome.do"><img src = "images/locus.png" class = "img-responsive small" alt = "Locusera"></a>
						</h1>
					</div>
				</div>
			
						<div class="header-bottom">
	
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav ">
									<li><a class="active list-border" href="LEhome.do" style="font-size: 14px;color:black">Home</a></li>
									<li><a href="LEabout.do" style="font-size: 14px;color:black">About</a></li>
															<li class="dropdown full-width sub-menu-parent"><a href="LEservices.do" class="dropdown-toggle hvr-bounce-to-bottom"  style="font-size: 14px;color:black" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
										  <ul class="dropdown-menu sub-menu" role="menu">
										  <div class = "row">
											 <div class = "col-md-2"></div>
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service1.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800 ">Recruitment / Talent Acquisition</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service2.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Out Sourcing & Permanent Staffing</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service3.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Training And Development</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service4.jpg" style = "width:160px;height:120px;"/></a>
													<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Payroll management</a></li>
											</div>
										</div>	
											  <div class = "row">
												 <div class = "col-md-2"></div>
												 
												 <div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service5.jpg" style = "width:160px;height:120px;"/></a>
															<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Compliance Management</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service6.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Green Field Project Execution</a></li>
											</div>
											
											<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service7.jpg" style = "width:160px;height:120px;"/></a>
										<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">HR & Legal Audits</a></li>
											</div>
											
										<div class = "col-md-2">
											<a href="LEservices.do"><img src = "images/service8.jpg" style = "width:160px;height:120px;"/></a>
											<li><a class="hvr-bounce-to-bottom" href="LEservices.do" style="color:black;font-weight:800">Patents, IPR, Trademark & Copyrights filing</a></li>
											</div>
										</div>
							
											
										</ul>
									</li>					
									<li><a href="LEjobs.do" style="font-size: 14px;color:black">Job Seeker</a></li>
									<li><a href="LEblog.do" style="font-size: 14px;color:black">Our Blog</a></li>
									<li><a class="list-border1" href="LEcontact.do" style="font-size: 14px;color:black">Contact Us</a></li>
<c:if test="${empty frontUserLoggedIn}">
      <li><a href = "#"  data-toggle="modal" data-target="#myLogin" style="font-size: 14px;color:black"><span class="glyphicon glyphicon-log-in" style="color: black;"></span> Login</a></li>
      
       </c:if>
								 <c:if test="${not empty frontUserLoggedIn}">
								       &nbsp;&nbsp;&nbsp;  <li class="dropdown" id="profile-messages" >
        <a href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle" style="font-size: 14px;color:black">
        <i class="icon icon-user"></i>
        <c:out value="${userList.le_user_firstname}"> </c:out><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a href="LEchangePass.do" style="font-size: 14px;color:black;"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
          <li class="divider"></li>
          <li class="dropdowm menu" ><a href="userLogout.do"  style="font-size: 14px;color:black;"><span class="	glyphicon glyphicon-off"></span> Logout</a></li>
      </ul>
      </li>
      </c:if>
											
								</ul>	
								
		</div>	

							
						</nav>		
				</div>
						
			
				
			
		</div>
				
			</div>
		</div>
				
			</div>

	<!-- //banner -->
				<div class="about-heading">	
			<h2 style ="color: #b3bf06;padding:1.9em;">Blog</h2>

	</div>
	<!-- <div class="about-heading">	
		<div class="container">
			<h2>Blog</h2>
		</div>
	</div> -->
	<!-- blog -->
	<div class="codes icons main-grid-border" style="padding: 0px;">
	<div class="blog" style="padding: 0px;">
		<div class="container">
		
		 <c:forEach items="${blogArticals}" var="item" varStatus="loopCounter">
         <c:choose>
         <c:when test = "${loopCounter.count%2==0}">
         	<hr>
			<div class="col-md-12 agile-blog-grid-left" style="margin-top:60px; box-shadow: 5px 3px 30px #888888;"">
				<div class="agile-blog-grid">
					<div class = "col-md-7">
						<div class="blog-left-grids">
							<div class="blog-left-left">
								<i class="fa fa-newspaper-o" aria-hidden="true"></i>
							</div>
							<div class="blog-left-right-top">
								<h4 style="padding: 16px 0px;">  &nbsp;${item.artical_title}</h4>
								<p>Posted By  &nbsp;&nbsp; ${item.writer_name}  &nbsp;&nbsp; on  ${item.posteddate}  &nbsp;&nbsp; <a href="#"></a></p>
							</div>
							<div class="blog-left-right-bottom">
				                       <p style="color:#000;white-space: nowrap; width: 100%;max-width: 40em;overflow: hidden;text-overflow: ellipsis;font-size: 15px;">${item.artical_content}</p>
				          				<a href="aboutBlog.do?id=${item.blog_id}" style="float:left;">Read More</a>								
				          </div>
				       </div>
				  </div>
						
				
					<div class = "col-md-5" >
							<div class="agile-blog-grid-left-img">
								<a href="single.html"><img height="100%" src="${item.artical_logo_path}" alt="" /></a>
							</div>
					</div>
					</div>
			</div>	
		</c:when>
         
        
         
         <c:otherwise>
         
         		<div class="agile-blog-grids">
				<div class="col-md-12 agile-blog-grid-left" style="margin-top: 60px; box-shadow: 5px 3px 30px #888888;"">
					<div class="agile-blog-grid">
					<div class = "col-md-5">
						<div class="agile-blog-grid-left-img">
							<a href=""><img height="100%" src="${item.artical_logo_path}" alt="" /></a>
						</div>
					</div>	
					<div class = "col-md-7">
						<div class="blog-left-grids">
							<div class="blog-left-left">
								<i class="fa fa-newspaper-o" aria-hidden="true"></i>

							</div>
							<div class="blog-left-right-top">
							<h4 style="padding: 16px 0px;">  &nbsp;${item.artical_title}</h4>
							<p>Posted By  &nbsp;&nbsp;  ${item.writer_name}  &nbsp;&nbsp; on ${item.posteddate} &nbsp;&nbsp; <a href="#"></a></p>
							</div>
						
								<div class="blog-left-right-bottom">
									<p style="color:#000;white-space: nowrap; width: 100%;max-width: 40em;overflow: hidden;text-overflow: ellipsis;font-size: 15px;">${item.artical_content}</p>
          <a href="aboutBlog.do?id=${item.blog_id}" style="float: right;">Read More</a>								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>	
					</div>
					</div>
         	
         
          
         </c:otherwise>
      </c:choose>
        </c:forEach>
				


				</div>
				
			
				<div class="clearfix"> </div>
				
				

<!-- 
					<nav>
						<ul class="pagination">
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">�</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" aria-label="Next">
									<span aria-hidden="true">�</span>
								</a>
							</li>
						</ul>
					</nav> -->
					<div class="center">
          <ul class="paginationforblog">

            <input type="hidden" value="${totalPages}" id="totalPages" />


          </ul>
        </div>
			</div>
			</div>
			<script type="text/javascript" src="js/jquery.twbsPagination.min.js"></script>
			<script type="text/javascript">
			
		$(document).ready(function() {
			var totalPages = $("#totalPages").val();
			
		    $('.paginationforblog').twbsPagination({
		    	
		      
		    	
		        totalPages: totalPages,
		        visiblePages:10,
		        
                 href: 'LEblog.do?pageIndex={{number}}'
                		 
                	
		        		  
		        	  
		    });
		});
	
		
		</script>
			
	<!-- //blog -->
